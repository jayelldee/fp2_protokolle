clear all
close all
%Quantile der t-Verteilung fuer zweiseitigen Vertrauensbereich für 95% und
%99% Wahrscheinlichkeit
t_quantil=[
    1 12.706  63.66;
    2 4.303   9.925;
    3 3.182 	5.841;
    4 2.776 	4.604;
    5 2.571 	4.032;
    6 2.447 	3.707;
    7 2.365 	3.499;
    8 	2.306 	3.355;
    9 	2.262 	3.250;
    10 	2.228 	3.169;
    11 	2.201 	3.106;
    12 	2.179 	3.055;
    13 	2.160 	3.012;
    14 	2.145 	2.977;
    15 	2.131 	2.947;
    16 	2.120 	2.921;
    17 	2.110 	2.898;
    18  2.101 	2.878;
    19  2.093   2.861;
    20 	2.086 	2.845;
    21 	2.080 	2.831;
    22 	2.074 	2.819;
    23 	2.069 	2.807;
    24 	2.064 	2.797;
    25 	2.060 	2.787;
    26 	2.056 	2.779;
    27 	2.052 	2.771;
    28 	2.048 	2.763;
    29 	2.045 	2.756;
    30 	2.042 	2.750];
    
% fid = fopen(fname);
% iRow = 1;
% description_str={};
% parameter_list={};
% while (~feof(fid))
%     data_line = textscan(fid,'%s %s\n','CommentStyle','%','Delimiter',':','CollectOutput',true);
%     %    celldisp(data_line)
%     if size(data_line{:},1)>0
%         description_str=data_line{1}{1};
%         parameter_list=data_line{1}{2};
%         iRow = iRow + 1;
%     end
% end
% npar=size(description_str,2);
% fclose(fid);
% optpar=get_optpar(parameter_list);

filename = 'Ausgleichsrechnung_Eingabe.txt';
[fid, errmsg]= fopen(filename);
if fid<0
    error("Fehler beim Oeffnen der Eingabedatei: %s",errmsg)
end
iRow = 1;
description_str={};
parameter_list={};
n_data=0;

% Standardwerte für die Vairablen setzen
name_auswerter='';
name_versuch='';
name_x='';
dim_x='';
name_y='';
dim_y='';
achstrafo_x='x';
achstrafo_y='y';
daten_tabelle=[];

isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;

while ~feof(fid)
    line=fgetl(fid);
    data_line = textscan(line,'%s %s\n','CommentStyle','%','Delimiter',':','CollectOutput',true);
    celldisp(data_line)
    if size(data_line{:},1)>0
        description_str=data_line{1}{1};
        data_entry=data_line{1}{2};
        switch description_str
            case 'Name'
                name_auswerter=data_entry;
            case 'Versuch'
                name_versuch=data_entry;
            case 'Groesse x-Achse'
                name_x=data_entry;
            case 'Einheit x-Achse'
                dim_x=data_entry;
            case 'Groesse y-Achse'
                name_y=data_entry;
            case 'Einheit y-Achse'
                dim_y=data_entry;
            case 'Achsentransformation x'
                achstrafo_x=data_entry;
            case 'Achsentransformation y'
                achstrafo_y=data_entry;
            case 'Wertepaare x/y'
                while ~feof(fid)
                    line=fgetl(fid);
                    data_line = textscan(line,'%f %f\n','CommentStyle','%');
                    num1_ok=true; num2_ok=true;
                    if isnan(data_line{1,1}) num1_ok=false; end
                    if isnan(data_line{1,2}) num2_ok=false; end
                    if num1_ok && num2_ok
                        daten_tabelle=[daten_tabelle; data_line{1} data_line{2}];
                    else
                        error('2 numerische Werte pro Zeile erwartet.');
                    end
                    n_data=size(daten_tabelle,1);
                end
            otherwise
                warning('Eingabezeile ungueltig: %s',description_str);
        end
    end
end
fclose(fid);
if n_data<3
    error('Wenigstens 3 Wertepaare erforderlich.');
end
switch achstrafo_x
    case 'x'
        achstrafo_x_labelprefix='';
        achstrafo_x_labelpostfix='';
        xdata=daten_tabelle(:,1);
    case 'x^2'
        achstrafo_x_labelprefix='(';
        achstrafo_x_labelpostfix=')^2';
        xdata=daten_tabelle(:,1).^2;
    case 'x^3'
        achstrafo_x_labelprefix='('
        achstrafo_x_labelpostfix=')^3';
        xdata=daten_tabelle(:,1).^3;
    case 'x^4'
        achstrafo_x_labelprefix='(';
        achstrafo_x_labelpostfix=')^4';
        xdata=daten_tabelle(:,1).^4;
    case '1/x'
        achstrafo_x_labelprefix='1/(';
        achstrafo_x_labelpostfix=')';
        xdata=daten_tabelle(:,1).^(-1);
    case '1/x^2'
        achstrafo_x_labelprefix='1/(';
        achstrafo_x_labelpostfix=')^2';
        xdata=daten_tabelle(:,1).^(-2);
    case '1/x^3'
        achstrafo_x_labelprefix='1/(';
        achstrafo_x_labelpostfix=')^3';
        xdata=daten_tabelle(:,1).^(-3);
    case '1/x^4'
        achstrafo_x_labelprefix='1/(';
        achstrafo_x_labelpostfix=')^4';
        xdata=daten_tabelle(:,1).^(-4);
    case 'exp(x)'
        achstrafo_x_labelprefix='exp(';
        achstrafo_x_labelpostfix=')';
        xdata=exp(daten_tabelle(:,1));
    case 'ln(x)' % natürlicher Logarithmus (in octave log)
        achstrafo_x_labelprefix='ln(';
        achstrafo_x_labelpostfix=')';
        xdata=log(daten_tabelle(:,1));
    case 'lg(x)' % dekadischer Logarithmus (in octave log10)
        achstrafo_x_labelprefix='lg(';
        achstrafo_x_labelpostfix=')';
        xdata=log10(daten_tabelle(:,1));
    case 'sqrt(x)'
        achstrafo_x_labelprefix='sqrt(';
        achstrafo_x_labelpostfix=')';
        xdata=sqrt(daten_tabelle(:,1));
    otherwise
        error('x-Achsentransformation ungueltig: %s',achstrafo_x);
end

switch achstrafo_y
    case 'y'
        achstrafo_y_labelprefix='';
        achstrafo_y_labelpostfix='';
        ydata=daten_tabelle(:,2);
    case 'y^2'
        achstrafo_y_labelprefix='(';
        achstrafo_y_labelpostfix=')^2';
        ydata=daten_tabelle(:,2).^2;
    case 'y^3'
        achstrafo_y_labelprefix='(';
        achstrafo_y_labelpostfix=')^3';
        ydata=daten_tabelle(:,2).^3;
    case 'y^4'
        achstrafo_y_labelprefix='(';
        achstrafo_y_labelpostfix=')^4';
        ydata=daten_tabelle(:,2).^4;
    case '1/y'
        achstrafo_y_labelprefix='1/(';
        achstrafo_y_labelpostfix=')';
        ydata=daten_tabelle(:,2).^(-1);
    case '1/y^2'
        achstrafo_y_labelprefix='1/(';
        achstrafo_y_labelpostfix=')^2';
        ydata=daten_tabelle(:,2).^(-2);
    case '1/y^3'
        achstrafo_y_labelprefix='1/(';
        achstrafo_y_labelpostfix=')^3';
        ydata=daten_tabelle(:,2).^(-3);
    case '1/y^4'
        achstrafo_y_labelprefix='1/(';
        achstrafo_y_labelpostfix=')^4';
        ydata=daten_tabelle(:,2).^(-4);
    case 'exp(y)'
        achstrafo_y_labelprefix='eyp(';
        achstrafo_y_labelpostfix=')';
        ydata=exp(daten_tabelle(:,2));
    case 'ln(y)' % natürlicher Logarithmus (in octave log)
        achstrafo_y_labelprefix='ln(';
        achstrafo_y_labelpostfix=')';
        ydata=log(daten_tabelle(:,2));
    case 'lg(y)' % dekadischer Logarithmus (in octave log10)
        achstrafo_y_labelprefix='lg(';
        achstrafo_y_labelpostfix=')';
        ydata=log10(daten_tabelle(:,2));
    case 'sqrt(y)'
        achstrafo_y_labelprefix='sqrt(';
        achstrafo_y_labelpostfix=')';
        ydata=sqrt(daten_tabelle(:,2));
    otherwise
        error('y-Achsentransformation ungueltig: %s',achstrafo_y);
end


xavg=sum(xdata)/n_data;
yavg=sum(ydata)/n_data;

a_par=sum((xdata-xavg).*(ydata-yavg))/sum((xdata-xavg).^2); % Anstieg der Regressionsgeraden a
b_par=yavg-a_par*xavg;                                      % Achsenabschnitt b

R2=(sum((xdata-xavg).*(ydata-yavg)))^2/(sum((xdata-xavg).^2)*sum((ydata-yavg).^2)); %Bestimmtheitsmaß

var_x=sum((xdata-xavg).^2);
var_y=sum((ydata-yavg).^2);

% sigma_x=sqrt(var_x)
% sigma_y=sqrt(var_y)

sigma_hat=sqrt(1/(n_data-2)*sum((ydata-b_par-a_par.*xdata).^2)); % Standardfehler der Regression

sigma_a=sigma_hat*sqrt(1/sum((xdata-xavg).^2));     % Standardfehler von a
sigma_b=sigma_hat*sqrt(sum(xdata.^2)/(n_data*sum((xdata-xavg).^2))); % Standardfehler von b

t_95=t_quantil(n_data-2,2);       % t-Quantil für 95% Wahrscheinlichkeit
t_99=t_quantil(n_data-2,3);       % t-Quantil für 99% Wahrscheinlichkeit

% Vertrauensgrenzen für a
u_a95=t_95*sigma_a;
u_a99=t_99*sigma_a;

% Vertrauensgrenzen für b
u_b95=t_95*sigma_b;
u_b99=t_99*sigma_b;

% rel. Vertrauensgrenzen für a
u_a95_rel=abs(u_a95/a_par);
u_a99_rel=abs(u_a99/a_par);

% rel. Vertrauensgrenzen für b
u_b95_rel=abs(u_b95/b_par);
u_b99_rel=abs(u_b99/b_par);


fig1=figure;
set(gcf,'Name','Main');
set(gcf,'PaperType','a4','PaperUnits','centimeters');
width=840;
height=1180;
set(gcf,'Position',[2200 90 width height]);
top_ax=subplot(5,1,1);
main_ax=subplot(5,1,[2 3]);
bottom_ax=subplot(5,1,4);
bottom2_ax=subplot(5,1,5);

set(gcf,'CurrentAxes',top_ax); 
hold on;

t1=text(0.50,.99,'Ausgleichsrechnung    y = a x + b','HorizontalAlignment','center','FontSize',18);

 t2str=sprintf('Name: %s',name_auswerter);
 t3str=sprintf('Versuch: %s',name_versuch);
 t4str=sprintf(datestr(clock()));
t2_4str={t2str,t3str,t4str};
t2_4=text(0.50,.6,t2_4str,'HorizontalAlignment','center');

% t2str=sprintf('Name: %s',name_auswerter);
% t2=text(0.50,.9,t2str,'HorizontalAlignment','center');
% t3str=sprintf('Versuch: %s',name_versuch);
% t3=text(0.50,.8,t3str,'HorizontalAlignment','center');
% t4=text(0.50,0.7,t4str,'HorizontalAlignment','center');
%plot([0 1],[0.05 0.05],'k-');

 t5str=sprintf('Achsentransformationen:  ');
 if strcmp(achstrafo_x,'x')
     achstrafo2_x='keine';
 else
     achstrafo2_x=achstrafo_x;
 end
 if strcmp(achstrafo_y,'y')
     achstrafo2_y='keine';
 else
     achstrafo2_y=achstrafo_y;
 end
 t6str=sprintf('x: %s      y: %s',achstrafo2_x,achstrafo2_y);
%t5_6str={t5str,t6str};
t5_6str=sprintf('%s %s',t5str,t6str);

t5_6=text(0.50,.05,t5_6str,'HorizontalAlignment','center');
axis off

set(gcf,'CurrentAxes',main_ax); 
%subplot(2,1,1)
hold on;
plot(xdata,ydata,'k+');
plot(xdata,xdata*a_par+b_par,'r-');
%axis padded;
xlabelstr=sprintf('%s%s/%s%s',achstrafo_x_labelprefix,name_x,dim_x,achstrafo_x_labelpostfix);
xlabel(xlabelstr);
xmin=min(xdata); xmax=max(xdata);
ymin=min(ydata); ymax=max(ydata);
xlim([xmin-0.05*(xmax-xmin) xmax+0.05*(xmax-xmin)]);
ylim([ymin-0.05*(ymax-ymin) ymax+0.05*(ymax-ymin)]);

ylabelstr=sprintf('%s%s/%s%s',achstrafo_y_labelprefix,name_y,dim_y,achstrafo_y_labelpostfix);
ylabel(ylabelstr);

%subplot(2,1,2);
set(gcf,'CurrentAxes',bottom_ax); 

ydiff=ydata-(xdata*a_par+b_par);
stem(xdata,ydiff,'k+-');
%axis padded;
xlabelstr=sprintf('%s%s/%s%s',achstrafo_x_labelprefix,name_x,dim_x,achstrafo_x_labelpostfix);
xlabel(xlabelstr);
ylabel('Differenzen');
xlim([xmin-0.05*(xmax-xmin) xmax+0.05*(xmax-xmin)]);
ymin=min(ydiff); ymax=max(ydiff);
ylim([ymin-0.05*(ymax-ymin) ymax+0.05*(ymax-ymin)]);


set(gcf,'CurrentAxes',bottom2_ax); 

 t7str=sprintf('Anstieg                        a = %.8f %s',a_par);
 t8str=sprintf('Standardabweichung            Sa = %.8f %s',sigma_a);
 t9str=sprintf('Vertrauensgrenze [95%%]     t*Sa = %.8f %s',u_a95);
t10str=sprintf('relative Messunsicherheit t*Sa/a = %.8f %s',u_a95_rel);
text(0.4,.7,{t7str,t8str,t9str,t10str},'HorizontalAlignment','right');

t11str=sprintf('Schnittpunkt mit der y-Achse   b = %.8f %s',b_par);
t12str=sprintf('Standardabweichung            Sb = %.8f %s',sigma_b);
t13str=sprintf('Vertrauensgrenze [95%%]     t*Sb = %.8f %s',u_b95);
t14str=sprintf('relative Messunsicherheit t*Sb/b = %.8f %s',u_b95_rel);
text(0.99,.7,{t11str,t12str,t13str,t14str},'HorizontalAlignment','right');


axis off;
plotfile='./plot.pdf';
print(fig1,plotfile,'-dpdf','-bestfit');