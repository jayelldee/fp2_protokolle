GNU Octave-Skript zur Ausgleichsrechnung
----------------------------------------

Datei Ausgleichsrechnung.m:
  Berechnungsskript, wird ausgeführt
Datei Ausgleichsrechnung_Eingabge.txt:
  Parameter, Werteliste, wird von Ausgleichsrechnung.m eingelesen

Ausgabe:
graphische Darstellung der Messwerte in Fenster
Zusammenfassung als plot.pdf (wie beim Windows-Programm Ausgleichsrechnung.exe)
Anstieg, Schnittpunkt mit y-Achse, Standardabweichung, Vertrauensgrenze, relative Messunsicherheit

Erläuterungen zur Datei Ausgleichsrechnung_Eingabe.txt
------------------------------------------------------

Schlüsselwort: Wert
Die Schlüsselwörter sind:
Name
Versuch
Größe x-Achse
Einheit x-Achse
Größe y-Achse
Einheit y-Achse
Achsentransformation x
Achsentransformation y
Wertepaare x/y

jegliche LeerZEILEN werden ignoriert

Wertepaare x/y muss als letztes stehen und dahinter die Messwerte,
alle anderen davor in beliebiger Zeile

Erlaubte Werte für Achsentransformation:
x, x^2, x^3, x^4, 1/x, 1/x^2, 1/x^3, 1/x^4, exp(x), ln(x), lg(x), sqrt(x)
das gleiche für y

ln() ist der natürliche Logarithmus
lg() ist der dekadische Logarithmus

Unter Wertepaare x/y stehen die Messwertpaare
1 Messwertpaar pro Zeile
x und y getrennt durch Tab oder Leerzeichen

Werte können wie folgt angegeben werden:
z.B. Schreibweise für 10:
10
10.0
1e1
1.0e1
.1e2
etc.

Messwertepaare können mittels vorangestelltem Prozentzeichen
auskommentiert (aus der Berechnung herausgenommen) werden, z.B.
1.0 2.1
2.5 3.0
4.0 3.9
%5.5 5.2
6.0 5.9