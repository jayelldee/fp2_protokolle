

%%% Auswertung der Zeitmessung
filename = 'Messdaten_Pendel_Zeit.csv';
% Die Dimension der Elemente, ob Verktor oder Matrix, sieht man links bei "Arbeitsumgebung"
M_Zeit = csvread(filename);


[nz, ns]=size(M_Zeit); % Formatueberpruefung nz für die Zeilen, ns für die Spalten
if nz ~=11 
       error('Die Messdatenliste muss genau 11 Zeilen lang sein.'); % error ist ein Abbruchbefehl
end
if ns ~=3
       error('Die Messdatenliste muss genau 3 Spalten besitzen.'); % error ist ein Abbruchbefehl
end


N_vec=M_Zeit(1,:); % N=1,10,30 wird uebernommen -> alle Elemente einer Zeile

t_matrix=M_Zeit(2:nz,:); % Messwerte werden in eine Matrix übernommen

t_mittel=mean(t_matrix); %Mittelwertbestimmung für jede Spalte, hat die Dimension 1x3
s_t=std(t_matrix); % Standartabweichung
sN_s1=s_t/s_t(1); % alle Standardabweichungen werden durch erste geteilt
delta_t_z=s_t*2.262/sqrt(10); % Faktoren bitte Anpassen sofern sich n ändert
delta_t_s=1.4*t_mittel/1e5; % Faktoren bitte Anpassen sofern sich n ändert
u_t=delta_t_z+delta_t_s;
u_t_rel=u_t./t_mittel;
vgl_u_t=u_t/u_t(1);
pr_zf=t_mittel/t_mittel(1);

T=t_mittel./N_vec;
u_T=u_t./N_vec;

fileid=1;       % Ausgabe im Befehlsfenster
%fileid=fopen("Ausgabe","w"); % Ausgabe als Textdatei
fprintf(fileid,'\nMathematisches Pendel - Versuchsauswertung\n'); % \n newline
fprintf(fileid,'\na) Zeitmessung\n'); 
fprintf(fileid,'% 50s','Mittelwert [s]:'); fprintf(fileid,'%10.4f',t_mittel); fprintf(fileid,'\n'); % "% 50s:" Platzhalter für Variablen 50 Zeichen bis zum Doppelpunkt ; "%10.4" 10 Zeichen insgesamt und vier davon hinter dem Komma
fprintf(fileid,'% 50s','Standardabweichung der Stichprobe [s]:'); fprintf(fileid,'%10.4f',s_t); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Vergleich der Standardabw. der Stichprobe:'); fprintf(fileid,'%10.4f',sN_s1); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Vertrauensgrenze (zufaellige Messabweichung) [s]:'); fprintf(fileid,'%10.5f',delta_t_z); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Systematische Messabweichung [s]:'); fprintf(fileid,'%10.1e',delta_t_s); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Messunsicherheit absolut [s]:'); fprintf(fileid,'%10.5f',u_t); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Messunsicherheit relativ [%]:'); fprintf(fileid,'%10.4f',u_t_rel*100); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Vergleich der Messunsicherheiten:'); fprintf(fileid,'%10.1f',vgl_u_t); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Pruefung auf Zaehlfehler:'); fprintf(fileid,'%10.1f',pr_zf); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','N corr:'); fprintf(fileid,'%10d',N_vec); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Ergebnis Schwingungsdauer [s]:'); fprintf(fileid,'%10.5f',T); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Messunsicherheit absolut [s]:'); fprintf(fileid,'%10.5f',u_T); fprintf(fileid,'\n');

%%% Auswertung der Laengenmessung 
filename = 'Messdaten_Pendel_Laenge.csv';
daten2 = csvread(filename);

[nz, ns]=size(daten2);
if nz ~=5 
       error('Die Messdatenliste muss genau 5 Zeilen lang sein.');
end
if ns ~=1
       error('Die Messdatenliste muss genau 1 Spalte besitzen.');
end

M_Laenge=daten2(1:nz-1,ns);
delta_l_z=daten2(nz,ns);        % geschaetzter zufaelliger Fehler in der letzten Zeile von daten2

l_mittel=mean(M_Laenge);

delta_l_s=0.5+l_mittel/100; % Faktoren bitte Anpassen
u_l=delta_l_z+delta_l_s;
u_l_rel=u_l/10/l_mittel;

g_berechnet=4*pi^2*l_mittel./(100*T.^2);

fprintf(fileid,'\nb) Laengenmessung\n'); 
fprintf(fileid,'% 50s','Mittelwert [cm]:'); fprintf(fileid,'%10.4f',l_mittel); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Zufaellige Messabweichung von l (geschaetzt) [mm]:'); fprintf(fileid,'%10.4f',delta_l_z); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Systematische Messabweichung Delta_l_syst [mm]:'); fprintf(fileid,'%10.4f',delta_l_s); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Messunsicherheit absolut [mm]:'); fprintf(fileid,'%10.4f',u_l); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Messunsicherheit relativ [%]:'); fprintf(fileid,'%10.4f',u_l_rel*100); fprintf(fileid,'\n');

fprintf(fileid,'\nc) Zwischenergebnis\n'); 
fprintf(fileid,'% 50s','g mit Schutzstellen [m/s2]:'); fprintf(fileid,'%10.5f',g_berechnet); fprintf(fileid,'\n');
%fclose(fileid); % Schliessen der Ausgabedatei
