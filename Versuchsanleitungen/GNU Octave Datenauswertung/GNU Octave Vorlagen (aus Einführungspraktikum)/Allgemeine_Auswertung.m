%%% Auswertung einer Messung

filename = 'Daten_03.csv';
daten1 = csvread(filename); % Daten werden eingelesen

[nz, ns]=size(daten1); % Definieren von Zeilen und Spalten


t_matrix=daten1(1:nz,:); % Matrix wird ab der ersten Zeile definiert

n=10; % Anzahl der Messwerte für Zeile 16
tau=2.262; % Das Tau hängt von n ab, hier für n=10

t_mittel=mean(t_matrix); % Mittelwertbestimmung für jede Spalte
s_t=std(t_matrix); % Standardabweichung
delta_t_x=s_t*tau/sqrt(n); % Zufaellige Messabweichung (Vertrauensbereich), bitte Zeile 11 und 12 beachten.
delta_t_y=1.4*t_mittel/1e5; % Systematische Messabweichung, Faktoren bitte anpassen, bzw. Formel anpassen 
u_a=delta_t_x+delta_t_y; % Addition beider Messabweichungen, u_a = Messunsicherheit
u_a_rel=u_a./t_mittel; % Berechnung für relative Angabe

% Die Messunsicherheit wird hier nur von einer Groesse bestimmt, beachten Sie die Messunsicherheitsfortpflanzung.

fileid=1;       % Ausgabe im Befehlsfenster
%fileid=fopen("Ausgabe","w"); % Ausgabe als Textdatei
fprintf(fileid,'% 50s','Mittelwert [??]:'); fprintf(fileid,'%12.4e',t_mittel); fprintf(fileid,'\n'); % In der Klammer [??] bitte die richtige Einheit eintragen.
fprintf(fileid,'% 50s','Standardabweichung der Stichprobe [??]:'); fprintf(fileid,'%12.4e',s_t); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Zufaellige Messabweichung [??]:'); fprintf(fileid,'%12.4e',delta_t_x); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Systematische Messabweichung [??]:'); fprintf(fileid,'%12.4e',delta_t_y); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Messunsicherheit absolut [??]:'); fprintf(fileid,'%12.4e',u_a); fprintf(fileid,'\n');
fprintf(fileid,'% 50s','Messunsicherheit relativ [%]:'); fprintf(fileid,'%12.4f',u_a_rel*100); fprintf(fileid,'\n'); % Bei der Ausgabe wird die relative Angabe in % umgerechnet (u_a_rel*100)
%fclose(fileid); % Schliessen der Ausgabedatei