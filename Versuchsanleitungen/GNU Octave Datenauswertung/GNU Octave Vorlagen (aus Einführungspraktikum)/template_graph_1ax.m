
filename = 'Daten_01.csv'; % string Zuweisung
data = csvread(filename); % Dateiname wird uebergeben in data was eine Matrix ist


figure; %legt Objekt an
ax=gca; % get current axes
hold on % Ueberlagerung der Plots nur wenn mehr als ein plot geschrieben wird
plot(data(:,1),data(:,2),'r+-'); %plot1; :,1 steht für alle Elemete in der ersten Spalte; r=Rot; +=Marker; -Verbindung
plot(data(:,1),data(:,2)-0.1,'r+-','markersize',10); % plot1; mit groesseren Markern
plot(data(:,1),data(:,2)-0.2,'bo'); % plot2; b=Blau; o=Marker
title('Kraft-Weg-Diagramm','FontWeight','bold')
xlabel('Weg [cm]','Fontsize',13);
ylabel('Kraft [N]','Fontsize',13);


legend('erste Messung','zweite Messung');
grid on;
box on



