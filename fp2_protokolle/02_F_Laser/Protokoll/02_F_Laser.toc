\babel@toc {english}{}
\contentsline {section}{\numberline {1}Non-linear optics and higher harmonic generation}{1}{section.1}%
\contentsline {subsection}{\numberline {1.1}Frequency doubling and SHG}{1}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Frequency mixing and THG}{2}{subsection.1.2}%
\contentsline {section}{\numberline {2}The Nd:YAG laser and Q-switching}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Operating principle}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Q-switching}{4}{subsection.2.2}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
