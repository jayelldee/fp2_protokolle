\documentclass[a4paper,10pt,headsepline=2pt,footinclude=false]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{graphicx}
\graphicspath{{Bilder/}}
\usepackage{xcolor}
\usepackage[left=2cm,right=1.5cm,top=2.5cm,bottom=2cm]{geometry}
\usepackage{scrlayer-scrpage}
\usepackage[version=3]{mhchem}
\usepackage{csquotes}
\usepackage{mdframed}
\usepackage{multirow}
\usepackage{caption}
\usepackage{hyperref}
\usepackage{siunitx}
\sisetup{locale = DE, per-mode = fraction, separate-uncertainty}
\usepackage{ulem}
\usepackage{appendix}
\usepackage{float}
\usepackage{subfig}
\usepackage{enumitem}
\usepackage{helvet}
\newcommand{\sst}[1]{_{\text{#1}}}
\newcommand{\ra}{$\rightarrow$ }
\newcommand{\eq}[1]{Eq.(\ref{eq:#1})}
\newcommand{\fig}[1]{Fig. \ref{fig:#1}}
\newcommand{\tab}[1]{Tab. \ref{tab:#1}}
\newcommand{\bib}[1]{[\ref{bib:#1}]}
\renewcommand{\familydefault}{\sfdefault}
\newcommand*\myappendixchanges{%
  \setcounter{table}{0}%
  \gdef\thesection{A.\arabic{section}}%
  \gdef\thetable{A.\arabic{table}}}


\lohead{\includegraphics[width=4cm]{logo}}			%Logo in der Kopfzeile
\cohead{Advanced laboratory course 2\\[0.2cm] Alex Kreis \& Jannik Dierks - Group 1}	%Text in der Kopfzeile
\rohead{\thepage}									%Seitenzahl
\cfoot[]{}											%keine Fußzeile
\setkomafont{headsepline}{\color{red!70!black}}		%Farbe der Trennlinie	
\setkomafont{pagehead}{\bfseries}					%Im Kopf Fett geschrieben
\usepackage{colortbl}								%bringt Farbe in die Tabelle
\definecolor{dunkelgrau}{gray}{0.8}					%Farbe definieren
\definecolor{hellgrau}{gray}{0.9}					%Farbe definieren
\definecolor{rot}{rgb}{0.616,0.051,0.082}			%Farben definieren 0-100%

%Links zu Gleitumgebung von figure und table:
%http://tex.lickert.net/tipps/optionh/optionh.html
%https://www.texwelt.de/fragen/3427/wann-sollte-ich-gleitumgebungen-fur-tabellen-und-abbildungen-verwenden

\setlength\parindent{0pt}

%--------------------------------------------------------------------------------
%	DOKUMENT
%--------------------------------------------------------------------------------
\begin{document}

\renewcommand{\figurename}{Abb.}
\counterwithin{figure}{section}
\renewcommand{\tablename}{Tab.}
\counterwithin{table}{section}
\counterwithin{equation}{section}

\begin{center}
	\huge \textbf{Non-linear optics and the Nd:YAG-laser}\\\vspace{3pt}
	\Large Lead author: Jannik Dierks \\
	\large Second author: Alex Kreis\\
	\Large Group 1\\
	\vspace{0.5cm}
	\large Institute for Physics - University of Rostock - \today\\
	Supervisor: Mr. Kumar
\end{center}

\tableofcontents
\vspace{0.5cm}

\section*{Aim of the course}
\begin{enumerate}[label=\alph*)]
	\item
\end{enumerate}

	
\section{Non-linear optics and higher harmonic generation}

To understand the way frequency doubling or mixing works one needs to familiarize with the way non-linear optics work. In general the term \enquote{non-linear} refers to the fact, that at high light-field-intensities ($> \SI{e8}{\frac{\volt}{\meter}}$) the polarization in materials reacts non-linearily to the amount of light being send into them. This effect is commonly used in laboratories to create higher harmonic beams of a laser. Therefor the main beam of the laser is send into a BBO crystal ($\beta$-Bariumborat crystal) which has the properties described above. When exiting the crystal the beam has doubled its frequency. This process is known as second harmonic generation. In the following we will describe this process of frequency doubling on the example of second harmonic generation (SHG) and explain how frequency mixing can be implemented on the example of third harmonic generation (THG). 

\subsection{Frequency doubling and SHG}
The frequency doubling is done with BBO-crystals. Therefor light of the laser beam is send into the crystal and drives electrons, which are not strictly bound. Driven by the main frequency of the laser light the electrons then can emit higher harmonics of the incoming laser beam - such as the second harmonic. This process is known as the \enquote{second harmonic generation}.\\
To mathematically discribe the procedure, the polarisation of the electromagnetic wave is developed in a power series such as

\begin{align}
    P=\varepsilon_0 (\chi_1E+\chi_2E^2+ \dots)
    \label{eq:polar}
\end{align}
For an easier visualisation, the wave can be discribed by the complex equation
\begin{align}
    E=\hat{E}\cdot e^{iwt}+\hat{\Bar{E}} \cdot e^{-iwt}.
    \label{eq:wave}
\end{align}
Inserting \eq{wave} in the quadratic term of \eq{polar} we get 
\begin{align}
    E^2=\hat{E}^2\cdot e^{i2wt}+\hat{E}^2 \cdot e^{-i2wt}+\hat{E}^2
    \label{eq:shg}
\end{align}
and are able to see a term independend of the frequency $\omega$, as well as two terms depending on double the frequency of $\omega$. This is the Second Harmonic that can be observed.\\
In order for the SHG to work properly a phase-matching condition needs to be satisfied. As the induced dipoles in the BBO emit waves with frequency $\omega$ they run through the medium with a phase velocity of 
\begin{align}
    \vec{v}\sst{Ph} = \dfrac{\omega}{\vec{k}} = \dfrac{c_0}{n(\omega)}
    \label{eq:phase_vel}
\end{align}
where $\vec{k}$ is the wave number, $c_0$ the lightspeed in vacuum and $n$ the refractive index of the medium.\\
In order for the second harmonic (SH) to build it is crucial, that the emitted waves at different places of the material match their phases, such that the higher hamonics have the same group velocity as the fundamental. Therefor the wave numbers $\vec{k}_i$ have to satisfy one of the following two phase-matching conditions:
\begin{align}
    \vec{k}(\omega_1 + \omega_2) &= \vec{k}(\omega_1) + \vec{k}(\omega_2) \nonumber \\
    \vec{k}(\omega_1 - \omega_2) &= \vec{k}(\omega_1) - \vec{k}(\omega_2)
    \label{eq:phase_match}
\end{align}
For collinear waves inside the medium the following equation can be set with keeping \eq{phase_match} and $\dfrac{\omega}{k} = \dfrac{c_0}{n}$ in mind:
\begin{align}
    n_3\omega_3 = n_1\omega_1 + n_2\omega_2 \hspace{0.5cm} \text{with} \hspace{0.5cm} \omega_3 = \omega_1 \pm \omega_2
\end{align}
With \eq{phase_vel} one can see that for $\omega_1 = \omega_2 = \omega$ the phase-matching condition comes down to
\begin{align}
    k(2\omega) = 2k(\omega)
    \label{eq:shg_phase}
\end{align}
where the phase velocity then is $v\sst{Ph}(2\omega) = v\sst{Ph}(\omega)$. This - in case of a laser with $\lambda = \omega \cdot c_0 = \SI{1064}{\nano\meter}$ - means that for the incoming beam with $\omega$ a SH can build at $\dfrac{\lambda}{2} = 2\omega = \SI{532}{\nano\meter}$.\\

As mentioned above the process is usually done with BBO-crystals, which have a single optical axis, making it easier to produce a SH; the SH only builds up, if the light of the incoming beam is send through the BBO with the correct angle. This is caused by a different reflective index for the first harmonic $n(\omega)$ and the second harmonic $n(2\omega)$. It is possible to find an angle between the laser beam and optical axis of the BBO, so that the $\vec{k}$ vectors are aligned. Therefor the eliptical equation
\begin{align}
   \frac{1}{n^2_a(\theta)}=\frac{cos^2(\theta)}{n^2_o}+\frac{sin^2(\theta)}{n^2_a}
    \label{eq:elip}
\end{align}
is important. In this case $n(\omega)$ is the refractive index $n_o$ for the ordinary beam and $n(2\omega)$ is the refractive index $n_a$ for the extraordinary beam.\\
Once the \enquote{coupling} angle is right, the phase-matching condition above automatically is satisfied.

\subsection{Frequency mixing and THG}
In order to create third harmonics (TH) of the incoming light, instead of creating it directly from the fundamental it is mixed from the SH and fundamental. Since the $\chi_3 E^3$ addend from \eq{polar} needs really high beam intensities to work (due to a low value of $\chi_3$) the THG instead is created by mixing the fundamental with its SH. This increases efficiency in the power output of the setup (i.e. the TH).\\
Again the mixing process is based on the phase-matching conditions described in \eq{phase_match}. The condition for the mixing however comes down to
\begin{align}
    n_3\cdot 3\omega_1 = n_1\omega_1 + n_2 \cdot 2\omega_1
    \label{eq:thg_phase}
\end{align}
because $\omega_2 = 2\omega_1$ (SHG) and $\omega_3 = 3\omega_1$ since this is the 3rd harmonic being created. \\

It has to be said, that for each harmonic you want to generate, you will need a different crystal. This comes from the fact, that on the one hand the phase-matching conditions are different (see \eq{shg_phase} and \eq{thg_phase}). Therefor, depending on which harmonic you want to create, the crystaline structure needs to be different. On the other hand, even when you want to create the same harmonics of different incoming beams (i.e. beams with different wavelengths), you need to change the crystal due to the refractive indices being related not only to material constants but also to the frequency of the wave travelling through it.\\
To summarize a BBO for e.g. \SI{1064}{\nano\meter} would therefor not work to create a SH from an incoming beam with \SI{532}{\nano\meter}.

\section{The Nd:YAG laser and Q-switching}
One of the most efficient and common ways to build a laser system is the Nd:YAG-Laser. It is found in many laboratories since the setup itself is rather compact and easy to operate compared to e.g. dye-lasers (toxic dyes) or a flour laser (toxic $F_2$-Gas).

\subsection{Operating principle}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{laser.png}}
	\captionof{figure}{Schematic setup of a laser with the mirrors (Spiegel) surrounding the laser medium (aktives Medium) \bib{laser}}
	\label{fig:laser}
\end{minipage}
\vspace{0.5cm}

As the name suggests a Neodymium:YAG-laser creates its radiation from $\ce{Nd^{3+}}$-Ions which are embedded in a YAG(Ytterbium-Aluminium-Garnet)-crystal. It has a high gain and useful mechanical properties which make it an excellent material for different laser systems.\\

In general a laser (light amplification by stimulated emission of radiation) system works with a so called \enquote{pump}-transition which stimulates the ions bringing them into a higher energy level. This is usually done with a flash lamp. Since this energy level is instable the ions then recombine to lower energy levels. Whilst recombining they emit photons with the frequency according to the energy difference of the higher and lower level of the laser transition. Important to note is that this process alone does not suffice to build a laser, since the emitted photons are not directed. At this stage, they emit into all directions.\\
\fig{laser} shows how the emitted photons are mechanically directed by a mirror setup - a so called resonator. In this way the photons, that are reflected by the mirrors in a ninety degree angle, are redirected into the medium where they then can strike other emitted photons and transfer their impulse - and with this the movement direction. Passing through the medium the photons hit another mirror and bounce back through the laser medium. The newly emitted photons from the laser medium are therefor physically \enquote{pushed} along the laser axis (Laserstrahl).\\

The mirrors coatings as well as the length of the resonator ($d$) are usually optimized for the wavelength of the laser transition. This way the photons are amplified, because the bouncing inside the resonator builds a standing wave. Once new photons are emitted, due to the sheer amount of photons already bouncing between the mirrors, they are quickly pushed into the laser beam and can amplify new photons themselfs.\\
As soon as the lasing (standing wave is stable) starts, the power inside the resonator builds up rapidly and some of the photons instead of reflecting on the mirrors, decouple from the resonator by transmitting through the mirrors. This comes down to the fact that the mirrors of the resonator don't have a reflectivity of \SI{100}{\percent}. This low percentage of the light decoupled from the resonator is known as the laserlight that we can see coming out of a laser system.

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{thermschema_ndyag.png}}
	\captionof{figure}{Energy levels of a YAG-crystal and its laser transitions \bib{thermschema}}
	\label{fig:thermschema}
\end{minipage}
\vspace{0.5cm}

In our specific case we are driving the pump-transition in the Nd:YAG-crystal with a flash lamp that lifts the Ions into one of the high pump states (\fig{thermschema}b). Since these pump states are very instable the ions quickly drop into the upper state of the laser transition - the $\ce{^4F_{3/2}}$ state. This is a non-radiant process and the energy, which is created by the transition from the pump state into the $\ce{^4F_{3/2}}$ state, is absorbed by the crystal as heat.\\
Once the ions enter the $\ce{^4F_{3/2}}$ state they have a lifespan of roughly \SI{230}{\micro\second} before they decay into the $\ce{^4I_{11/2}}$ state. In this time the electrons gather in the $\ce{^4F_{3/2}}$ state until a photon stimulates them to collapse into the $\ce{^4I_{11/2}}$ state. This laser transition ($\ce{^4F_{3/2}} \rightarrow \ce{^4I_{11/2}}$) emits photons with a wavelength of \SI{1064,1}{\nano\meter}. From the $\ce{^4I_{11/2}}$ state the ions decay once more back into their ground state very quickly and the process can be repeated. The whole process is shown in \fig{thermschema}.

\subsection{Q-switching}
A Q-switch (Q = quality) is used to shorten the emission time of the laser leading to a higher power. This is especially interesting for nonlinear optics. There are mainly two possibilities to achieve this. First by cutting off the laser beam and second by rising the internal loss in the medium. The idea is to only let the oszillation (cause of laser) happen, if the maximum of the population inversion is reached. At the end of the pump puls the resonator is opened. Thus a high power and short laser puls is emitted.\\
Usually this is done with an acousto-optical resonator, which blocks the beam on one side of the laser medium. This way the laser medium accumulates a lot of energy, since the lasing is stopped and the wave can't be decoupled from the resonator. Once the acousto-optical modulator is turned off the beam is transmitted through the modulator and the power build up in the laser medium is decoupled from the resonator all at once, creating a very high energy pulse like described above.\\
By frequently switching the acousto-optical modulator (Q-switch) on and off, a CW-laser (continious wave laser) can be turned into a pulsed laser with a set repetition rate.

\newpage

\begin{thebibliography}{9}

    \bibitem{}Jens Bahn, Nichtlineare Effekte und Farbstofflaser, Institut für Physik, 2010 (Users Manual) [24.04.2022]
    \label{bib:thermschema}
    \bibitem{}Prof. Dr. Stefan Lochbrunner, Vorlesungsunterlagen zu Experimentalphysik 4 - Atome und Moleküle, Institut für Physik, 2021 [25.04.2022]
    \label{bib:laser}

\end{thebibliography}

\section*{Erklärung der selbstständigen Anfertigung}
Hiermit erklären wir, dass das vorliegende Protokoll selbständig verfasst und keine anderen als die angegebenen Hilfsmittel verwendet wurden. \\
Insbesondere versichern wir, dass alle genutzten Internetseiten kenntlich gemacht wurden und im Verzeichnis der Internetseiten bzw. im Quellenverzeichnis aufgeführt.\\
Sämtliche Schaltskizzen, Abbildungen sowie Oszilloskopaufnahmen sind selbstständig angefertigt, oder der Versuchsanleitung entnommen. 

\end{document}
