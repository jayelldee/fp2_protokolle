%% Clean Workspace
clear;
clc;
load('mspec.mat','-mat');

%% Definitions, normalization and more
f1 = figure('Name','Krypton Spektrum');
f2 = figure('Name','Schwefelhexaflourid Spektrum');
f3 = figure('Name','Probe 1 Spektrum');
f4 = figure('Name','Probe 2 Spektrum');
f5 = figure('Name','Schwefelhexaflourid Spektrum 2');
mz = [41,42,43,80,82,83,84,86];
%% 1.1 Krypton Plot
kr_x = [0.632 0.6402 0.6493 0.9114 0.9233 0.9288 0.9342 0.9461];
kr_y = [1.058 1.238 1.076 1.072 1.305 1.318 2.331 1.428];
kr_labels ={'^{82}Kr^{2+}', '^{84}Kr^{2+}', '^{86}Kr^{2+}', '^{80}Kr^{+}', '^{82}Kr^{+}', '^{83}Kr^{+}', '^{84}Kr^{+}', '^{86}Kr^{+}'};
figure(f1);
plot(Kr_U, Kr_I, 'k-')
hold on
plot(kr_x, kr_y, '.', 'Color', 'Red')
hold off
ylabel ('Intensit�t / (a.u.)')
ylim([0.9 2.4])
xlabel ('Betriebsspannung U / kV')
xlim([0.6 1]);
text(kr_x,kr_y,kr_labels,'VerticalAlignment','bottom','HorizontalAlignment','center')
grid on

%% 1.2 SF6 Plot
sf6_x = [0.4237 0.59 0.72 0.7414 0.8501 0.961 1.063 1.155];
sf6_y = [2.264 1.476 2.571 1.721 1.843 3.957 1.756 9.9];
sf6_labels ={'F^+','SF_2^{2+}', 'SF^{+}', 'SF_4^{2+}', 'SF_2^{+}', 'SF_3^{+}', 'SF_4^{+}', 'SF_5^{+}'};
figure(f2);
plot(sf6_2_U, sf6_2_I, 'k-')
hold on
plot(sf6_x, sf6_y, '+', 'Color', 'Red')
hold off
ylabel ('Intensit�t / (a.u.)')
ylim([0.9 10])
xlabel ('Betriebsspannung U / kV')
xlim([0.3 1.3]);
text(sf6_x+0.015,sf6_y,sf6_labels,'VerticalAlignment','top','HorizontalAlignment','center')
grid on



%% 1.3 Probe1 Plot
% p1_x = [0.632 0.6402 0.6493 0.9114 0.9233 0.9288 0.9342 0.9461];
% p1_y = [1.058 1.238 1.076 1.072 1.305 1.318 2.331 1.428];
% p1_labels ={'^{82}Kr^{2+}', '^{84}Kr^{2+}', '^{86}Kr^{2+}', '^{80}Kr^{+}', '^{82}Kr^{+}', '^{83}Kr^{+}', '^{84}Kr^{+}', '^{86}Kr^{+}'};
figure(f3);
plot(Prob1_U, Prob1_I, 'k-')
% hold on
% plot(p1_x, p1_y, '.', 'Color', 'Red')
% hold off
ylabel ('Intensit�t / (a.u.)')
ylim([0.9 2.4])
xlabel ('Betriebsspannung U / kV')
xlim([0.4 1.25]);
% text(p1_x,p1_y,p1_labels,'VerticalAlignment','bottom','HorizontalAlignment','center')
grid on

%% 1.4 Probe2 Plot
% p2_x = [0.632 0.6402 0.6493 0.9114 0.9233 0.9288 0.9342 0.9461];
% p2_y = [1.058 1.238 1.076 1.072 1.305 1.318 2.331 1.428];
% p2_labels ={'^{82}Kr^{2+}', '^{84}Kr^{2+}', '^{86}Kr^{2+}', '^{80}Kr^{+}', '^{82}Kr^{+}', '^{83}Kr^{+}', '^{84}Kr^{+}', '^{86}Kr^{+}'};
figure(f4);
plot(Prob2_U, Prob2_I, 'k-')
% hold on
% plot(p2_x, p2_y, '.', 'Color', 'Red')
% hold off
ylabel ('Intensit�t / (a.u.)')
ylim([0.9 2.4])
xlabel ('Betriebsspannung U / kV')
xlim([0.3 1.3]);
% text(p2_x,p2_y,p2_labels,'VerticalAlignment','bottom','HorizontalAlignment','center')
grid on

%% 1.5 SF6_2 Plot
sf6_x = [0.4238 0.721 0.743 0.8518 0.9634 1.063 1.155];
sf6_y = [1.153 1.171 1.066 1.085 1.329 1.756 9.9];
sf6_labels ={'F^+', 'SF^{+}', 'SF_4^{2+}', 'SF_2^{+}', 'SF_3^{+}', 'SF_4^{+}', 'SF_5^{+}'};
figure(f5);
plot(sf6_x_korr, sf6_y_korr, 'k-')
hold on
plot(sf6_x, sf6_y, '+', 'Color', 'Red')
hold off
ylabel ('Intensit�t / (a.u.)')
ylim([0.9 11])
xlabel ('Betriebsspannung U / kV')
xlim([0.4 1.3]);
text(sf6_x+0.015,sf6_y,sf6_labels,'VerticalAlignment','bottom','HorizontalAlignment','center')
grid on

%% 2 Fitting
% %Linear-Fit
% f_lin = fit(x_beta,Z_beta,'poly1')
% 
% %Plotting the linear fit
% plot(f_lin,x_beta, Z_beta, 'k+')
% legend('Datenpunkte','Linearer Fit')
% ylabel ('Zählrate Z(x) / 1')
% ylim([9000, 14000])
% xlabel ('Stellschraubenposition x / \mum')
% xlim([-5 505]);
% grid on


% %Fit-Formel aus Versuchsanleitung
%lambda_approx_va = ['(1/4).*(sin(a.*sin(((x-248).*10.^(-6))./0.489))./(a.*sin(((x-248).*10.^(-6))./0.489))).^2'...
%'*(sin(2.*b.*sin(((x-248).*10.^(-6))./0.489))./sin(b.*sin(((x-248).*10.^(-6))./0.489))).^2+c'];

% %Fitoptionen
% fo = fitoptions('Method','NonlinearLeastSquare',...
%     'Lower',[0,0,0],'Upper',[Inf,Inf,1],...
%     'DiffMinChange',1.0e-8,...
%     'DiffMaxChange',0.1,...
%     'MaxFunEvals',600,...
%     'MaxIter',400,...
%     'TolFun',1.0e-6,...
%     'TolX',1.0e-6,...
%     'StartPoint',[0.1473,0.6013,0.5325]);
% ft1 = fittype(lambda_approx_va,'options',fo);

% %Fit
% lambdafit_va = fit(x,Z_kor,ft1);
% 
% 
% %Fit-Plot
% plot(lambdafit_va,x, Z_kor, 'k+')
% legend('Datenpunkte', 'Lambda-Fit')
% ylabel ('Zählrate Z(x) / 1')
% ylim([0, 1.05])
% xlabel ('Stellschraubenposition x / \mum')
% xlim([-20 520]);
% grid on