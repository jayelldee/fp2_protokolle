\documentclass[a4paper,10pt,headsepline=2pt,footinclude=false]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{graphicx}
\graphicspath{{Bilder/}}
\usepackage{xcolor}
\usepackage[left=2cm,right=1.5cm,top=2.5cm,bottom=2cm]{geometry}
\usepackage{scrlayer-scrpage}
\usepackage[version=3]{mhchem}
\usepackage{csquotes}
\usepackage{mdframed}
\usepackage{multirow}
\usepackage{caption}
\usepackage{hyperref}
\usepackage{siunitx}
\sisetup{locale = DE, per-mode = fraction, separate-uncertainty}
\usepackage{ulem}
\usepackage{appendix}
\usepackage{float}
\usepackage{subfig}
\usepackage{enumitem}
\usepackage{helvet}
\newcommand{\sst}[1]{_{\text{#1}}}
\newcommand{\ra}{$\rightarrow$ }
\newcommand{\gl}[1]{Gl.(\ref{eq:#1})}
\newcommand{\abb}[1]{Abb. \ref{fig:#1}}
\newcommand{\tab}[1]{Tab. \ref{tab:#1}}
\newcommand{\bib}[1]{[\ref{bib:#1}]}
\renewcommand{\familydefault}{\sfdefault}
\newcommand*\myappendixchanges{%
  \setcounter{table}{0}%
  \gdef\thesection{A.\arabic{section}}%
  \gdef\thetable{A.\arabic{table}}}


\lohead{\includegraphics[width=4cm]{logo}}			%Logo in der Kopfzeile
\cohead{Fortgeschrittenenpraktikum 2\\[0.2cm] Alex Kreis \& Jannik Dierks - Gruppe 1}	%Text in der Kopfzeile
\rohead{\thepage}									%Seitenzahl
\cfoot[]{}											%keine Fußzeile
\setkomafont{headsepline}{\color{red!70!black}}		%Farbe der Trennlinie	
\setkomafont{pagehead}{\bfseries}					%Im Kopf Fett geschrieben
\usepackage{colortbl}								%bringt Farbe in die Tabelle
\definecolor{dunkelgrau}{gray}{0.8}					%Farbe definieren
\definecolor{hellgrau}{gray}{0.9}					%Farbe definieren
\definecolor{rot}{rgb}{0.616,0.051,0.082}			%Farben definieren 0-100%

%Links zu Gleitumgebung von figure und table:
%http://tex.lickert.net/tipps/optionh/optionh.html
%https://www.texwelt.de/fragen/3427/wann-sollte-ich-gleitumgebungen-fur-tabellen-und-abbildungen-verwenden

\setlength\parindent{0pt}

%--------------------------------------------------------------------------------
%	DOKUMENT
%--------------------------------------------------------------------------------
\begin{document}

\renewcommand{\figurename}{Abb.}
\counterwithin{figure}{section}
\renewcommand{\tablename}{Tab.}
\counterwithin{table}{section}
\counterwithin{equation}{section}

\begin{center}
	\huge \textbf{Massenspektrometrie}\\\vspace{3pt}
		\Large Erstautor: Jannik Dierks\\
	\large Zweitautor: Alex Kreis\\
	\Large Gruppe 1\\
	\vspace{0.5cm}
	\large Institut für Physik - Universität Rostock - \today\\
	Versuchsbetreuer*in: Herr Kempf
\end{center}

\tableofcontents
\vspace{0.5cm}

\section*{Ziel des Versuchs}
In diesem Versuch wird Massenspektrometrie an verschiedenen Substanzen durchgeführt. Ziel ist es ein besseres Verständnis für die Funktionsweise von Massenfiltern zu erlangen.
	
\section{Aufgaben}
\begin{enumerate}
    \item Kalibrieren Sie das Sektorfeldmassenspektrometer an einem bekannten Gas (Krypton).
    \item Nehmen Sie das Spektrum für Schwefelhexaflourid (\ce{SF6}) auf.
    \item Nehmen Sie das Spektrum für zwei verschiedene unbekannte Proben auf und ermitteln Sie die Zusammensetzung der Stoffe. Bestimmen Sie die Stoffe, um die es sich handelt.
\end{enumerate}


\section{Physikalische Grundlagen und experimenteller Aufbau}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.5\linewidth]{SFMS.png}}
	\captionof{figure}{Schematischer Aufbau eines Sektorfeldmassenspektrometers \bib{sfms}}
	\label{fig:aufbau_sfms}
\end{minipage}
\vspace{0.5cm}

Die Massenspektrometrie - hier durchgeführt mit einen Sektorfeldmassenspektrometer (SFMS) - dient der Analyse unbekannter Stoffe durch ihr Masse-Ladungs-Verhältnis, mithilfe dessen man die Stoffzusammensetzung insbesondere von größeren Molekülen bestimmen kann.\\
Um den Stoff analysieren zu können wird sich der Effekt der Elektronenstoß-Ionisation sowie die Ablenkung solcher Ionen in einem Magnetfeld (Lorentzkraft) zunutze gemacht. Dabei wird die Probe durch eine \enquote{Elektronenkanone} (Glühlampe) ionisiert und dann mittels verschiedener elektrischer Optiken zunächst beschleunigt und dann als Ionenstrahl in ein Magnetfeld fokussiert (s. \abb{aufbau_sfms}).\\

Innerhalb des Magnetfelds werden die Ionen dann durch die Lorentzkraft auf kreisförmige Flugbahnen gelenkt, deren Radius abhängig von dem Masse-Ladungs-Verhältnis der Ionen ist. Mathematisch betrachtet kann ein Kräftegleichgewicht zwischen der Zentripetalkraft $F_Z$ und der Lorentzkraft $F_L$ angenommen werden. Aus dem Gleichgewicht lässt sich schließen:
\begin{align}
    qvB &= m\dfrac{v^2}{r} \nonumber\\ 
    \dfrac{m}{q} &= \dfrac{Br}{v}
    \label{eq:ggw}
\end{align}
Wobei $m$ die Masse des Ionen, $q$ die Ladung des Ionen, $v$ die Geschwindigkeit der Ionen, $B$ das angliegende Magnetfeld und $r$ der Bahnradius ist.\\
Da die Ionen durch die elektrischen Optiken beschleunigt werden, kann die Geschwindigkeit der Ionen über die Energie des elektrischen Feldes ausgedrückt werden.
\begin{align}
    E\sst{Beschl} &= \dfrac{m}{2}v^2 \nonumber\\
    U_B \cdot q &= \dfrac{m}{2}v^2 \nonumber\\
    v^2 &= 2U_B\dfrac{m}{q}
    \label{eq:beschl}
\end{align}
Einsetzen von \gl{beschl} in \gl{ggw} führt zu
\begin{align}
    \dfrac{m}{q} = \dfrac{B^2r^2}{2U_B}.
    \label{eq:mq}
\end{align}
Zuletzt kann die Ladung $q$ in Vielfache der Elementarladung $e$ zerlegt werden
\begin{align*}
    q = z\cdot e
\end{align*}
wodurch \gl{mq} zu 
\begin{align}
    \dfrac{m}{z} = \dfrac{B^2r^2e}{2U_B}
    \label{eq:mz}
\end{align}
wird.\\

Um die Ionen messen zu können, wird eine Detektoreinheit benötigt. In unserem Fall wird dafür ein Sekundärelektronenvervielfacher oder \enquote{Faraday-Becher} verwendet. Dieser Faraday-Becher \enquote{fängt} die Ionen, die auf ihn treffen dabei ein und die überschüssigen Elektronen der Ionen werden verstärkt und über ein Strom- oder Spannungsmessgerät gemessen. Diese Signale erzeugen dann die Massen-Spektren der jeweiligen Stoffe. \\
Um den Faraday-Becher mechanisch nicht bewegen zu müssen, wird das Magnetfeld langsam verändert, um die Ablenkung der Ionen zu verändern. Scannt man das Magnetfeld kontinuierlich, wandern die einzelnen Ionenstrahlen irgendwann durch den Faraday-Becher und können detektiert werden. So kann der Faraday-Becher starr bleiben und man erhält trotzdem ein kontinuierliches Spektrum.\\

Da das Spektrum des SFMS ohne Referenz nicht klar zuzuordnen ist, muss das SFMS vor einer Messung kalibriert werden. Dafür benutzt man Stoffe, bei denen das Spektrum bekannt ist, bzw. Stoffe, von denen man die Zusammensetzung kennt. In unserem Fall wird das SFMS an Krypton kalibriert.\\
Die Steuerspannung $U$ des Magnetfelds skaliert linear mit der Magnetfeldstärke, sodass für die Kalibrierung ein quadratischer Zusammenhang zwischen dem Masse-Ladungs-Verhältnis und der Steuerspannung des Magneten angenommen wird, mit
\begin{align}
    \dfrac{m}{z} = aU^2 + bU + c
    \label{eq:kalibrierung}
\end{align}

\section{Durchführung}
\begin{enumerate}
    \item Der Aufbau wird hochgefahren. Dafür werden die Spannungen an der Ionenquelle, der Beschleunigungs- und Fokussiereinheit und dem Magneten eingeschaltet. Ferner wird die Detektoreinheit eingeschaltet und der Verstärker hinter dem Faraday-Becher hochgefahren.
    \item Es wird Luft durch den Gaseinlass in die Ionenquelle eingelassen und ein Spektrum am PC im zugehörigen Programm (Igor-Code) aufgenommen.
    \item Die Parameter der Beschleunigungs- und Fokussiereinheit werden optimiert, indem ein einzelner (möglichst hoher) Peak des Luft-Spektrums im Messprogramm wiederholend mit niedriger Auflösung gemessen wird. Es wird versucht die FWHM zu minimieren und gleichzeitig die absolute Amplitude zu erhöhen.
    \item Um die Kalibrierung durchzuführen, wird erst ein \ce{SF6}- (Schwefelhexafluorid) und dann ein \ce{Kr}-(Krypton)Spektrum aufgenommen. Dazu wird die jeweilige Gasflasche an den Gaseinlass angeschlossen und vermessen.
    \item Zuletzt wird das Verfahren von oben für zwei unbekannte Proben wiederholt. Es ist darauf zu achten, dass man das Einlassventil für die flüssigen Proben unter Umständen weiter öffnen muss, um genug Probenmaterial einzulassen.
\end{enumerate}
\newpage
\section{Resultate}
Mit Hilfe der Website Webbook.nist \bib{krypton} können die bekannten $\frac{m}{z}$ von Krypton gefunden werden. Aus der durchgeführten Spektroskopie von Krypton können die dazugehörigen Spannungen ermittelt werden. Diese Wertepaare können dann als Messpunkte für die Fitfunktion benutzt werden. 
\begin{table}[h]
\begin{center}
\begin{tabular}[h]{|c|c|c|c|}
\hline
Ion &   Symbol  & $\frac{m}{z}$	& $U$/\si{\kV} \\
\hline
1	& \ce{^{82}Kr^{2+}}  &	41	&	0,632\\
2   &   ?           &   ?   &   0,636\\
3	& \ce{^{84}Kr^{2+}}  &	42	&	0,6402\\
4	& \ce{^{86}Kr^{2+}}  &	43	&	0,6493\\
5	& \ce{^{78}Kr^{+}}  &	78	&	?\\
6	& \ce{^{80}Kr^{+}}  &	80	&	0,9114\\
7	& \ce{^{82}Kr^{+}}  &	82	&	0,9233\\
8   & \ce{^{83}Kr^{+}}  &  83  &   0,9288\\
9   & \ce{^{84}Kr^{+}}  &  84  &   0,9342\\
10   & \ce{^{86}Kr^{+}}  &   86  &   0,9461\\
\hline
\end{tabular}
\end{center}
\caption{Zuordung der Spannungen zu $\frac{m}{z}$ für Krypton}
\label{tab:krypton}
\end{table}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{Krypton_Spektrum.png}}
	\captionof{figure}{Massenspektrum von Krypton}
	\label{fig:spektrum_krypton}
\end{minipage}
\vspace{0.5cm}

Es gab bei der Auswertung einen Peak (Ion 2) der keinem ionisiertem Krypton-Isotop zugeordnet werden konnte. Von der Lage her würde es sich am ehesten um $^{83}Kr^{2+}$ handeln, obwohl es für diesen laut \bib{krypton} keinen erkennbaren Peak gibt. Der theoretische Peak für 78-Krypton kann nicht gefunden werden, da der Peak zu klein. Das ist wenig verwunderlich, da dieses Isotop nur eine Häufigkeit von 0,35\% zu den anderen Isotopen aufweist \bib{kryptonwiki}.

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{nist/krypton.png}}
	\captionof{figure}{Theoretisches Massenspektrum von Krypton}
	\label{fig:nist_krypton}
\end{minipage}
\vspace{0.5cm}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{Kalibrierungs_Fit.png}}
	\captionof{figure}{Quadratischer Fit für die Kalibrierung des SFMS}
	\label{fig:kalibrierung_sfms}
\end{minipage}
\vspace{0.5cm}

Mit dem Fit erhält man dann die Parameter:
\begin{align}
    a &= \SI{106,5}{} \nonumber\\
    b &= \SI{-24,9}{} \nonumber\\
    c &= \SI{14,26}{}
\end{align}
Der $R^2$-Wert liegt dabei bei \SI{1}{}.\\

Diese können dann mit \gl{kalibrierung} benutzt werden, um die Spannungen bei denen die Peaks der folgenden Proben zu finden sind in den spezifischen Quotienten $\frac{m}{z}$ umzurechnen.
\\
In der Schwefelhexafluorid-Probe konnten folgende Peaks beobachtet werden und mit Hilfe von webbook.nist und einigen Vermutungen wie folgt zugeordnet werden:

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{SF6_Spektrum.png}}
	\captionof{figure}{Massenspektrum von Schwefelhexafluorid}
	\label{fig:spektrum_sf6}
\end{minipage}
\vspace{0.5cm}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{SF6_Spektrum_2.png}}
	\captionof{figure}{Massenspektrum von Schwefelhexafluorid ohne Verunreinigungen}
	\label{fig:spektrum_sf6_clean}
\end{minipage}
\vspace{0.5cm}

\begin{table}[h]
\begin{center}
\begin{tabular}[h]{|c|c|c|c|c|c|}
\hline
Ion & $U$/\si{\kV}	& $\frac{m}{z}$ exp. & Symbol & $\frac{m}{z}$ theo. & Fehler in \%\\
\hline
1	&	0,424	&	22,849	&	\ce{F^+}	    &	19	& 20,26\\
2	&	0,562	&	33,904	&	\ce{SF_2^{2+}}	&	32	& 5,95\\
3	&	0,721	&	51,670	&	\ce{SF^{+}}	&	51	& 1,31\\
4	&	0,743	&	54,553	&	\ce{SF_4^{2+}}	&	53	& 2,93\\
5	&	0,852	&	70,354	&	\ce{SF_2^{+}}	&	70	& 0,51\\
6	&	0,963	&	89,046	&	\ce{SF_3^{+}}	&	89	& 0,05\\
7	&	1,062	&	107,932	&	\ce{SF_4^{+}}	&	108	& 0,06\\
8	&	1,155	&	127,574	&	\ce{SF_5^{+}}	&	127	& 0,45\\
\hline
\end{tabular}
\end{center}
\caption{Peaks der Schwefelhexafluorid-Probe}
\label{tab:schwefel}
\end{table}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{nist/schwefel.png}}
	\captionof{figure}{Theoretisches Massenspektrum von Schwefeltetrafluorid}
	\label{fig:nist_schwefel}
\end{minipage}
\vspace{0.5cm}

Nun werden die unbekannten Proben untersucht und mithilfe des SFMS bestimmt, welcher Stoff in der Probe enthalten war.\\
Das Spektrum für Probe 1 sieht wie folgt aus:

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{Probe1_Spektrum.png}}
	\captionof{figure}{Massenspektrum von Probe 1}
	\label{fig:spektrum_probe1}
\end{minipage}
\vspace{0.5cm}

\begin{table}[h]
\begin{center}
\begin{tabular}[h]{|c|c|c|c|c|c|}
\hline
Ion & $U$/\si{\kV}	& $\frac{m}{z}$ exp. & Symbol & $\frac{m}{z}$ theo. & Fehler in \%\\
\hline
1	&	0,4085	& 21,860 & \ce{F^{+}}	& 19 & 14,18\\
2	&	0,5129	& 29,505 & \ce{C_2H_6^+} & 30	& 2,65\\
3	&	0,5534	& 33,096 & \ce{SF_2^{2+}} und \ce{S^+} & 32	& 4,41\\
4	&	0,5811	& 35,753 & \ce{C_3^+} &	36 & 1,79\\
5   &   0,716   & 51,03 &  \ce{SF^+} & 51 & 0,16\\
6	&	0,7387	& 53,981 & \ce{SF^2+_4}	und \ce{C_4H_6^+}& 53 und 54 & 0,14\\
7	&	0,8474	& 69,636 & \ce{SF^2_+} und \ce{C_5H_10^+}& 70 & 0,52\\
8	&	0,9326	& 83,666 &	\ce{C_4H_5S^+} und \ce{C_6H_12^+}& 85 und 84 & 0,40\\
9	&	0,9624	& 88,938 & \ce{SF^+_3}	& 89 & 0,07\\
10	&	1,063	& 108,133 & \ce{SF^+_4} & 108 & 0,13\\
11	&	1,154	& 127,353 &	\ce{SF^+_5} & 127 & 0,28\\
\hline
\end{tabular}
\end{center}
\caption{Peaks des Spektrums von unbekannter Probe 1}
\label{tab:probe1}
\end{table}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{nist/hexen.png}}
	\captionof{figure}{Theoretisches Massenspektrum von Hexen}
	\label{fig:nist_hexen}
\end{minipage}
\vspace{0.5cm}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{nist/Thiirane.png}}
	\captionof{figure}{Theoretisches Massenspektrum von Thiirane}
	\label{fig:nist_thiirane}
\end{minipage}
\vspace{0.5cm}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{Probe2_Spektrum.png}}
	\captionof{figure}{Massenspektrum von Probe 2}
	\label{fig:spektrum_probe2}
\end{minipage}
\vspace{0.5cm}

\begin{table}[h]
\begin{center}
\begin{tabular}[h]{|c|c|c|c|c|c|}
\hline
Ion & $U$/\si{\kV}	& $\frac{m}{z}$ exp. & Symbol & $\frac{m}{z}$ theo. & Fehler in \%\\
\hline
1	&	0,3426	& 18,230 & \ce{H2O+} & 18 &1,28\\
2	&	0,3576	& 18,975 & \ce{F+} und \ce{H3O+} & 19 & 0,13\\
3	&	0,3704	& 19,648 & \ce{FH+} & 20 & 1,76\\
4	&	0,5047	& 28,821 & \ce{C2H5+} & 29 & 0,62\\
5	&	0,5132	& 29,531 & \ce{C2H6+} & 30 &1,56\\
6	&	0,5239	& 30,446 & \ce{CH3O+} & 31 &1,79\\
7	&	0,5537	& 33,124 & \ce{SF_2^2+} und \ce{S+} & 32 &3,61\\
8	&	0,6177	& 39,515 & \ce{C3H3+} & 39 &1,32\\
9	&	0,6262	& 40,429 & \ce{C3H4+} & 40 &1,07\\
10	&	0,6348	& 41,370 & \ce{C3H5+} & 41 &0,9\\
11	&	0,6433	& 42,315 & \ce{C2H2O+} & 42 &0,75\\
12	&	0,6518	& 43,276 & \ce{C2H3O+} & 43 &0,64\\
13	&	0,752	& 55,761 & \ce{C4H7+} & 55 &1,38\\
14	&	0,7584	& 56,632 & \ce{C4H4OH+} und \ce{C4H9+} & 57 &0,65\\
15	&	0,8522	& 70,385 & \ce{SF2+} und \ce{C5H10+} & 70 &0,55\\
16	&	0,865	& 72,407 & \ce{C5H12+} & 72 &0,57\\
\hline
\end{tabular}
\end{center}
\caption{Peaks des Spektrums von unbekannter Probe 2}
\label{tab:probe1}
\end{table}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{nist/pentan.png}}
	\captionof{figure}{Theoretisches Massenspektrum von Pentan}
	\label{fig:nist_pentan}
\end{minipage}
\vspace{0.5cm}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{nist/1-pentanol.png}}
	\captionof{figure}{Theoretisches Massenspektrum von 1-Pentanol}
	\label{fig:nist_pentanol}
\end{minipage}
\vspace{0.5cm}

\section{Diskussion}
Bei der Kalibrierung mit Krypton fällt auf, dass das theoretische Spektrum dem experimentell bestimmten sehr ähnlich sieht. Die einzigen Unterschiede sind der Fehlende Peak bei $\frac{m}{z}=78$ und der zusätzliche Peak bei $U=0,636kV$. Diese Unterschiede wurden unter \abb{spektrum_krypton} diskutiert, da dies dort für die Kalibrierung notwendig war.
Die Kalibrierung konnte ideal erfolgen mit dem Bestwert $R^2=1$. Damit ist die Umrechnung von der Spannung $U$ zum Quotienten $\frac{m}{z}$ sehr genau.\\
Die Messung der Schwefelhexafluorid Probe musste in mehreren Schritten erfolgen. Zunächst wurde nur bis $U=1kV$ gemessen, wobei sich später herausstellte, dass noch zwei weitere Peaks im höheren
Spannungsbereich gefunden werden konnten. Diese erste Messung wurde nach der Krypton Messung durchgeführt, weshalb noch die Peaks von 82-,83-,84- und 86-Krypton sichtbar sind. Die zweite Messung \abb{spektrum_sf6} wurde nach der Messung der unbekannten Probe 2 durchgeführt, was zu einer starken Verunreinigung des Spektrums im unterem Spannungsbereich geführt hat. Die letzten zwei Peaks sind jedoch ungestört. Deshalb wurden die Werte für $U>1kV$ an die Messreihe angehängt, um ein sauberes Spektrum zu erhalten (siehe \abb{spektrum_sf6_clean}). Beim Vergleich mit dem theoretischem Spektum \abb{nist_schwefel} fällt auf, dass jeweils zwei Peaks mit höherer Masse bei $\frac{m}{z}=70$ und $\frac{m}{z}=89$ nicht sichtbar sind. Es handelt sich wahrscheinlich um Isotope von Schwefel oder Fluor die sich um die Masse 1u oder 2u unterscheiden. Diese Peaks sind allerdings so gering, da diese Isotope wahrscheinlich selten auftreffen, sodass sie vermutlich im \enquote{Rauschen verschwinden}. Des weiteren war nur das Massenspektrum für Schwefeltetrafluorid auf der NIST-Webseite verfügbar und nicht das für Schwefelhexafluorid, weshalb die beiden letzten Peaks nicht verglichen werden konnte. Der theoretische Wert wurde ermittelt, indem jeweils die Masse von Fluor (19u) auf den vorherigen Peak addiert wurde. Es ist außerdem auffällig, dass der Fehler für $\ce{F^+}$ sehr hoch ist. Das kann daran liegen, dass ein falsches Ion zugeordnet wurde oder dass der Fokus für diesen Bereich nicht ideal gewählt war. Es ist generell auffällig, dass bei niedrigen $\frac{m}{z}$ der Fehler größer wird.\\

Die erste unbekannte Probe wurde nach der Schwefelhexafluorid Probe gemessen, weshalb einige Peaks, insbesondere $\frac{m}{z}=89;108;127$ noch zu Schwefelhexafluorid zugeordnet werden können.\\
Solche Verunreinigungen können vermieden werden, indem noch länger gewartet wird oder mehr vom neuen Stoff in die Kammer eingelassen wird, da das Abpumpen bei wenigen Molekülen ein zufälliger Prozess ist. Eine weitere Möglichkeit wäre es die Kammer mit einem anderen Stoff zu \enquote{fluten}, der nur einen oder wenige bekannte Peaks in uninteressanten Bereichen hat. Danach wird wieder vakuumiert und es sollte nur dieser Stoff übrig bleiben. Dieser Prozess wäre mit ausspülen vergleichbar.\\
Es kann aber auch eien Überlagerung von mehreren Peaks geben. So kommen je nach Interpretation zwei verschiedene Stoffe in Frage.\\
Zum einen kann es Hexen (\abb{nist_hexen}) zugewiesen werden. Dabei wäre bei $\frac{m}{z}=70$ eine Überlagerung der Peaks \ce{SF^2_+} und \ce{C_5H_10^+} oder ein reiner \ce{C_5H_{10}^+} Peak zu beobachten. Bei Thiirane verhält es sich ähnlich beim \ce{SF^2_2+} und \ce{S^+} Peak, wobei ein \ce{F^+} auf diesen Stoff deuten würde (\abb{nist_thiirane}). Vergleicht man diese theoretischen Spektren der beiden Möglichkeiten, spricht vielleicht die geringere Menge an Peaks (in ähnlichen Bereichen wie die experimentell ermittelten Peaks) dafür, dass Thiirane eher der unbekannten Probe entspricht.\\
Alles in Allem kann auch hier beobachtet werden, dass die relativen Fehler der experimentell bestimmten Masse-Ladungs-Verhältnisse sinkt, wenn die absoluten Werte von $\dfrac{m}{z}$ sinken. Auch für Probe 1 liegt die Vermutung nahe, dass der Peak bei $\dfrac{m}{z} = \SI{21,860}{}$ entweder falsch zugeordnet wurde oder weit außerhalb des Fokus aufgenommen wird, da der relative Fehler zwei Größenordnungen größer ist, als die Fehler für höhere $\dfrac{m}{z}$.
\\

Die Peaks der Probe 2 (\abb{spektrum_probe2}) deuten auf eine organische Verbindung hin, da viele Peaks dicht beieinander liegen. Das kann vor allem dann vorkommen, wenn bei organischen Stoffen einzelne Wasserstoffatome hinzukommen und so das $\dfrac{m}{z}$-Verhältnis um immer genau eins erhöht wird.\\
Der höchste Peak des Spektrums ist sehr ähnlich zu dem Pentan Molekül (Vgl. \abb{nist_pentan}). Betrachtet man auf der anderen Seite das 1-Pentanol Spektrum (\abb{nist_pentanol}) ist bei $\dfrac{m}{z} = \SI{18}{}$ ein Peak für Wasser zu erkennen. Dieser Peak fehlt im Pentan Spektrum, tritt jedoch deutlich in unserem gemessenen Spektrum für Probe 2 auf. Es ist in diesem Fall schwierig mit absoluter Sicherheit zu sagen, um welchen Stoff es sich handelt, da gerade Peaks, die in den Theorie-Spektren von Pentan und Pentanol schon kleine Intensitäten hatten, im Untergrundrauschen unserer Messung verschwunden sein können oder generell andere relative Amplituden gehabt haben können. Dennoch ist zu beobachten, dass 13 der 16 Peaks, die wir in unserer Messung identifiziert haben sich im 1-Pentanol Spektrum wiederfinden lassen ($\dfrac{m}{z} = \{18,29,30,31,32,39,40,41,42,43,55,57,70\}$) während das NIST-Spektrum für Pentan nur 10 der 16 Peaks aus unserer Messung wiederspiegelt ($\dfrac{m}{z} = \{29,30,39,40,41,42,43,55,57,72\}$). Demnach schließen wir für Probe 2 auf 1-Pentanol.\\
Für die Fehler ist auffällig, dass alle relativen Fehler der Messung in diesem Fall unter \SI{10}{\percent} lagen, die meisten sogar unter \SI{1}{\percent}, was für eine sehr gute Messung spricht. 


\begin{thebibliography}{9}

    \bibitem{} \label{bib:sfms}
    \href{https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Mass_Spectrometer_Schematic_DE.svg/640px-Mass_Spectrometer_Schematic_DE.svg.png?1651741796397}{https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Mass\_Spectrometer\_Schematic\_DE.svg/640px-Mass\_Spectrometer\_Schematic\_DE.svg.png?1651741796397} [05.05.2022]
    
    \bibitem{} \label{bib:krypton}
    \href{https://webbook.nist.gov/cgi/cbook.cgi?Name=Krypton&Units=SI&cMS=on}{https://webbook.nist.gov/cgi/cbook.cgi?Name=Krypton\&Units=SI\&cMS=on} [09.05.2022]
    
    \bibitem{} \label{bib:kryptonwiki}
    \href{https://de.wikipedia.org/wiki/Krypton}{https://de.wikipedia.org/wiki/Krypton} [09.05.2022]
\end{thebibliography}

\section*{Erklärung der selbstständigen Anfertigung}
Hiermit erklären wir, dass das vorliegende Protokoll selbständig verfasst und keine anderen als die angegebenen Hilfsmittel verwendet wurden. \\
Insbesondere versichern wir, dass alle genutzten Internetseiten kenntlich gemacht wurden und im Verzeichnis der Internetseiten bzw. im Quellenverzeichnis aufgeführt.\\
Sämtliche Schaltskizzen, Abbildungen sowie Oszilloskopaufnahmen sind selbstständig angefertigt, oder der Versuchsanleitung entnommen. 

\end{document}
