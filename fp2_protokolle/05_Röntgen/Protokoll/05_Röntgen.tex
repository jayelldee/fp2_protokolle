\documentclass[a4paper,10pt,headsepline=2pt,footinclude=false]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{graphicx}
\graphicspath{{Bilder/}}
\usepackage{xcolor}
\usepackage[left=2cm,right=1.5cm,top=2.5cm,bottom=2cm]{geometry}
\usepackage{scrlayer-scrpage}
\usepackage[version=3]{mhchem}
\usepackage{csquotes}
\usepackage{mdframed}
\usepackage{multirow}
\usepackage{caption}
\usepackage{hyperref}
\usepackage{siunitx}
\sisetup{locale = DE, per-mode = fraction, separate-uncertainty}
\usepackage{ulem}
\usepackage{appendix}
\usepackage{float}
\usepackage{subfig}
\usepackage{enumitem}
\usepackage{helvet}
\newcommand{\sst}[1]{_{\text{#1}}}
\newcommand{\ra}{$\rightarrow$ }
\newcommand{\gl}[1]{Gl.(\ref{eq:#1})}
\newcommand{\abb}[1]{Abb. \ref{fig:#1}}
\newcommand{\tab}[1]{Tab. \ref{tab:#1}}
\newcommand{\bib}[1]{[\ref{bib:#1}]}
\renewcommand{\familydefault}{\sfdefault}
\newcommand*\myappendixchanges{%
  \setcounter{table}{0}%
  \gdef\thesection{A.\arabic{section}}%
  \gdef\thetable{A.\arabic{table}}}


\lohead{\includegraphics[width=4cm]{logo}}			%Logo in der Kopfzeile
\cohead{Fortgeschrittenenpraktikum 2\\[0.2cm] Alex Kreis \& Jannik Dierks - Gruppe 1}	%Text in der Kopfzeile
\rohead{\thepage}									%Seitenzahl
\cfoot[]{}											%keine Fußzeile
\setkomafont{headsepline}{\color{red!70!black}}		%Farbe der Trennlinie	
\setkomafont{pagehead}{\bfseries}					%Im Kopf Fett geschrieben
\usepackage{colortbl}								%bringt Farbe in die Tabelle
\definecolor{dunkelgrau}{gray}{0.8}					%Farbe definieren
\definecolor{hellgrau}{gray}{0.9}					%Farbe definieren
\definecolor{rot}{rgb}{0.616,0.051,0.082}			%Farben definieren 0-100%

%Links zu Gleitumgebung von figure und table:
%http://tex.lickert.net/tipps/optionh/optionh.html
%https://www.texwelt.de/fragen/3427/wann-sollte-ich-gleitumgebungen-fur-tabellen-und-abbildungen-verwenden

\setlength\parindent{0pt}

%--------------------------------------------------------------------------------
%	DOKUMENT
%--------------------------------------------------------------------------------
\begin{document}

\renewcommand{\figurename}{Abb.}
\counterwithin{figure}{section}
\renewcommand{\tablename}{Tab.}
\counterwithin{table}{section}
\counterwithin{equation}{section}

\begin{center}
	\huge \textbf{Zweispaltinterferenzversuch mit einzelnen Photonen}\\\vspace{3pt}
	\Large Erstautor: Alex Kreis\\
	\large Zweitautor: Jannik Dierks\\
	\Large Gruppe 1\\
	\vspace{0.5cm}
	\large Institut für Physik - Universität Rostock - \today\\
	Versuchsbetreuer*in: Herr Dr. Hartwig
\end{center}

\tableofcontents
\vspace{0.5cm}

\section*{Ziel des Versuchs}
\begin{enumerate}[label=\alph*)]
	\item Die Energieverteilung von Röntgenquanten, die eine Röntgenröhre aussendet, wird mit einem Kristallspektrometer bestimmt.
	\item Außerdem wird das energieabhängige Verhalten der Röntgenabsorption einiger Metalle in der Nähe ihrer Absorptionskanten untersucht.
	\item Die charakteristische Strahlung der Röntgenröhre wird verwendet, um die Gitterparameter einfacher Kristallstrukturen anhand ihrer Beugungsdiagramme zu ermitteln. 
\end{enumerate}
\newpage

\section{Aufgabenstellung}
\begin{itemize}
    \item Aufnahme des Spektrums der Cu-Röhre mittels Beugung am Einkristall. Überprüfen der
    Energien, Wellenlängen der $K_\alpha$- und $K_\beta$-Linien des Kupfers. Erläutern Sie das Abtrennen
    der $Cu-K_{\alpha}$ Strahlung durch Bragg-Reflexion am Einkristall (Kristallmonochromator).
    \item Untersuchung der Energieabhängigkeit der Röntgenabsorption einiger Metalle. Messung
    über die jeweilige K-Kante hinweg und ggf. Beobachtung der Wirkung auf die Höhe der $K_{\alpha,\beta}$-
    Linien. Demonstrieren Sie das Abtrennen der $Cu-K_{\alpha}$ Strahlung mit Ni-Filter entweder am
    Eintritts- oder Detektorschlitz.
    \item Untersuchung des Einflusses der Röhrenspannung auf das gemessene Röntgenspektrum.
    Änderung der Grenzenergie $E_{max}$ (Grenzwellenlänge $\lambda_{min}$) nach dem \textsc{Duane-Hunt}-Gesetz.
    \item Bestimmen Sie den kristallinen Aufbau gegebener polykristalliner Materialien aus deren
    \textsc{Debye-Scherrer}-Diagrammen (Gittertyp, -parameter). Sie verwenden die \textsc{Bragg-Brentano}-Geometrie und nutzen das ganze Röhrenspektrum. Überprüfen Sie die Maße der Einheitszelle auch anhand der Massendichte des Materials.
\end{itemize}


\section{Physikalische Grundlagen} 
Röntgenstrahlen sind elektromagnetische Wellen mit ungefähr $\SI{30}{\pico\meter}\leq\lambda\leq\SI{1}{\nano\meter}$. Um diese hochenergetische Strahlung zu erzeugen, wird eine Röntgenröhre benutzt. Dabei werden Elektronen thermisch aus dem Kathodenmaterial gelöst und mit einer Anodenspannung $U_A$ beschleunigt. Diese treffen auf das Anodenmaterial und geben Röntgenstrahlung ab. Das daraus folgende Spektrum kann in zwei Anteile unterschieden werden: das kontinuierliche Spektrum der Bremsstrahlung und das Linienspektrum der charakteristischen Linien. Bei der Bremsstrahlung wird das Elektron in der Nähe des Atomkerns durch die Coulomb-Kraft abgelengt und ausgebremst. Die verlorene Energie wird dann in Form von Röntgenquanten ausgesendet. Elektronen können auch untereinander ihre Energie umverteilen und dann auch wieder in Atomnähe abgebremst werden. Wenn ein Elektron seine gesamte Energie in Röntgenquanten umwandelt, ist die maximale Energie, bzw. die minimale Wellenlänge erreicht. Der Zusammenhang zwischen Anodenspannung und minimale Wellenlänge $\lambda_{min}$ wird durch das \textsc{Duane-Hunt}-Gesetz beschrieben

\begin{align}
    U_A\cdot\lambda_{min}=\frac{hc}{e}=\SI{1,239842}{\kilo\electronvolt\nano\meter}
    \label{eq:duane}
\end{align}

, wobei die Energie-Wellenlänge Beziehung und die Energie der beschleunigten Elektronen $E_e$

\begin{align}
    E_e=U_A\cdot=E=\frac{hc}{\lambda}
    \label{eq:ew}
\end{align}

benutzt wurde. $h$ ist dabei das Planksche Wirkungsquantum, $c$ die Lichtgeschwindigkeit und $e$ die Elementarladung.\\
Das Linienspektrum der charakteristischen Linien entsteht, wenn ein beschleunigtes Elektron ein Elektron des Anodenmaterials aus der Atomschale herausschlägt. Dazu muss die Energie des beschleunigten Elektrons mindestens der Ionisierungsenergie entsprechen. An der Stelle des herausgeschlagenen Elektrons befindet sich nun ein Loch, das von einem Elektron einer energetisch höheren Schale besetzt werden kann. Beim Übergang des Elektrons verringert es seine Energie und gibt die Differenz als Röntgenquant ab. Da der energetische Abstand zwischen den Schalen konstant ist und bei jedem Material unterschiedlich ist, ergeben sich nur einzelne Linien deren Wellenlänge charakteristisch für bestimmte Materialien ist. Dabei werden Linien die durch einen Übergang in die unterste Schale entstehen als K-Linien bezeichnet, wobei die $\alpha$ Line durch ein Elektron der L-Schale und die $\beta$-Linie durch ein Elektron der M-Schale ausgesendet wird (äquivalent für höhere Schalen).\\
Die Röntgenröhre sendet ihr gesamtes Spektrum gleichzeitig aus, allerdings ist es für Untersuchungen sinnvoller gezielte Welenlängen zu benutzen. Durch Ausnutzung der \textsc{Bragg}-Reflexion kann dies erreicht werden. Dabei wird die entstehende Interferenz bei Reflexion der Röntgenstrahlung an den Netzebenen einer Kristallstruktur benutzt. Für konstruktive Interferenz gilt demnach:

\begin{align}
    n\lambda=2d\sin{\theta}
    \label{eq:bragg}
\end{align}

dabei ist $n$ das n-te Maximum ($n\in\mathbb{N}$), $\lambda$ die reflektierte Wellenlänge, $d$ der Abstand zwischen den Netzebenen und $\theta$ der Glanzwinkel (Winkel zwischen einfallendem Strahl und der reflektierenden Netzebene). Bei bekannter Gitterkonstante $a$ und bekannter reflektierenden Ebenen (hkl) kann der Abstand berechnet werden gemäß:

\begin{align}
    d=\frac{a}{\sqrt{h^2+k^2+l^2}}
    \label{eq:abstand}
\end{align}

Verändert man nun also den Winkel des Kristalls zum Röntgenstrahl kann eine andere Wellenlänge ausgewählt werden. Ein Drehen des Kristalls um $\theta$ (relativ zur optischen Achse des Röntgen-Strahls) bedeutet dabei aufgrund der Experiment-Geometrie eine Drehung des Detektorwinkels auf $2\theta$  (vgl. \abb{aufbau}). Diese Eigenschaften können genutzt werden, um den Aufbau eines Stoffes zu untersuchen.\\
Röntgenquanten können von Materialien auch absorbiert, bzw. geschwächt werden. Zur Veranschaulichung bietet es sich an, die Absorption von geringen Energien (hohe Wellenlängen) zu hohen Energien (kleine Wellenlängen) zu betrachten. Absorption findet hauptsächlich durch Stoßprozesse statt, wobei Röntgenstrahlen mit geringer Energie nach einem Stoß weniger Energie besitzen als jene mit hoher Energie. So sinkt die Absorptionsfähigkeit des Materials mit steigender Energie, bis die Absorption auf einmal sprunghaft ansteigt - die sogenannten Absorptionskanten. Diese haben ihren Ursprung darin, dass die Energie des Röntgenquants gerade groß genug ist, um eine Elektron aus der Atomschale zu ionisieren. Dann tritt maximale Absorption ein und sie existieren für jede besetzte Atomschale, bei den jeweiligen Ionisierungsenergien (K-Kante: n = 1; L-Kante: n = 2; ...). Steigt die Energie weiter an, sinkt der Absorptionskoeffizient wieder bis die nächst stärkere Ionisation möglich ist.\\

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.5\linewidth]{absorptionBSP.PNG}}
	\captionof{figure}{Beispiel für Absorptionskanten}
	\label{fig:absorpBSP}
\end{minipage}

\section{Experimenteller Aufbau}
\subsubsection*{Aufbau}
\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.5\linewidth]{Aufbau.jpeg}}
	\captionof{figure}{Röntgenröhre mit Kristall und Detektor}
	\label{fig:aufbau}
\end{minipage}

\subsubsection*{Materialien}
\begin{minipage}[t]{0.48\linewidth}
	\begin{itemize}[label=-]
    	\item Röntgen Apparat (Geschlossener Aufbau mit Cu-Röntgenröhre, Kristall und Detektor)
	\end{itemize}
\end{minipage}
\hfill
\begin{minipage}[t]{0.48\linewidth}
	\begin{itemize}[label=-]
    	\item Verschiedene Kristalle und Proben (Silbermünze) zur Röntgen-Analyse
	\end{itemize}
\end{minipage}

\section{Kalibrierung}
Zur Kalibrierung bietet es sich an, einen Kristall mit bekannter Struktur (LiF-Kristall) zu benutzen und die bekannte K$_\alpha$ Linie der Kupferröhre zu betrachten. Mit den bekannten Daten und der \textsc{Bragg}-Bedingung lässt sich der Glanzwinkel für das erste Maximum berechnen und damit letztlich auch der Detektorwinkel. In einer Umgebung dieses Winkels wird der Winkel gesucht, bei dem die Reflexion der K$_\alpha$ Linie zu finden ist. Der Experimentelle Wert kann dann mit dem theoretischen verglichen werden.\\
\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{kalibrierung.png}}
	\captionof{figure}{Kupfer (200) Reflex zur Kalibrierung des Goniometers}
	\label{fig:kalibrierung}
\end{minipage}
\vspace{0.5cm}

Der Streuwinkel (Detektorwinkel) $2\theta$ sollte für die Cu-K$_\alpha$ Linie bei $2\theta = \SI{45,02}{\degree}$ liegen, mit einem Glanzwinkel $\theta = \SI{22,51}{\degree}$ am LiF-Kristall \bib{versuchsanleitung}. Der Peak aus \abb{kalibrierung} hat sein Maximum bei $2\theta = \SI{45,05}{\degree}$. Da die Abweichung nur $\theta=\SI{0,03}{\degree}$ beträgt, kann die Einstellung als genau gesehen werden. Das heißt es muss nichts nachträglich justiert werden. Aufgrund der Breite des Peaks, also der geringen Auflösung, ist die Annahme des vernachlässigbaren Fehlers zu begründen.
\section{Durchführung}
\begin{itemize}
    \item [zu 1)] Das Programm wird auf 1:2-Kopplung eingestellt, damit die \textsc{Bragg}-Bedingung stets erfüllt bleibt. Es wird ein Start- und Endwinkel des LiF-Kristalls eingestellt. Dabei ist ein Winkel ungleich Null sinnvoll, da sonst die Röntgenstrahlen ohne Reflexion in den Detektor einfallen. Das Programm arbeitet nun von alleine und das Ergebnis kann gespeichert werden. Die Anodenspannung wurde auf ihr Maximum festgelegt, um das gesamte Spektrum zu sehen.
    \item[zu 2)] Vor dem Detektor werden verschiedene Metallplättchen eingesetzt, durch die die Röntgenstrahlung geschwächt wird. Das gesamte Spektrum wird wieder Aufgenommen, wobei die Absorptionskanten der Metalle gesehen werden sollte. Für ein ausreichendes Verständnis der Absorption wird ein Metall untersucht, dessen K-Kante zwischen der $K_{\alpha}$ und $K_{\beta}$ liegt und eins dessen Kante im Hügel des Bremsspektrums liegt.
    \item[zu 3)] Für acht verschiedene Anodenspannungen (9 bis 20kV) wird das Bremsspektrum in der Nähe des Anstieges untersucht. Die minimale Winkel bzw. Wellenlänge kann dann abgelesen werden. Eine andere Möglichkeit ist es die Fitfunktion $\frac{dn}{d\lambda}=KIZ\left[\frac{\lambda}{\lambda_{min}}-1\right]\cdot\lambda^{-2}$ an die Kurven anzupassen und die Werte für $\lambda_{min}$ abzulesen. Über eine lineare Regression kann dann das \textsc{Duane-Hunt}-Gesetz geprüft werden.
    \item[zu 4)] Zur Messung der polykristallinen Probe wird eine hohe Intensität benötigt, da nur ein kleiner Anteil des Kristalls in einer festen Orientierung die \textsc{Bragg}-Bedingung erfüllt. Deshalb wird zur Untersuchung hauptsächlich die K$_\alpha$ betrachtet. Der Abstand zwischen Eintrittsschlitz und Goniometer-Achse wird nicht verkleinert um das Winkelmaximum von $\theta_{max}=\SI{56}{\degree}$ nicht einzuschränken. Es wird eine große Röhrenleistung benutzt mit einer Zeit pro Messpunkt $\geq \SI{5}{\second}$ und einer Schrittweite von $\Delta \theta=\SI{0,1}{\degree}$. Es ist wichtig, dass die Probenoberfläche in der Gionometer-Achse liegt. Es ist zu beachten, dass generell alle Wellenlängen zum Spektrum der Probe beitragen, damit auch die $K_\beta$ Linie und die n-ten Maxima der Linien. Daher ist es wichtig die relevanten Reflexe zu erkennen. Mit diesen Reflexen und bekannter Literaturwerte für Reflexion an verschieden Gitterarten, kaönnen die Strukturen des polykristallinen Kristalls zugeordnet werden.
\end{itemize}

\section{Resultate}
\subsection{Spektrum der Kupfer-Röntgenröhre}
\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.9\linewidth]{kupferspektrum.png}}
	\captionof{figure}{Röntgenspektrum der Kupferröhre zur Bestimmung der $K_\alpha$- und $K_\beta$-Linien im Spektrum}
	\label{fig:kupferspektrum}
\end{minipage}
\vspace{0.5cm}

Die Peaks der entsprechenden Linien liegen bei
\begin{align}
    \theta(K_\beta, n=1) &= \SI{20,5}{\degree} \hspace{0.5cm}   \theta(K_\beta, n=2) = \SI{43,5}{\degree} \nonumber\\
    \theta(K_\alpha, n=1) &= \SI{22,5}{\degree} \hspace{0.5cm}   \theta(K_\alpha, n=2) = \SI{50,0}{\degree}
\end{align}
Dabei sind die $n=1$ Peaks die Linien erster Ordnung und die $n=2$ Peaks die Linien der zweiten Ordnung. Aus der Bragg-Bedingung \eqref{eq:bragg} folgt für die Wellenlängen der $K_\alpha$- bzw. $K_\beta$-Linien:
\begin{align}
    \lambda_\beta(n=1) &= a\sin{\theta(K_\beta, n=1)} = \SI{0,141064}{\nm}\nonumber\\
    \lambda_\beta(n=2) &= \dfrac{a}{2}\sin{\theta(K_\beta, n=2)} = \SI{0,138635}{\nm}\nonumber\\
    \lambda_\alpha(n=1) &= a\sin{\theta(K_\alpha, n=1)} = \SI{0,154145}{\nm}\nonumber\\
    \lambda_\alpha(n=2) &= \dfrac{a}{2}\sin{\theta(K_\alpha, n=2)} = \SI{0,154281}{\nm}
    \label{eq:peaksKlinien}
\end{align}

Im Vergleich dazu sind die Theoriewerte der Wellenlängen bei
\begin{align}
    \overline{\lambda_\beta} &= \SI{0,139849}{\nm} \hspace{0.5cm} \lambda_{\beta\text{, theo}} = \SI{0,139217}{\nm}\nonumber\\
    \overline{\lambda_\alpha} &= \SI{0,154213}{\nm} \hspace{0.5cm} \lambda_{\alpha\text{, theo}} = \SI{0,15418}{\nm}
    \label{eq:wellenlaengen}
\end{align}

Die jeweiligen Energien der Linien ergeben sich dann aus \eqref{eq:ew} zu
\begin{align}
    \overline{E_\beta} &= \SI{8,86558}{\keV}\nonumber\\
    \overline{E_\alpha} &= \SI{8,03980}{\keV}
    \label{eq:energien}
\end{align}

\subsection{Röntgen-Absorption von verschiedenen Metallen (Filter)}
\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{kupfer_molybden.png}}
	\captionof{figure}{Darstellung der Röntgen-Absorption von Molybden anhand des Verhältnisses zwischen dem Kupfer Spektrum ohne Filter und dem Kupfer Spektrum mit einem Molybden Filter}
	\label{fig:kupfer_molybden}
\end{minipage}
\vspace{0.5cm}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{kupfer_nickel.png}}
	\captionof{figure}{Darstellung der Röntgen-Absorption von Nickel anhand des Verhältnisses zwischen dem Kupfer Spektrum ohne Filter und dem Kupfer Spektrum mit einem Nickel Filter}
	\label{fig:kupfer_nickel}
\end{minipage}
\vspace{0.5cm}

\subsection{Bremsstrahlung bei verschiedenen Röhrenspannung - \textsc{Duane-Hunt}-Gesetz}
\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{bremsstrahlungen.png}}
	\captionof{figure}{Bremsstrahlungen der Röntgenstrahlung für verschiedene Röhrenspannungen}
	\label{fig:bremsstrahlungen}
\end{minipage}
\vspace{0.5cm}

Die Fits ergeben die folgenden Parameter für die einzelnen Bremsspannungen:
\begin{align}
    \lambda\sst{min}(\SI{9}{\kV}) &= \SI{0,1362}{\nm} \hspace{0.5cm} R^2 \approx \SI{0,664}{} \nonumber\\
    \lambda\sst{min}(\SI{10}{\kV}) &= \SI{0,12508}{\nm} \hspace{0.5cm} R^2 \approx \SI{0,605}{} \nonumber\\
    \lambda\sst{min}(\SI{12}{\kV}) &= \SI{0,10194}{\nm} \hspace{0.5cm} R^2 \approx \SI{0,861}{} \nonumber\\
    \lambda\sst{min}(\SI{14}{\kV}) &= \SI{0,088}{\nm} \hspace{0.5cm} R^2 \approx \SI{0,972}{} \nonumber\\
    \lambda\sst{min}(\SI{16}{\kV}) &= \SI{0,07601}{\nm} \hspace{0.5cm} R^2 \approx \SI{0,961}{} \nonumber\\
    \lambda\sst{min}(\SI{18}{\kV}) &= \SI{0,0693}{\nm} \hspace{0.5cm} R^2 \approx \SI{0,967}{} \nonumber\\
    \lambda\sst{min}(\SI{20}{\kV}) &= \SI{0,06234}{\nm} \hspace{0.5cm} R^2 \approx \SI{0,947}{} \nonumber\\
    \lambda\sst{min}(\SI{22}{\kV}) &= \SI{0,05601}{\nm} \hspace{0.5cm} R^2 \approx \SI{0,895}{} 
    \label{eq:lambda_min}
\end{align}

Trägt man die entsprechenden minimalen Wellenlängen $\lambda\sst{min}$ gegen die inverse Röhrenspannung $U_A^{-1}$ auf ergibt sich folgende lineare Regression:

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{duane_hunt_regression.png}}
	\captionof{figure}{Lineare Regression der minimalen Wellenlängen $\lambda\sst{min}$ zur experimentellen Bestimmung von $\dfrac{hc}{e}$}
	\label{fig:duanehuntfit}
\end{minipage}
\vspace{0.5cm}

Aus dem Fit und der Versuchsanleitung [\ref{bib:versuchsanleitung}] folgt:
\begin{align}
    \left(\dfrac{hc}{e}\right)\sst{exp} = \SI{1,23095}{\kV\nm} \hspace{1cm} \left(\dfrac{hc}{e}\right)\sst{theo} = \SI{1,239842}{\kV\nm}
    \label{eq:hce}
\end{align}


\subsection{\textsc{Debye-Scherrer}-Diagramm einer Silbermünze}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{silbermuenze.png}}
	\captionof{figure}{Spektrum der Silbermünze in einem \textsc{Debye-Scherrer}-Diagramm. Die jeweiligen Peaks in schwarz sind Silber Reflexe die mit den \textsc{Miller}-Indizes $(hkl)$ und Glanzwinkel $\theta$ gekennzeichnet wurden. Die Peaks, die mit roten Fragezeichen versehen sind, sind nicht direkt Silber Reflexen zuzuordnen und werden im Folgenden genauer auf ihre Struktur analysiert.}
	\label{fig:silbermuenze}
\end{minipage}
\vspace{0.5cm}

Nach der \textsc{Bragg}-Gleichung mit $n=1$ und \eqref{eq:abstand} lässt sich folgende Ausgleichsrechnung der Form $y = mx$ aufstellen:
\begin{align}
    \sin{\theta}^2 = \dfrac{1}{4a^2}\cdot \lambda^2 (h^2+k^2+l^2)
    \label{eq:hklfit}
\end{align}
wobei $y = \sin{\theta}^2$, $m = \dfrac{1}{4a^2}$ und $x=\lambda^2 (h^2+k^2+l^2)$.

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{millerindizes_silber_fit.png}}
	\captionof{figure}{Lineare Regression der \textsc{Miller}-Indizes für Silber aus \abb{silbermuenze} (schwarz markiert). Mithilfe des Anstiegs $m = \dfrac{1}{4a^2}$ kann der experimentelle Wert für die Gitterkonstante mit der Theorie verglichen werden.}
	\label{fig:hklfit}
\end{minipage}
\vspace{0.5cm}

Wobei für die Gitterkonstante $a$ nach \eqref{eq:hklfit} gilt:
\begin{align}
    a = \dfrac{1}{2\sqrt{m}} = \dfrac{1}{2\sqrt{\SI{1,49362}{\nm^{-2}}}} = \SI{0,409119}{\nm}
\end{align}

Der Theoriewert für Silber liegt bei
\begin{align}
    a\sst{theo} = \SI{0,40853}{\nm}
\end{align}
\vspace{0.5cm}

Damit weicht der experimentell bestimmte Wert um etwa \SI{1,15}{\percent} von der Theorie ab.

\section{Diskussion}
Bei Betrachtung des Spektrums der Kupferröhre \abb{kupferspektrum} sind die in der Theorie beschriebenen Spektren klar zu erkennen. Dazu gehört das Bremsspektrum und die beiden K-Linien, wobei allerdings zwei weitere Peaks auftauchen. Diese sind ein Relikt aus der \textsc{Bragg}-Reflexion und können dem zweiten Maximum der Interferenz zugeordenet werden. Für die K$_\alpha$-Linie kann die bekannte Aufspaltung nicht beobachtet werden, da die Winkelauflösung zu niedrig ist und die Integrationszeit recht gering gewählt wurde. Betrachtet man aber die mittlere Wellenlänge der K$_\alpha$-Linie ($\lambda=\SI{0,15418}{\nano\meter}$) sind die experimentellen Werte sehr nah am Theorie Wert wobei die Abweichung für das erste Maximum \SI{0,023}{\percent} und das zweite Maximum \SI{0,07}{\percent} was sehr gering ist für die mögliche Winkelauflösung. Die K$_\alpha$-Linie konnte also sehr genau bestimmt werden. Für die K$_\beta$-Linie ($\lambda=\SI{0,139217}{\nano\meter}$) liegt die Abweichung beim ersten Maximum bei \SI{1,4}{\percent} und fürs zweite Maximum bei \SI{0,5}{\percent}, was immer noch sehr gut ist, aber deutlich mehr als bei der K$_\alpha$-Linie. Das kann an der geringeren Intensität bei kurzer Integrationszeit liegen. Eine längere Zeit für jede Messung hätte das Ergebnis genauer gestaltet können. Allerdings ist der Fehler immer noch sehr gering und die Bestimmung des Peaks kann als sehr genau gesehen werden. Ein gewisser Fehler kann auch immer dem LiF-Kristall zugeschrieben werden, da dieser trotz seiner monokristallinen Struktur Fehler (Störstellen) aufweisen kann - diese können für leicht abweichende Netzebenenabständen sorgen und damit die \textsc{Bragg}-Bedingung verändern. Es kann auch ein zufälliger Reflex an einer anderen Ebene das Spektrum verfälschen. Das Spektrum zeigt außerdem einen unerwarteten Anstieg der Intensität unterhalb $\theta_{min}$. Das kann wieder ein Relikt der \textsc{Bragg}-Reflexion sein und eigentlich ein n-tes Maximum anderer Reflexionen sein, was in dieser Versuchsanordnung zu erwarten war.\\
Die Argumentation der Absorptionskante wird von hoher Wellenlänge zur geringen Wellenlänge ausgeführt, um die Begriffe vor und hinter genauer zu definieren.\\

Bei Betrachtungen der Absorption ist vor Allem der Nickel-Filter interessant, da dessen Absorptionskante zwischen der K$_\alpha$- und K$_\beta$-Linie liegt. Das Ergebnis dieser besonderen Lage ist in \abb{kupfer_nickel} zu sehen. Es ist zu erkennen, dass die K$_\alpha$-Linie nahezu ungestört passieren konnte und deutlich zu sehen ist. Die K$_\beta$-Linie hingegen ist fast gar nicht zu erkennen, da sich die Intensität sehr stark verringert hat. Das lässt sich damit begründen, dass die K$_\alpha$-Linie direkt vor Absorptionskante liegt, wo der Absorptionskoeffizient in der Theorie sehr gering ist. Die K$_\beta$-Linie liegt sehr nah hinter der Kante, wo der Absorptionskoeffizient nahezu maximal ist und wird dementsprechend sehr stark geschwächt. Ein Nickel-Filter kann also benutzt werden um die K$_\beta$-Linie zu blockieren und somit nur noch die K$_\alpha$-Linie klar zu erkennen ist. Dies kann bei Untersuchungen mit der \textsc{Bragg}-Reflexion genutzt werden, um Verwechslungen der Maxima zu verhindern.\\

Die Absorptionkante von Molybden liegt theoretisch im Bereich des Bremsspektrums und ist dort sichtbar, indem das Verhältnis zwischen Molybden- und Kupferspektrum (s. \abb{kupfer_molybden}) aufgetragen wird. So kann man gut erkennen, dass sowohl für Molybden, als auch für Nickel die experimentelle Lage der Absorptionskante etwas zu niedrig ist - verglichen mit der Theorie-Linie in rot. Die experimentell bestimmte Kante liegt für Molybden bei etwa \SI{0,063}{\nm} und die von Nickel bei ca. \SI{0,15}{\nm}. Die Abweichung kann vor allem mit der minimalen Schrittweite des Motors erklärt werden, der den Kristall und Detektorwinkel steuert. Eine minimale Grad-Auflösung von \SI{2}{\degree} ist dabei eine mögliche Ursache für die Abweichung der Theoriewerte von den gemessenen Kanten, da der Messfehler für den Winkel größer sein kann als die absolute Differenz zwischen experimentell bestimmter und theoretischer Absorptionskante.\\

Der experimentell ermittelte Wert für $\frac{hc}{e}$ ist mit $\SI{1,23095}{\kilo\volt\nano\meter}$ sehr nah am theoretischen und liegt mit seinem Fehler ($\SI{0,01666}{\kilo\volt\nano\meter}$) im Bereich von $\SI{1,21429}{\kilo\volt\nano\meter}$ bis $\SI{1,24761}{\kilo\volt\nano\meter}$. Damit ist der theoretische Wert innerhalb dieses Bereiches mit $\SI{1,239842}{\kilo\volt\nano\meter}$ und die Diskrepanz somit insignifikant. Auch so ist zu erkennen, das die Werte nahezu identisch sind. Die Abweichung liegt bei nur \SI{0,8}{\percent}. Damit konnte die Gültigkeit des \textsc{Duane-Hunt}-Gesetzes bestätigt werden. Mit \SI{1,4}{\percent} ist der Fehler des Anstieges und damit des experimentellen Wertes sehr gering, sodass von einer sehr guten und gelungen Messung ausgegangen werden kann.\\

Die Peaks der K$_\alpha$-Linie konnten sehr gut den Gitterebenen zugeordnet werden. So erhält man bei der linearen Regression eine Gitterkonstante, die nahezu exakt am Theoriewert liegt. Mit dem Fehler, errechnet durch $u_a=\frac{1}{4\sqrt{m^3}}\cdot u_m$, liegt der Wert im Bereich $a_{exp}=\SI{0,409119}{\nano\meter}\pm\SI{0,001357}{\nano\meter}$, womit eine insignifikante Diskrepanz zu $a_{theo}=\SI{0,40853}{\nano\meter}$ vorliegt. Damit ist der Versuch erfolgreich hinsichtlich der Silberstruktur. Aufgrund der zugeordneten Netzebenen zu den Reflexen (h,k,l alle gerade oder ungerade) kann eine kubisch flächenzentrierte (fcc) Struktur dem Silber zugeordnet werden, was wieder der Theorie entspricht.\\
Die Peaks, die mit Fragezeichen versehen wurden, konnten der $K_\beta$-Linie des Kupfers zugeordnet werden. Mit einem eingebauten Nickel-Filter hätte man diese also unterdrücken können und ein saubereres Bild erhalten. Der Bereich der hohen Intensität bevor das Bremsspektrum einsetzt, kann durch ein ungestörtes Passieren der Strahlung erklärt werden. Es wird also noch nicht reflektiert, sodass das gesamte Spektrum auf einmal auf den Detektor trifft. Daher ruht auch die hohe Intensität.\\
Es konnte also gezeigt werden, dass die untersuchte Münze dicht an reinem Silber ist. Eventuell beigemischte Metalle sind so gering vorhanden, dass sie nicht genügend Intensität bei Reflexion aufbringen, um im Spektrum bzw. für den Detektor erkennbar zu sein.
\begin{thebibliography}{9}

    \bibitem{} \label{bib:versuchsanleitung}
    \href{}{Versuchsanleitung \enquote{Röntgen}, Institut für Physik, Universität Rostock, Rostock, 29.07.2020} [14.05.2022]
    \bibitem{} \label{bib:GitterkonstanteSilber}
    \href{https://periodictable.com/Elements/047/data.html}{https://periodictable.com/Elements/047/data.html} [19.05.2022]

\end{thebibliography}

\section*{Erklärung der selbstständigen Anfertigung}
Hiermit erklären wir, dass das vorliegende Protokoll selbständig verfasst und keine anderen als die angegebenen Hilfsmittel verwendet wurden. \\
Insbesondere versichern wir, dass alle genutzten Internetseiten kenntlich gemacht wurden und im Verzeichnis der Internetseiten bzw. im Quellenverzeichnis aufgeführt.\\
Sämtliche Schaltskizzen, Abbildungen sowie Oszilloskopaufnahmen sind selbstständig angefertigt, oder der Versuchsanleitung entnommen. 

\end{document}
