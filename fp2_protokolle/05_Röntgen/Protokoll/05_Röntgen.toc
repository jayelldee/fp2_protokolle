\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Aufgabenstellung}{2}{section.1}%
\contentsline {section}{\numberline {2}Physikalische Grundlagen}{2}{section.2}%
\contentsline {section}{\numberline {3}Experimenteller Aufbau}{3}{section.3}%
\contentsline {section}{\numberline {4}Kalibrierung}{4}{section.4}%
\contentsline {section}{\numberline {5}Durchführung}{4}{section.5}%
\contentsline {section}{\numberline {6}Resultate}{5}{section.6}%
\contentsline {subsection}{\numberline {6.1}Spektrum der Kupfer-Röntgenröhre}{5}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Röntgen-Absorption von verschiedenen Metallen (Filter)}{6}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}Bremsstrahlung bei verschiedenen Röhrenspannung - \textsc {Duane-Hunt}-Gesetz}{8}{subsection.6.3}%
\contentsline {subsection}{\numberline {6.4}\textsc {Debye-Scherrer}-Diagramm einer Silbermünze}{10}{subsection.6.4}%
\contentsline {section}{\numberline {7}Diskussion}{11}{section.7}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
