\documentclass[a4paper,10pt,headsepline=2pt,footinclude=false]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{graphicx}
\graphicspath{{Bilder/}}
\usepackage{xcolor}
\usepackage[left=2cm,right=1.5cm,top=2.5cm,bottom=2cm]{geometry}
\usepackage{scrlayer-scrpage}
\usepackage[version=3]{mhchem}
\usepackage{csquotes}
\usepackage{mdframed}
\usepackage{multirow}
\usepackage{caption}
\usepackage{hyperref}
\usepackage{siunitx}
\sisetup{locale = DE, per-mode = fraction, separate-uncertainty}
\usepackage{ulem}
\usepackage{appendix}
\usepackage{float}
\usepackage{subfig}
\usepackage{enumitem}
\usepackage{helvet}
\newcommand{\ignore}[1]{}
\newcommand{\sst}[1]{_{\text{#1}}}
\newcommand{\ra}{$\rightarrow$ }
\newcommand{\gl}[1]{Gl.(\ref{eq:#1})}
\newcommand{\abb}[1]{Abb. \ref{fig:#1}}
\newcommand{\tab}[1]{Tab. \ref{tab:#1}}
\newcommand{\bib}[1]{[\ref{bib:#1}]}
\renewcommand{\familydefault}{\sfdefault}
\newcommand*\myappendixchanges{%
  \setcounter{table}{0}%
  \gdef\thesection{A.\arabic{section}}%
  \gdef\thetable{A.\arabic{table}}}


\lohead{\includegraphics[width=4cm]{logo}}			%Logo in der Kopfzeile
\cohead{Fortgeschrittenenpraktikum 2\\[0.2cm] Alex Kreis \& Jannik Dierks - Gruppe 1}	%Text in der Kopfzeile
\rohead{\thepage}									%Seitenzahl
\cfoot[]{}											%keine Fußzeile
\setkomafont{headsepline}{\color{red!70!black}}		%Farbe der Trennlinie	
\setkomafont{pagehead}{\bfseries}					%Im Kopf Fett geschrieben
\usepackage{colortbl}								%bringt Farbe in die Tabelle
\definecolor{dunkelgrau}{gray}{0.8}					%Farbe definieren
\definecolor{hellgrau}{gray}{0.9}					%Farbe definieren
\definecolor{rot}{rgb}{0.616,0.051,0.082}			%Farben definieren 0-100%

%Links zu Gleitumgebung von figure und table:
%http://tex.lickert.net/tipps/optionh/optionh.html
%https://www.texwelt.de/fragen/3427/wann-sollte-ich-gleitumgebungen-fur-tabellen-und-abbildungen-verwenden

\setlength\parindent{0pt}

%--------------------------------------------------------------------------------
%	DOKUMENT
%--------------------------------------------------------------------------------
\begin{document}

\renewcommand{\figurename}{Abb.}
\counterwithin{figure}{section}
\renewcommand{\tablename}{Tab.}
\counterwithin{table}{section}
\counterwithin{equation}{section}

\begin{center}
	\huge \textbf{Zweispaltinterferenzversuch mit einzelnen Photonen}\\\vspace{3pt}
	\Large Erstautor: Alex Kreis\\
	\large Zweitautor: Jannik Dierks\\
	\Large Gruppe 1\\
	\vspace{0.5cm}
	\large Institut für Physik - Universität Rostock - \today\\
	Versuchsbetreuer*in: Herr Altenkirch
\end{center}

\tableofcontents
\vspace{0.5cm}

\section*{Ziel des Versuchs}
\begin{enumerate}[label=\alph*)]
	\item Die Geräte \enquote{Photomultiplier (PMT)} und \enquote{Diskriminator} kennenlernen
	\item Das Fachwissen um die Beugung an Spalten vertiefen
\end{enumerate}
\newpage
	
\section{Physikalische Grundlagen}
\begin{itemize}[label=-]
	\item Das Doppelspalt-Experiment ist eines der bekanntesten Experimente, um den Welle-Teilchen-Dualismus von Photonen zu demonstrieren.
	\item Dabei werden Photonen einer Lichtquelle (in unserem Fall thermisches Licht einer Glühlampe) auf einen Doppelspalt geschossen. Weit hinter dem Doppelspalt  kann das Beugungsbild dann auf einem Schirm betrachtet werden (der Spaltabstand und die Spaltbreiten sind viel kleiner als der Abstand des Doppelspalts zum Schirm).
	\item In unserem Fall handelt es sich um eine abgeschlossene Vorrichtung, um den Einfall von fremden Photonen zu verhindern, die das Messergebnis verfälschen. Die Vorrichtung kombiniert die Lichtquelle, den Doppelspalt und die Detektoreinheit (s. \abb{aufbau}).
	\item Der Detektor ist ein sogenannter \enquote{Photomultiplier} (s. \abb{aufbau_pmt}) oder PMT (Photomultiplier tube - aus dem Englischen). Dabei treffen die zu detektierenden Photonen auf eine Photokathode, aus der Elektronen durch den Photoelektrischen Effekt ausgelöst werden. Diese Photoelektronen werden dann durch eine Kaskade aus Dynoden (Metallplatten mit stark positivem Potential) vervielfältigt - durch die Beschleunigung des einzelnen Photoelektrons, das aus der Photokathode gelöst wurde, kann dieses an der ersten Dynode mehrere Elektronen auslösen, die dann wieder mehrere Elektronen an der 2. Dynode auslösen können und so weiter. Die Elektronen, die aus den Dynoden ausgelöst werden nennt man Sekundärelektronen. Das Signal wird dann durch einen $I$-$U$-Wandler in ein Spannungssignal umgewandelt und kann dann gemessen werden.
	\item An den PMT angeschlossen wird ein Pulshöhen Diskriminator (PHD). Dieser erzeugt einen definierten Signalpuls, immer wenn eine zuvor eingestellte Signalgrenze überschritten wird. Dieser ist nötig, um die Messverfälschung durch den sogenannten Dunkelstrom zu minimieren. 
	\item Beim Dunkelstrom kommt es zu der Emission von freien Ladungsträgern durch thermische Bewegung im Material der Photokathode im PMT. Diese freien Ladungsträger können dann durch die Dynoden verstärkt werden und zu einem falschen Signalpuls führen. Mithilfe des PHD kann dieser Dunkelstrom \enquote{eliminiert} werden, da die Dunkelstrom-Signal in der Regel kleiner sind, als die vom PHD festgesetzte Signalgrenze. Die tatsächlichen Signale der Photonencounts hingegen sind meist größer als die Signalschwelle des PHD.
	\item Der Photoncounter zählt dann die einzelnen Signale der Photonen, die einen Signalpuls im PMT auslösen der groß genug ist, um das Diskriminatorlevel zu überwinden.
	\item Um die Messwerte hinsichtlich der Dunkelstromrate zu korrigieren, wird folgender Zusammenhang zwischen der Zählrate $Z(x)$, dem durchschnittlichen Dunkelstrom $Z_D$ und der Korrektur $\alpha$ angenommen:
	\begin{align}
	    Z\sst{kor1}(x) = Z(x)+\alpha = Z(x) - Z_D \hspace{0.5cm} \rightarrow \hspace{0.5cm} \alpha = -Z_D
	    \label{eq:kor1}
	\end{align}
	
	$Z\sst{kor1}$ ist dabei die um den Dunkelstrom korrigierte Zählrate.
	\item Die zweite Korrektur, die für die Zählrate vorgenommen werden muss, ergibt sich aus der Tatsache, dass der PMT nicht an allen Stellen entlang der Messachse gleich empfindlich ist. Es lässt sich zeigen, dass die Empfindlichkeit entlang der Messachse linear abnimmt. Damit kann für eine zweite Korrektur folgende Annahme getroffen werden:
	\begin{align}
	    Z\sst{kor1}(x) = Z\sst{kor2} \cdot \beta = Z\sst{kor2} \cdot (ax+b) 
	    \label{eq:kor2}
	\end{align}
	
	\item Mithilfe der folgenden Fitfunktion kann die Wellenlänge der Photonen bestimmt werden:
	\begin{align}
	    Z(x) = \dfrac{Z\sst{max}}{4}\cdot \left[ \dfrac{\sin\left(A\cdot\sin\left(\dfrac{x-x_0}{L}\right)\right)}{A\cdot\sin\left(\dfrac{x-x_0}{L}\right)} \right]^2 \cdot \left[ \dfrac{\sin\left(2\cdot B\cdot\sin\left(\dfrac{x-x_0}{L}\right)\right)}{\sin\left(B\cdot\sin\left(\dfrac{x-x_0}{L}\right)\right)}\right]^2
	    \label{eq:lambda_fit}
	\end{align}
	\begin{align}
        I(\varphi) &= I_0 \cdot \left[\dfrac{\sin\left(\dfrac{\pi b}{\lambda}\cdot\sin(\varphi)\right)}{\dfrac{\pi b}{\lambda}\cdot\sin(\varphi)}\right]^2 \cdot \left[ \dfrac{\sin\left(2\cdot\dfrac{\pi d}{\lambda}\cdot\sin(\varphi)\right)}{\sin\left(\dfrac{\pi d}{\lambda}\cdot\sin(\varphi)\right)}\right]^2
        \label{eq:lambda_leifi}
	\end{align}

    \begin{minipage}{\linewidth}
    	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{Doppelspalt_Schema.png}}
    	\captionof{figure}{Schematischer Aufbau der Messapparatur \bib{aufbau}}
    	\label{fig:aufbau}
    \end{minipage}

    Es können folgende Annahmen getroffen werden, um die Wellenlänge aus den Parametern $A$ und $B$ zu berechnen:\\ 
    Da $L >> x-x_0$ ($L$ etwa \SI{50}{\centi\meter}, $x-x_0$ einige hundert \si{\micro\meter}) kann angenommen werden, dass die Kleinwinkelnäherung gilt. Damit ergibt sich $sin(\varphi) \approx \varphi$.\\ 
    Ferner kann durch die Kleinwinkelnäherung angenommen werden, dass die Hypotenuse $H$ des Dreiecks aus $L$, $x-x_0$ und $H$ ungefähr $L$ ist. Es ergibt sich:
    \begin{align}
        sin(\varphi) = \dfrac{x-x_0}{H} \approx \dfrac{x-x_0}{L} \approx \varphi
    \end{align}
    Damit kann durch Vergleichen der Vorfaktoren in den Gl.(\ref{eq:lambda_fit} und \ref{eq:lambda_leifi}) $\lambda$ wie folgt bestimmt werden: 
    \begin{align}
        A = \dfrac{\pi b}{\lambda} \hspace{0.5cm} B = \dfrac{\pi d}{\lambda} \hspace{0.5cm} \rightarrow \hspace{0.5cm} \lambda = \dfrac{\pi b}{A} = \dfrac{\pi d}{B}
		\label{eq:lambda}
    \end{align}
    .
\end{itemize}

\section{Experimenteller Aufbau}
	\subsubsection*{Aufbau}
    \begin{minipage}{\linewidth}
    	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{aufbau.png}}
    	\captionof{figure}{Schematischer Aufbau der Messapparatur \bib{aufbau}}
    	\label{fig:aufbau}
    \end{minipage}
    
    \vspace{0.5cm}
    
    \begin{minipage}{\linewidth}
    	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{aufbau_pmt.png}}
    	\captionof{figure}{Schematischer Aufbau eines PMT \bib{aufbau_pmt}}
    	\label{fig:aufbau_pmt}
    \end{minipage}
    
    \subsubsection*{Materialien}
    \begin{minipage}[t]{0.48\linewidth}
    	\begin{itemize}[label=-]
        	\item Oszilloskop
        	\item Lichtquelle (Glühlampe mit Schmalbandfilter)
        	\item Laserdiode
        	\item Doppelspalt
        	\item \enquote{Fenster} (Blendenspalt hinter dem Doppelspalt)
    	\end{itemize}
    \end{minipage}
    \hfill
    \begin{minipage}[t]{0.48\linewidth}
    	\begin{itemize}[label=-]
        	\item Pulshöhendiskriminator
        	\item Photodiode
        	\item Detektorspalt 
        	\item Photomultiplier \& Photoncounter
        	\item Digitalmultimeter 
    	\end{itemize}
    \end{minipage}
    
    
\section{Aufgabe 1}
\subsection{Aufgabenstellung}
Führen Sie Youngs Doppelspaltexperiment mit einzelnen Photonen durch.

\subsection{Durchführung}
\begin{enumerate}
    \item Der Versuchsaufbau wird geöffnet und alle Spalte in den dafür vorgesehenen Halterungen befestigt (s. \abb{aufbau}).
    \item Der PMT sollte ausgeschaltet sein und die Photodiode den Lichtweg zum PMT blockieren (Shutter unten).
    \item Nun wird der Laser eingeschaltet und die Spalte entlang der Laserachse so ausgerichtet, dass hinter der ersten Blende zunächst das Einzelspalt-Beugungsmuster entsteht. Dann wird der Doppelspalt mittig zum Maximum des Einzelspalt-Beugungsmusters ausgerichtet
    \item Zuletzt wird am Detektorspalt überprüft, ob dieser senkrecht zu den Interferenzstreifen des Doppelspaltmusters steht. Die entsprechenden Muster können mit einer \enquote{Detektorkarte} überprüft werden.
    \item Der Laser wird nun aus dem Aufbau entfernt. Und vor der Glühlampe wird ein Spektralfilter (grünes Licht) eingebaut.
    \item Die Glühlampe wird auf etwa die halbe Leistung gestellt und an den PMT wird das Digitalmultimeter als Spannungsüberwachung angeschlossen.
    \item An den PMT werden \SI{900}{\volt} angelegt. Das Digitalmultimeter hat einen Umrechnungsfaktor von \SI{1e-3}{}. Am DMM werden also \SI{0,9}{\volt} gemessen.
    \item Der PMT wird an den Photoncounter angeschlossen und das Diskriminatorlevel auf Null gestellt. Für die Zählzeit wird zunächst \SI{1}{\second} gewählt.
    \item Die Photodiode wird nun aus dem Lichtweg entfernt (Shutter hoch) und mithilfe der Mikrometerschraube wird der Detektorschlitz solange verschoben, bis das Signal am Photoncounter maximal wird. Für diese Messung bietet sich der \enquote{Auto}-Modus in der Zeitauswahl am Photoncounter an.
    \item Ist das Maximum gefunden, wird das Diskriminatorlevel so gewählt, dass $\dfrac{Z_D}{Z(x)}$ minimal wird. Dabei ist $Z_D$ die Anzahl an Dunkelstromcounts (Shutter unten) und $Z(x)$ die Anzahl an gezählten Photonen (Shutter offen).
    \item Das gewählte Diskriminatorlevel wird für die folgenden Messungen beibehalten.
    \item Mit geschlossenem Shutter wird bei dem Maximum $x_0$ der Zählrate $Z(x)$ die durchschnittliche Anzahl an Dunkelstromcounts bestimmt. Der Shutter wird dafür geschlossen und der Photoncounter auf \SI{10}{\second} Zeitintervalle gestellt. Es werden fünf Messungen aufgenommen.
    \item Nun wird der Shutter wieder geöffnet. Der Detektorspalt wird in \SI{5}{\micro\meter}-Schritten um insgesamt \SI{2,5}{\milli\meter} zu jeder Seite vom Maximum $Z(x_0)$ gefahren.
    \item Für jeden Ort $x$ wird mit einer Messdauer von \SI{10}{\second} die Photonenzahl $Z(x)$ aufgenommen.
    \item Am Oszilloskop wird die Pulsbreite eines einzelnen Photonenpulses beim Maximum $Z(x_0)$ gemessen, um diese später mit dem durchschnittlichen zeitlichen Abstand der Pulse an dem Ort $x_0$ zu vergleichen.
    \item Es werden nun alle Spalte (bis auf den Detektorspalt) aus dem Versuchsaufbau entfernt.
    \item Der Detektorspalt wird in \SI{25}{\micro\meter}-Schritten auf demselben Intervall von $[x_0 -\SI{250}{\micro\meter}, x_0+\SI{250}{\micro\meter}]$ durchgefahren und für jeden Ort $x$ werden fünf Messwerte mit einem Zeitintervall von \SI{1}{\second} aufgenommen.
    
\end{enumerate}

\subsection{Resultate}
\vspace{0.5 cm}
\begin{table}[h]
\begin{center}
\begin{tabular}[h]{|c|c|c|c|}
\hline
Dis-Stufe & $Z(x)$	&	$Z_D$	&	$Z_D/Z(x)$	\\
\hline
0	&	902	&	216	&	0,24	\\
3	&	515	&	70	&	0,14	\\
3,5	&	375	&	56	&	0,15	\\
4,5	&	180	&	31	&	0,17	\\
5	&	123	&	33	&	0,27	\\
6,5	&	35	&	9	&	0,26	\\
\hline
\end{tabular}
\end{center}
\caption{Quotient bei Verschiedenen Diskriminatoreinstellungen (Dis-Stufe)}
\label{tab:diskrim}
\end{table}

Die ideale Diskriminatoreinstellung ist laut \tab{diskrim} die Stufe 3. Dort ist der Quotient minimal, das heißt im Verhältnis kommen auf die Anzahl der relevanten Photonen (Photonenzählrate) der geringste Dunkelstrom. Damit sind die Messergebnisse bei dieser Diskriminatoreinstellung am reinsten und am wenigsten fehlerbehaftet.
\newpage

\begin{table}[h]
\begin{center}
\begin{tabular}[h]{|c|c|}
\hline
Nr. & $Z_D$ \\
\hline
1 & 70 \\
2 & 73 \\
3 & 82 \\
4 & 72 \\
5 & 79 \\
6 & 73 \\
7 & 88 \\
8 & 59 \\
9 & 64 \\
10 & 75 \\
\hline
\end{tabular}
\end{center}
\caption{Dunkelstromzählrate für $T = 10s$}
\label{tab:dunkel}
\end{table}

Mit Hilfe von \tab{dunkel} kann eine mittlerer Dunkelstromzählrate $\overline{Z_D}$ berechnet werden, die gemäß \gl{kor1} dem negativem Wert des Korrekturterms $\alpha$ entspricht. Daraus folgt $\alpha = -73,5$.\\

\vspace{0.5 cm}
 \begin{minipage}{\linewidth}
    	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{scope_2.png}}
    	\captionof{figure}{Pulsdauer des PMT}
    	\label{fig:pulsPMT}
    \end{minipage}
\vspace{0.5 cm}

Die Pulsdauer $\Delta t\sst{PMT}$ der PMT-Signale kann aus \abb{pulsPMT} entnommen werden und beträgt $\SI{16,8e-9}{\second}$.

\begin{table}[h]
\begin{center}
\begin{tabular}[h]{|c|c|}
\hline
Nr.	&	$Z(x_0)$	\\
\hline
1	&	908	\\
2	&	916	\\
3	&	967	\\
4	&	930	\\
5	&	918	\\
\hline
\end{tabular}
\end{center}
\caption{Photonenzählrate für $T = \SI{10}{\second}$ bei $x_0= \SI{248}{\micro\meter}$ }
\label{tab:puls}
\end{table}

Um aus der Photonenzählrate den Pulsabstand zu erhalten, wird zunächst der Mittelwert $\bar{Z}(x)$ berechnet und entsprechend in die Gleichung $\Delta t = \dfrac{10s}{\overline{Z}(x)}$ eingesetzt. Damit erhält man die Zeit zwischen den Pulsen. Mit den Werten aus \tab{puls} erhält man:
\begin{align}
    \overline{Z}(x_0) &= \SI{927,8}{} \\
    \Delta t &= \SI{10,7782e-3}{\second}
\end{align}

Für die Kalibrierung des PMT (s. \gl{kor2}) wurden aus der linearen Regression (s. \abb{agr_beta}) folgende Parameter bestimmt:
\begin{align}
    a = \SI{-6,870}{} \cdot (1 \pm \SI{12}{\percent}) \si{\dfrac{\micro\meter}{1}} \hspace{0.5cm} \text{und} \hspace{0.5cm} b = \SI{13636}{} \cdot (1 \pm \SI{2}{\percent})
\end{align}

Damit ergibt sich $\beta$ zu:
\begin{align}
    \beta = \dfrac{1}{ax+b} = \dfrac{1}{\SI{-6,870}{\micro\meter}\cdot x + \SI{13636}{}}
    \label{eq:beta}
\end{align}

Der Verlauf der Zählrate $Z\sst{kor2}(x)$ (also die Zählrate $Z(x)$ korrigiert um den Dunkelstrom und die Kalibrierung des PMT) ist in der folgenden Abbildung dargestellt:

\vspace{0.5 cm}
\begin{minipage}{\linewidth}
    	\makebox[\linewidth]{\includegraphics[width=0.7\linewidth]{Z_kor1.png}}
    	\captionof{figure}{Verlauf der Zählrate $Z\sst{kor1}(x)$ über der Position des Mikrometertriebs $x$}
    	\label{fig:zkor1}
\end{minipage}
\vspace{0.5 cm}

\begin{minipage}{\linewidth}
    	\makebox[\linewidth]{\includegraphics[width=0.7\linewidth]{Z_kor2.png}}
    	\captionof{figure}{Verlauf der Zählrate $Z\sst{kor2}(x)$ über der Position des Mikrometertriebs $x$}
    	\label{fig:zkor2}
\end{minipage}
\vspace{0.5 cm}

Um den Effekt der $\beta$-Korrektur besser sichtbar zu machen, kann eine Differenz aus den Zählraten der entsprechenden Maxima gebildet werden. Beispiel:
\begin{align}
    \Delta Z\sst{kor1} = Z\sst{kor1}(x\approx 75) - Z\sst{kor1}(x\approx -80)
\end{align}

\begin{table}[H]
    \centering
    \begin{tabular}{|c|c|c|}
    \hline
    Ordnung der Maxima     &  $\Delta Z\sst{kor1}$/\SI{1}{} & $\Delta Z\sst{kor2}$/\SI{1}{}\\ \hline
    1     & \SI{0,053}{} & \SI{0,1302}{}\\
    2     & \SI{0,0376}{} & \SI{0,132}{}\\
    3     & \SI{0}{} & \SI{0,0618}{}     \\\hline
    \end{tabular}
    \caption{Normierte Zählratendifferenz $\Delta Z$ für verschiedene Maxima}
    \label{tab:delta_z}
\end{table}

\subsection{Diskussion}
Die perfekte Diskriminatoreinstellung zu finden ist nahezu unmöglich, da die Zählrate eine statistische Größe ist, was zu Abweichungen führt. Damit ist eine Änderung der Diskriminatoreinstellung in zu kleinen Schritten nicht sinnvoll. Ändert man die Einstellung beispielsweise um \SI{0,1}{} kann sie in einer Messung besser, aber in einer Zweiten schlechter sein als die Vorherige. Um Schwankungen in der Zählrate zu vermindern, wurde sie \SI{10}{} Sekunden lang gemessen. Der Quotient ist selbst bei der idealen Diskriminatoreinstellung 3 mit \SI{0,14}{} nicht ideal, da damit \SI{14}{\percent} der gezählten Impulse keine Photonen sind. Für so ein empfindliches System ist der Wert aber angemessen. In \tab{diskrim} fehlen Werte zwischen 0 und 3, um diesen Bereich auf einen besseren Quotienten zu untersuchen. Der Bereich wurde untersucht und stellte sich als schlechter raus, allerdings wurde vergessen die Messwerte zu notieren.\\

Um zu verifizieren, dass es sich tatsächlich um einzelne Photonen handelt, die den Doppelspalt passieren, bietet es sich an die Pulsdauer des PMT-Signals mit dem Pulsabstand der gezählten Photonen zu vergleichen. Es fällt auf, dass $\Delta t = \SI{10,7782e-3}{\second}$ um den Faktor von ca. \SI{6e5}{}\ignore{641559,5} größer ist als $\Delta t\sst{PMT} = \SI{16,8e-9}{\second}$. Dieser deutliche Größenunterschied zeigt, dass der PMT definitiv in der Lage ist diese Photonen einzeln zu detektieren, da der Zeitabstand zwischen zwei Photonen viel größer ist als die Dauer des Signals. Wäre der zeitliche Abstand zwischen zwei Photonen kleiner als die Dauer des PMT-Pulses, könnte der Photoncounter nicht jedes Photon einzeln detektieren und es könnten mehr Photonen auftreffen als gemessen werden.\\

In \abb{zkor1} und \abb{zkor2} ist das zu erwartende Interferenzmuster eines Doppelspaltes zu erkennen. Die Einhüllende Funktion ist dabei das Beugungsmuster eines Einfachspaltes.\\
Während $Z\sst{kor1}$ nur um den Dunkelstrom korrigiert wurde, ist in \abb{zkor2} auch die $\beta$-Korrektur (also die Kalibrierung des PMT) einbezogen worden. Den Effekt dieser Korrektur kann man besonders an den Maxima 1. Ordnung neben dem Hauptmaximum erkennen. In der Theorie sollten die Maxima dieselbe Höhe haben (gleiche Zählraten $Z(x)$), doch die lineare Abnahme der Empfindlichkeit des PMTs führt zu ungleichen Zählraten. Um dies zu korrigieren, wurde der Verlauf der PMT-Empfindlichkeit in \abb{agr_beta} geplottet und mit einer linearen Regression die $\beta$-Korrektur berechnet (s. \gl{beta}).\\
Leider ist in \abb{zkor2} der gegenteilige Effekt zu beobachten, der bei der $\beta$-Korrektur gewünscht ist. Die Zählratendifferenz $\Delta Z$ nimmt bei den Maxima erster Ordnung (und den Maxima der folgenden Ordnungen) nach der Korrektur zu (s. \tab{delta_z}). Hier sollte die Korrektur die Zählratendifferenz $\Delta Z$ eigentlich minimieren. Ursache dafür kann die geringe Anzahl an Messungen pro Messpunkt von $Z(x)$ sein (ein Messpunkt á \SI{10}{\second}). Dadurch können die Werte der Maxima für $x > 0$ durch statistische Effekte immer zufälligerweise größer sein, als die Messwerte für $x < 0$. Um dies auszuschließen, ist es sinnvoll, die Messung entweder häufiger zu wiederholen (wieder ein Messpunkt á \SI{10}{\second}) oder mehrere Messpunkte pro Ort $x$ aufzunehmen, um  statistische Fehler zu minimieren.\\

Vergleicht man die Ergebnisse der Pulsdauermessung am Oszilloskop mit der Interferenzmessung am PMT, kann der Welle-Teilchen-Dualismus der Photonen gut beschrieben werden. Das Detektieren (also die einzelnen Counts am PMT) zeigt den Teilchencharakter der Photonen, da immer ein Photon durch den Photoelektrischen Effekt die Elektronen im PMT auslöst. Die ortsabhängige Zählraten-Messung am PMT zeigt hingegen ein Interferenzmuster. Dies spiegelt die Welleneigenschaften von Photonen wieder.\\ 
Der Widerspruch des Experiments ist, dass man erwartet (wenn man Photonen als klassische Teilchen annähert) hinter dem Doppelspalt auf dem Schirm zwei Maxima zu sehen - ein Maxima hinter jedem Spalt. Stattdessen ist jedoch ein Interferenzmuster wie bei Wellen zu beobachten, obwohl nachgewiesen ist, dass die Photonen einzeln nacheinander durch den Doppelspalt geschossen werden. Das heißt ein Photon interferiert mit sich selbst und muss dementsprechend als Welle angesehen werden. Somit konnte mit dem Doppelspalt-Experiment der Welle-Teilchen-Dualismus von Photonen verifiziert werden.


\section{Aufgabe 2}
\subsection{Aufgabenstellung}
Ermitteln Sie die Wellenlänge $\lambda$ der einzelnen Photonen.

\subsection{Durchführung}
\begin{enumerate}
    \item Mit einem Stahllineal wird der Abstand vom Doppelspalt zum Detektorspalt bestimmt.
\end{enumerate}

\subsection{Resultate}
Mit dem Fit aus \gl{lambda_fit} ergibt sich folgende Grafik:

\begin{minipage}{\linewidth}
    	\makebox[\linewidth]{\includegraphics[width=0.9\linewidth]{Lambda_Fit.png}}
    	\captionof{figure}{Fit für die Bestimmung der Wellenlänge $\lambda$}
    	\label{fig:lambda_fit}
\end{minipage}
\vspace{0.5cm}

Für die Fitparameter $A,B,K$ aus \gl{lambda_fit} ergeben sich folgende Werte (die Werte in Klammern sind die Fehlergrenzen bei \SI{95}{\percent}igen Konfidenzintervallen):
\begin{align}
    A = 4280 \hspace{0.1cm} (4136; 4425) \hspace{0.5cm} B = 19600 \hspace{0.1cm} (19500; 19700) \hspace{0.5cm} K = 0,02548 \hspace{0.1cm} (0,01323; 0,03772)
\end{align}

Mit Hilfe von \gl{lambda} ergibt sich für die Wellenlänge $\lambda$:
\begin{align}
	\lambda = \dfrac{\pi \cdot d}{B} = \dfrac{\pi \cdot \SI{0,457}{\micro\meter}}{19600} \approx \SI{732,50}{\nano\meter}
\end{align}
Es wurden (anders als in der Versuchsanleitung beschrieben) \SI{0,457}{\micro\meter} für den Spaltabstand $d$ des Doppelspaltes angenommen, um mit dem Ergebnis für die Wellenlänge in der richtigen Größenordnung zu landen.

\subsection{Diskussion}
In \abb{lambda_fit} ist zu sehen, dass die Fitfunktion die \enquote{misslungene} $\beta$-Korrektur versucht zu korrigieren. Dadurch wirken die Maxima erster, zweiter und dritter Ordnung in positiver und negativer $x$-Richtung wie Ausreißer - diese liegen deutlich ober- bzw. unterhalb der Fitfunktion. \\

Betrachtet man das Ergebnis für die Wellenlänge $\lambda$ fällt auf, dass die gemessenen Photonen deutlich \enquote{röter} sind, als man erwartet. Durch den Filter vor der Glühlampe sollten hauptsächlich Photonen im Wellenlängenbereich \SI{510}{\nano\meter} bis \SI{570}{\nano\meter} - also grünes Licht - passieren. Die Abweichung der gemessenen Werte kann mehrere Ursachen haben.\\
Zunächst ist zu erwähnen, dass der Fit aus \abb{lambda_fit} einen $R^2$-Wert von $R^2 = 0,9749$ hat und somit nicht besonders gut zu den aufgenommenen Messwerten passt. Gute Fits haben $R^2$-Werte von etwa $0,98$ und höher. Dies kann vor allem durch die $\beta$-Korrektur in die \enquote{falsche} Richtung zustande kommen (s.o.). \\
Ein kleiner Beitrag zur Rot-Verschiebung der Wellenlänge kann außerdem durch Photonenstöße mit einzelnen Molekülen und Atomen in der Luft erklärt werden. Durch einen Zusammenstoß mit Molekülen aus der Umgebungsluft kann es zur Energieübertragung von Photonen auf diese Moleküle kommen. Dabei verlieren die Photonen Energie und ihre Wellenlänge verschiebt sich leicht ins Rote.\\
Außerdem verschiebt sich das Emissionspektrum der Glühlampe für niedrigere Leistungseinstellungen ins Rote. Da wir möglichst nur einzelne Photonen durch den Doppelspalt schicken wollen, wurde die Leistungseinstellung der Glühlampe bei etwa der Hälfte des Maximums gewählt. Damit ist das Spektrum schon vor dem Spektralfilter leicht rotverschoben und die Wahrscheinlichkeit, dass ein rot-verschobenes Photon durch den Filter fliegt steigt.\\
Zuletzt ist zu erwähnen, dass der Spektralfilter vor der Glühlampe nicht perfekt ist. Er wurde zwar optimiert möglichst viel Licht der Wellenlänge im Bereich um \SI{550}{\nano\meter} passieren zu lassen, doch ist es häufig nicht vermeidbar, dass auch Licht anderer Wellenlängen noch zu geringen Anteilen durch den Filter kommt. Dieses Licht (z.B. höherer Wellenlängen) kann am PMT detektiert werden und ebenfalls zu einem verfälschten Messergebnis der Wellenlänge führen.\\
Am gravierenstend wird jedoch der Fehler des Fits zu der starken Abweichung (ca. \SI{25}{\percent} zu \SI{550}{\nano\meter}) der Wellenlänge der Photonen beitragen.\\
Der Widerspruch zwischen dem Wellen- und Teilchencharakter des Lichts lässt sich mit Hilfe der Quantenmechanik auflösen. Denn dort wird Licht nicht mehr wie in der klassischen Elektrodynamik als klassische Elektromagnetische Welle aufgefasst, die durch die Maxwell-Gleichungen beschrieben werden kann und die Wellenlänge $\lambda$ aus der Ausbreitungsgeschwindigkeit $c$ folgt gemäß $\lambda=\frac{c}{\nu}$. Stattdessen wird das Licht in der Quantenmechanik als Teilchen mit gequantelter Energie beschrieben, weshalb die Wellenlänge nun dem Impuls der Photonen zugrunde liegt - gemäß $\lambda=\frac{h}{p}$. Demnach kann das Teilchen durch eine Wellenfunktion $\psi(x)$ beschrieben werden, die der Schrödingergleichung $\hat{H}\psi=E\psi$ genügt. Dabei gilt für die Aufenthaltswahrscheinlichkeit eines Photons $\int_{-\infty}^{\infty} |\psi(x)|^2 dx = 1$. Dies impliziert bereits, dass das Licht sich aufgrund der Wellenfunktion $\psi(x)$ wie eine Welle verhält, aber Aufgrund der Aufenthaltswahrscheinlichkeit (ein Teilchen kann gemessen werden) und der Quantelung der Energie hat das Licht Teilcheneigenschaften.

\begin{thebibliography}{9}

\bibitem{} \label{bib:aufbau}
\href{https://www.teachspin.com/two-slit}{https://www.teachspin.com/two-slit} [05.04.2022]

\bibitem{} \label{bib:aufbau_pmt}
\href{https://de.wikipedia.org/wiki/Photomultiplier#/media/Datei:Photomultiplier_schema_de.png}{https://de.wikipedia.org/wiki/Photomultiplier\#/media/Datei:Photomultiplier\_schema\_de.png} [05.04.2022]

\end{thebibliography}

\section*{Erklärung der selbstständigen Anfertigung}
Hiermit erklären wir, dass das vorliegende Protokoll selbständig verfasst und keine anderen als die angegebenen Hilfsmittel verwendet wurden. \\
Insbesondere versichern wir, dass alle genutzten Internetseiten kenntlich gemacht wurden und im Verzeichnis der Internetseiten bzw. im Quellenverzeichnis aufgeführt.\\
Sämtliche Schaltskizzen, Abbildungen sowie Oszilloskopaufnahmen sind selbstständig angefertigt, oder der Versuchsanleitung entnommen. 

\newpage

\appendix

\section{Ausgleichrechnung für die Kalibrierung des PMT}
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.75]{Ausgleichsrechnungsergebnisse.pdf}
    \caption{Lineare Regression für die Kalibrierung des PMT nach \gl{kor2}}
    \label{fig:agr_beta}
\end{figure}
\end{document}