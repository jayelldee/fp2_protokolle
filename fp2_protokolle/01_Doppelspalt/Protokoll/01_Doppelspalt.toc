\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Physikalische Grundlagen}{2}{section.1}%
\contentsline {section}{\numberline {2}Experimenteller Aufbau}{3}{section.2}%
\contentsline {section}{\numberline {3}Aufgabe 1}{4}{section.3}%
\contentsline {subsection}{\numberline {3.1}Aufgabenstellung}{4}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Durchführung}{4}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Resultate}{5}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Diskussion}{8}{subsection.3.4}%
\contentsline {section}{\numberline {4}Aufgabe 2}{9}{section.4}%
\contentsline {subsection}{\numberline {4.1}Aufgabenstellung}{9}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Durchführung}{9}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Resultate}{9}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Diskussion}{10}{subsection.4.4}%
\contentsline {section}{\numberline {A}Ausgleichrechnung für die Kalibrierung des PMT}{12}{appendix.A}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
