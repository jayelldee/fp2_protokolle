%% Workspace aufr�umen
clear;
clc;
load('dspalt.mat','-mat');

%% Konstantendefinition und Normierung bzw. Kalibrierung
Z_D_mean = mean(Z_D);
Z_kor1 = (Z - Z_D_mean);
Z_kor1_n = Z_kor1 ./ max(Z_kor1);
Z_kor2 = (Z - Z_D_mean) ./ (-6.870*x+13636);
Z_kor = Z_kor2 ./ max(Z_kor2);
Z_max = max(Z_kor2);
x_m = x - 250;
x_0 = 248;
L = 0.498;
%% 1.1 Plotten des Interferenzmusters Z_kor1 (Z_D-Korrektur)
%Normierte Z�hlrate korrigiert um den Dunkelstrom
plot(x_m, Z_kor1_n, 'k+');
ylabel ('Z�hlrate Z_{kor1}(x)')
ylim([0, 1.05])
xlabel ('Strecke \Deltax / \mum')
xlim([-300 300]);
grid on

%% 1.2 Plotten des Interferenzmusters Z_kor (Z_D- und -Korrektur)
%Normierte Z�hlrate korrigiert um den Dunkelstrom
plot(x_m, Z_kor, 'k+');
ylabel ('Z�hlrate Z_{kor2}(x)')
ylim([0, 1.05])
xlabel ('Strecke \Deltax / \mum')
xlim([-300 300]);
grid on

%% 1.3 Plotten von Beta
%Z�hlraten ohne Spalte im Versuchsaufbau
plot(x_beta, Z_beta, 'k+');
ylabel ('Z�hlrate Z(x) / 1')
ylim([9000, 14000]) 
xlabel ('Stellschraubenposition x / \mum')
xlim([-5 505]);
grid on

%% 1.4 Fit f�r linearen Ausgleich von 
%Linear-Fit
f_lin = fit(x_beta,Z_beta,'poly1')

%Plotting the linear fit
plot(f_lin,x_beta, Z_beta, 'k+')
legend('Datenpunkte','Linearer Fit')
ylabel ('Z�hlrate Z(x) / 1')
ylim([9000, 14000])
xlabel ('Stellschraubenposition x / \mum')
xlim([-5 505]);
grid on

%% BETA TEST
% %Test wierum man beta-Korrektur anwenden muss
% Z_beta_test = Z_beta ./ (-6.870.*x_beta+13636);
% %Plotting the linear fit
% plot(x_beta, Z_beta_test, 'k+')
% ylabel ('Z�hlrate Z(x) / 1')
% %ylim([0,1000])
% xlabel ('Stellschraubenposition x / \mum')
% xlim([-5 505]);
% grid on

%% 2.1 Fit f�r die Bestimmung der Wellenl�nge der Photonen
%Fit-Formel aus Versuchsanleitung
lambda_approx_va = ['(1/4).*(sin(a.*sin(((x-248).*10.^(-6))./0.489))./(a.*sin(((x-248).*10.^(-6))./0.489))).^2'...
'*(sin(2.*b.*sin(((x-248).*10.^(-6))./0.489))./sin(b.*sin(((x-248).*10.^(-6))./0.489))).^2+c'];

%Fitoptionen
fo = fitoptions('Method','NonlinearLeastSquare',...
    'Lower',[0,0,0],'Upper',[Inf,Inf,1],...
    'DiffMinChange',1.0e-8,...
    'DiffMaxChange',0.1,...
    'MaxFunEvals',600,...
    'MaxIter',400,...
    'TolFun',1.0e-6,...
    'TolX',1.0e-6,...
    'StartPoint',[0.1473,0.6013,0.5325]);
ft1 = fittype(lambda_approx_va,'options',fo);

%Fit
lambdafit_va = fit(x,Z_kor,ft1);


%Fit-Plot
plot(lambdafit_va,x, Z_kor, 'k+')
legend('Datenpunkte', 'Lambda-Fit')
ylabel ('Z�hlrate Z(x) / 1')
ylim([0, 1.05])
xlabel ('Stellschraubenposition x / \mum')
xlim([-20 520]);
grid on