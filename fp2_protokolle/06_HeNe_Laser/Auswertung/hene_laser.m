%% Workspace aufräumen
clear;
clc;
load('henelaser.mat','-mat');

%% Konstantendefinition und Normierung bzw. Kalibrierung
f1 = figure('Name','Beamradius in dependence of distance along o.a.');


%% 1.1 Plotten des Doppelpeaks für 0deg bis 40deg
figure(f1);
func = a*(1+((distance-b)/c)^2)^(1/2);
fit = lsqcurvefit(func,[10,80],distance,beamradius);
times = linspace(distance(1),distance(end));
plot(distance, beamradius, 'k+',times,func(x,times),'b-')
ylabel ('Beamradius / mm')
%ylim([0 16])
xlabel ('Distance / cm')
%xlim([4898 5002]);
grid on
legend('Datapoints', 'Fit');




%% 2 Fitting
% %Linear-Fit
% f_lin = fit(x_beta,Z_beta,'poly1')
% 
% %Plotting the linear fit
% plot(f_lin,x_beta, Z_beta, 'k+')
% legend('Datenpunkte','Linearer Fit')
% ylabel ('Zählrate Z(x) / 1')
% ylim([9000, 14000])
% xlabel ('Stellschraubenposition x / \mum')
% xlim([-5 505]);
% grid on


% %Fit-Formel aus Versuchsanleitung
%lambda_approx_va = ['(1/4).*(sin(a.*sin(((x-248).*10.^(-6))./0.489))./(a.*sin(((x-248).*10.^(-6))./0.489))).^2'...
%'*(sin(2.*b.*sin(((x-248).*10.^(-6))./0.489))./sin(b.*sin(((x-248).*10.^(-6))./0.489))).^2+c'];

% %Fitoptionen
% fo = fitoptions('Method','NonlinearLeastSquare',...
%     'Lower',[0,0,0],'Upper',[Inf,Inf,1],...
%     'DiffMinChange',1.0e-8,...
%     'DiffMaxChange',0.1,...
%     'MaxFunEvals',600,...
%     'MaxIter',400,...
%     'TolFun',1.0e-6,...
%     'TolX',1.0e-6,...
%     'StartPoint',[0.1473,0.6013,0.5325]);
% ft1 = fittype(lambda_approx_va,'options',fo);

% %Fit
% lambdafit_va = fit(x,Z_kor,ft1);
% 
% 
% %Fit-Plot
% plot(lambdafit_va,x, Z_kor, 'k+')
% legend('Datenpunkte', 'Lambda-Fit')
% ylabel ('Zählrate Z(x) / 1')
% ylim([0, 1.05])
% xlabel ('Stellschraubenposition x / \mum')
% xlim([-20 520]);
% grid on