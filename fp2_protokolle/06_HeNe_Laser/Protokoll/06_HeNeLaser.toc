\babel@toc {english}{}
\contentsline {section}{\numberline {1}Tasks}{2}{section.1}%
\contentsline {section}{\numberline {2}Fundamentals}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Working principle of the Helium-Neon-Laser}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}The ABCD-Formalism and derivation of the stability conditions}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}\textsc {Gaussian} beam profiles}{4}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Transversal electromagnetic modes (TEMs) and mirror reflectivities}{4}{subsection.2.4}%
\contentsline {section}{\numberline {3}Experimental Setup and Methods}{5}{section.3}%
\contentsline {subsection}{\numberline {3.1}Setup}{5}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Methods}{5}{subsection.3.2}%
\contentsline {section}{\numberline {4}Results and Discussion}{7}{section.4}%
\contentsline {subsection}{\numberline {4.1}Laser power in dependence of the cavity mirror distance}{7}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Analyzation of the beam radius with changing cavity mirror distance}{8}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Laser output power in dependence of the discharge current}{10}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Determination of the finesse of the resonator}{10}{subsection.4.4}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
