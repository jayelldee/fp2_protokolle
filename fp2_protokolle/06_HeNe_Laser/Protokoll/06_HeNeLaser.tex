\documentclass[a4paper,10pt,headsepline=2pt,footinclude=false]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{graphicx}
\graphicspath{{Bilder/}}
\usepackage{xcolor}
\usepackage[left=2cm,right=1.5cm,top=2.5cm,bottom=2cm]{geometry}
\usepackage{scrlayer-scrpage}
\usepackage[version=3]{mhchem}
\usepackage{csquotes}
\usepackage{mdframed}
\usepackage{multirow}
\usepackage{caption}
\usepackage{hyperref}
\usepackage{siunitx}
\sisetup{locale = DE, per-mode = fraction, separate-uncertainty}
\usepackage{ulem}
\usepackage{appendix}
\usepackage{float}
\usepackage{subfig}
\usepackage{enumitem}
\usepackage{helvet}
\newcommand{\sst}[1]{_{\text{#1}}}
\newcommand{\ra}{$\rightarrow$ }
\newcommand{\eq}[1]{Eq.(\ref{eq:#1})}
\newcommand{\fig}[1]{Fig. \ref{fig:#1}}
\newcommand{\tab}[1]{Tab. \ref{tab:#1}}
\newcommand{\bib}[1]{[\ref{bib:#1}]}
\renewcommand{\familydefault}{\sfdefault}
\newcommand*\myappendixchanges{%
  \setcounter{table}{0}%
  \gdef\thesection{A.\arabic{section}}%
  \gdef\thetable{A.\arabic{table}}}


\lohead{\includegraphics[width=4cm]{logo}}			%Logo in der Kopfzeile
\cohead{Advanced laboratory course 2\\[0.2cm] Alex Kreis \& Jannik Dierks - Group 1}	%Text in der Kopfzeile
\rohead{\thepage}									%Seitenzahl
\cfoot[]{}											%keine Fußzeile
\setkomafont{headsepline}{\color{red!70!black}}		%Farbe der Trennlinie	
\setkomafont{pagehead}{\bfseries}					%Im Kopf Fett geschrieben
\usepackage{colortbl}								%bringt Farbe in die Tabelle
\definecolor{dunkelgrau}{gray}{0.8}					%Farbe definieren
\definecolor{hellgrau}{gray}{0.9}					%Farbe definieren
\definecolor{rot}{rgb}{0.616,0.051,0.082}			%Farben definieren 0-100%

%Links zu Gleitumgebung von figure und table:
%http://tex.lickert.net/tipps/optionh/optionh.html
%https://www.texwelt.de/fragen/3427/wann-sollte-ich-gleitumgebungen-fur-tabellen-und-abbildungen-verwenden

\setlength\parindent{0pt}

%--------------------------------------------------------------------------------
%	DOKUMENT
%--------------------------------------------------------------------------------
\begin{document}

\renewcommand{\figurename}{Fig.}
\counterwithin{figure}{section}
\renewcommand{\tablename}{Tab.}
\counterwithin{table}{section}
\counterwithin{equation}{section}

\begin{center}
	\huge \textbf{Realisation of a Helium-Neon-Laser}\\\vspace{3pt}
	\Large Lead author: Jannik Dierks \\
	\large Second author: Alex Kreis \\
	\Large Group 1\\
	\vspace{0.5cm}
		\large Institute for Physics - University of Rostock - \today\\
	Supervisor: Mr. Trelin
\end{center}

\tableofcontents
\vspace{0.5cm}

\section*{Aim of the course}
To get to know laser setups, in this course a HeNe-Laser is build from scratch. It is aligned and characterised to familiarise the students with working on optics.
\newpage
	
\section{Tasks}
\begin{enumerate}
	\item Build a Helium-Neon-Laser with the help of an alignment laser.
	\item With the ABCD-Formalism derive the stability conditions for the cavity. Measure the area of stability and compare it to the theory.
	\item Measure the beam radius inside the resonator at different positions. Discuss the data for the focal radius $W_0$ and the \textsc{Rayleigh}-Length ($z_R$) concerning the theoretical beamline.
	\item Observe if the discharge current saturates the output power of the laser. 
	\item Calculate the Finesse of the cavity by measuring the reflectivities of the mirrors.
\end{enumerate}


\section{Fundamentals}
\subsection{Working principle of the Helium-Neon-Laser}
As in every laser the main setup consists of different essential components. The \textit{active medium}, the \textit{cavity} and the \textit{pump}.\\
The active medium is the crucial component, in which the lasing happens. In our case it is a gas mixture of roughly \SI{70}{\percent} Helium and \SI{30}{\percent} Neon trapped in a vacuumised glass tube.\\
The cavity consists of two concave mirrors, which reflect the produced light of the active medium between one another.\\
At last we need a component to start the process of lasing and keep it going. Therefor energy needs to be induced into the Helium-Neon gas tube. This is done by an electrode combination where one electrode sits on each end of the gas tube. Once you apply a voltage, the cathode produces electrons, which can ionise the gas mixture and start the lasing.\\

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.9\linewidth]{HeNeLaserEnergyLevels.png}}
	\captionof{figure}{Schematic diagram of the energy levels in Helium and Neon that are important to understand the way the Helium-Neon laser is realised. \bib{LaserEnergyLevels}}
	\label{fig:LaserEnergyLevels}
\end{minipage}
\vspace{0.5cm}

To understand why lasing happens it is sensible to look at the energy levels of the active medium that is ionised (\fig{LaserEnergyLevels}). The Helium atoms are ionised by the pump-electrons and lifted into the $2^3S_1$ and $2^1S_0$ states. Once the Helium ions sit on the higher energy levels, they have got enough energy (combined with the energy supplied by hitting a Neon atom) to ionise Neon to the $5s$ and $4s$ states. The population inversion for Neon (more atoms are in the excited energy state, than in the ground state) is reached, due to the Helium gas.\\
This is where the laser transition is located in Neon ($5s$ and $4s$). Once the population inversion is reached, the Neon atoms drop to meta-stable levels ($4p$ and $3p$) in between the ground and excited state. This drop only happens when a photon passes that has the same wavelength as the energy gap between the two states (stimulated emission). Falling down to the $4p$ and $3p$ states, Neon emits photons with the exact same properties as the one that stimulates the emission - which in this case corresponds to the wavelengths $\lambda_1 = \SI{3,39}{\um}$, $\lambda_2 = \SI{632,8}{\nm}$ and $\lambda_3 = \SI{1,15}{\um}$, depending on the transitions.\\
This means, that the emitted photons are already matched in their polarisation, frequency (wavelength) and direction of propagation to the ones, that stimulate the emission. This process is amplified by the two cavity mirrors which reflect the produced light back into the active medium, where the photons then can stimulate more emissions.\\
To ensure, that only the \SI{632,8}{\nm} photons leave the laser setup, the cavity mirrors are coated in a special way, such that they transmit $\lambda_2$ better than the other two wavelengths $\lambda_1$ and $\lambda_3$. Furthermore the ends of the vacuum tube are sealed with so called Brewster windows, that filter out $\lambda_1$ and $\lambda_3$.\\
From the meta-stable state $3p$ Neon drops back into the ground state by radiating to the $3s$ state and then diffusing energy to the glass walls.\\
As the cavity mirrors are not one-hundred percent reflective (see above) a small portion of the laser light with $\lambda = \SI{632,8}{\nm}$  can be coupled out from inside the cavity. This is the light we see as the laser beam.

\subsection{The ABCD-Formalism and derivation of the stability conditions}
In optics, the propagation of beams can be approximated with the so called ABCD-Formalism. In the following $x$ will describe the distance of the beam with respect to the optical axis (o.a.) and $\alpha$ the angle between the o.a. and the beam.
\begin{align}
    \begin{pmatrix}
    x_2\\
    \alpha_2
    \end{pmatrix} =
    \begin{pmatrix}
    A & B\\
    C & D
    \end{pmatrix} \cdot
    \begin{pmatrix}
    x_1\\
    \alpha_1
    \end{pmatrix}
    \label{eq:ABCD}
\end{align}

To describe a beam propagating along the o.a. by the distance $L$, the matrix $A_1$ is used. A reflection on the concave cavity mirrors is characterised by matrix $A_2$, where $R_i$ are the radii of the corresponding concave mirrors $i$.
\begin{align}
    A_1 = 
    \begin{pmatrix}
    1 & L\\
    0 & 1
    \end{pmatrix}
    \hspace{1cm}
    A_2 =
    \begin{pmatrix}
    1 & 0\\
    -\frac{2}{R_i} & 1
    \end{pmatrix}
    \label{eq:A1A2}
\end{align}

A full beam propagation through the resonator can therefor be noted as
\begin{align}
    A&=
    \begin{pmatrix}
    1 & L\\
    0 & 1
    \end{pmatrix}
    \begin{pmatrix}
    1 & 0\\
    -\dfrac{1}{R_2} & 1
    \end{pmatrix}
    \begin{pmatrix}
    1 & L\\
    0 & 1
    \end{pmatrix}
    \begin{pmatrix}
    1 & 0\\
    -\dfrac{1}{R_1} & 1
    \end{pmatrix}\nonumber\\
    &=
    \begin{pmatrix}
    1-\dfrac{2L}{R_1}+\dfrac{4L^2}{R_1R_2}-\dfrac{2L}{R_2}-\dfrac{2L}{R_1} & 2L-\dfrac{2L^2}{R_2}\\
    \dfrac{4L^2}{R_1R_2}-\dfrac{2L}{R_2}-\dfrac{2L}{R_1} & 1-\dfrac{2L}{R_2}
    \end{pmatrix}\nonumber\\
    &=
    \begin{pmatrix}
    4g_1g_2-2g_2-1 & 2Lg_2\\
    \dfrac{1}{L}(4g_1g_2-2g_1-2g_2) & 2g_2-1
    \end{pmatrix}
    \label{eq:FullPropagation}
\end{align}
with $g_i = 1 - \dfrac{L}{R_i}$. The corresponding Eigenvalues for the matrix $A$ can be calculated with
\begin{align}
    \det(A-\lambda I) = \lambda^2 -2\lambda(2g_1g_2 - 1)+1 = 0\nonumber\\
    \rightarrow \lambda_{1|2} = 2g_1g_2 - 1 \pm \sqrt{(2g_1g_2-1)^2-1}.
\end{align}

The stability conditions can be derived to 
\begin{align}
    |2g_1g_2 - 1| \leq 1 \hspace{0.5cm} \Rightarrow \hspace{0.5cm} 0 \leq g_1g_2 \leq 1
    \label{eq:StabilityConditions}
\end{align}

\subsection{\textsc{Gaussian} beam profiles}
A \textsc{Gaussian} beam describes the profile of a laser beam as a \textsc{Gaussian} fit where its width varies with the propagation distance $z$ along the optical axis. In other words the longer the beam propagates along the optical axis, the broader the \textsc{Gaussian} beam gets flattening the maximum but therefor maintaining the area under the \textsc{Gaussian} curve.\\

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.9\linewidth]{GaussianBeam.jpg}}
	\captionof{figure}{Schematic propagation of a Gaussian beam. Differing from our setup in this case a concave-planar lens (focal length $f$) was used to focus the beam, but the same principles of the \textsc{Rayleigh}-Length and beam waste-profiles apply. This is just a notation difference. $2\theta$ is the full divergence angle. \bib{GaussianBeam}}
	\label{fig:GaussianBeam}
\end{minipage}
\vspace{0.5cm}

As one can see in Fig. (\ref{fig:GaussianBeam}) the beam radius starts to expand after the point $z_C$ which is the point with the smallest beam radius $W(z_C) = W_0$. After this point the beam starts to expand with the following correlation:
\begin{align}
    W(z) = W_0\cdot \left[ 1 + \left( \dfrac{z-z_C}{z_R} \right)^2 \right]^{1/2}
    \label{eq:BeamRadius}
\end{align}
In this equation $W(z)$ describes the beam radius in dependence of the propagation distance $z$, $z_C$ the starting point of beam radius expansion and $z_R$ the \textsc{Rayleigh}-Length. In our case it is sensible to assume, that our system of coordinates satisfies $z_C = 0$. Therefor $W_0 = W(z_C = 0)$ is associated with the focal beam radius. This means that the origin of our coordinate system is located in the point of the smallest beam waste. This way \eqref{eq:BeamRadius} simplifies to 
\begin{align}
    W(z) = W_0\cdot \left[ 1 + \left( \dfrac{z}{z_R} \right)^2 \right]^{1/2}.
    \label{eq:BeamRadiusEasy}
\end{align}
The \textsc{Rayleigh}-Length $z_R$ is determined by the uncertainty realtion
\begin{align}
    z_R = \dfrac{\pi W_0^2}{\lambda}
    \label{eq:RayleighLength}
\end{align}

\subsection{Transversal electromagnetic modes (TEMs) and mirror reflectivities}
The laser can operate in different transversal electromagnetic modes (TEMs). These mainly differ in the number of knots in the beam profile. They are labeled with TEM-$xy$ where $x$ and $y$ are the numbers of knots in the horizontal and vertical direction of the profile.\\
When perfectly aligned, the laser should operate in the basic mode - TEM-00 - which correlates to a \textsc{Gaussian} beam. However it is fairly easy to see the TEM-10 or TEM-01 mode (one not in either horizontal or vertical direction) when aligning the laser, because the angle of the cavity mirrors can excite a different mode, when one of the higher modes is more stable than the basic mode. This will be shown later.\\

To characterise mirrors one of the essential values is its reflectivity $R$. It is a number between 1 and 0 and represents the percentage of the incoming intensity that is reflected back off the surface.\\
Together with the transmittance $0 \leq T \leq 1$ of a mirror (the percentage of transmitted intensity with respect to the initial intensity $I_0$) the following correlation can be created:
\begin{align}
    R_i = \dfrac{I_0 - T_i}{I_0}
    \label{eq:ReflectivityMirror}
\end{align}
Note, that this calculates the reflectivity of only one mirror $M_i$. To determine the total reflection of the resonator we need to calculate
\begin{align}
    R = \left( \prod_{i = 1}^n R_i \right)^{1/2}
\end{align}
which in our case leaves us with
\begin{align}
    R = \sqrt{R_1R_2}
    \label{eq:ReflectivityResonator}
\end{align}

With this we can now determine the Finesse ${\cal F}$ of the cavity by having a look at the overall reflectivity of the resonator. ${\cal F}$ is calculated by
\begin{align}
    {\cal F} = \dfrac{\pi \sqrt{R}}{1-R}.
    \label{eq:Finesse}
\end{align}


\section{Experimental Setup and Methods}
\subsection{Setup}
 \begin{minipage}{\linewidth}
 	\makebox[\linewidth]{\includegraphics[width=0.9\linewidth]{Setup.png}}
 	\captionof{figure}{Schematic of the experimental setup. \enquote{o.a.} represents the optical axis of the system and $M1$ and $M2$ the cavity mirrors. Both $M1$ and $M2$ are convocal mirrors with a curvature radius of $r_1 = r(M1) = \SI{700}{\mm}$ and $r_2 = r(M2) = \SI{1000}{\mm}$. Depending on the task performed, the alignment laser is turned off (see section \enquote{Methods}).}
 	\label{fig:Setup}
\end{minipage}
\vspace{0.5cm}

\subsection{Methods}
\begin{enumerate}
    \item Alignment:
        \begin{enumerate}[label=\roman*.]
            \item With the help of a pinhole the beam of the alignment laser is orientated along the mounting rail axis
            \item Next the two mirrors are aligned. Therefor the setup from \fig{Setup} is build, but without mounting the active medium and mirror $M2$. By sending the alignment laser onto mirror $M1$ the beam can be reflected directly back into the laser. This way mirror $M1$ is aligned.
            \item The glass tube with the active medium is inserted into the setup and $M1$ removed from its mount. The tube is aligned such that in the far field (on a screen about \SI{1}{\m} away from the tube) the beam profile looks as clean as possible. Mirror $M1$ can then be mounted again.
            \item $M2$ is aligned by shooting the laser on the back surface of $M2$ and centering the beam position by adjusting the angle and position of $M2$.
            \item The laser current is switched on and by fine tuning the adjustments of $M1$ and $M2$ the fundamental mode TEM-00 is set up.
         \end{enumerate} 
    \item Verifying the ABCD-Formalism by measuring the beam power in depence of the distance $d$ between $M1$ and $M2$:
        \begin{enumerate}[label=\roman*.]
            \item Once the laser is properly aligned (see \enquote{Alignment}) $M2$ is placed as close as possible to the glass tube.
            \item With a powermeter the laser output power (i.e. the power of the outcoupled light) is measured thrice. In order to measure the actual output power, the background light is measured by blocking the laser and noting the background value.
            \item $M2$ is now moved away from the glass tube in \SI{5}{\cm} steps (at first, later a finer stepsize is used) and the measurements from step ii. are repeated. 
        \end{enumerate}
    \item Measuring the \textsc{Rayleigh}-Length $z_R$ and the focal radius $W_0$:
        \begin{enumerate}[label=\roman*.]
            \item Mirror $M2$ is placed about \SI{80}{\cm} behind the glass tube and a slit is inserted between the resonator tube and $M2$.
            \item The slit is opened as far as possible and placed as close as possible to the active medium.
            \item Now the slit is aligned to the laserbeam, such, that the laser hits the slit in the center. The slit is closed until the laser disappears. At that point the measurement for the beam diameter is taken.
            \item The slit is moved towards $M2$ and step iii) is repeated.
        \end{enumerate}
    \item Measuring the depencene of output power in respect to the discharge current and the reflective indices of the mirrors $M1$ and $M2$:
        \begin{enumerate}[label=\roman*.]
            \item $M2$ is placed at a fixed distance to the active medium.
            \item Now the discharge current of the laser is increased in roughly \SI{0,5}{\mA} steps and the laser output power is measured with the powermeter.
            \item All optical parts are removed from the setup, except for the alignment laser and $M1$. The alignment laser is turned on and shot onto $M1$ without any other optical elements in the beam line. With the powermeter the laser power in front (input power) and behind (transmitted power) the mirror is measured - five times each. The process is repeated for $M2$.
        \end{enumerate}
\end{enumerate}



\section{Results and Discussion}
\subsection{Laser power in dependence of the cavity mirror distance}
\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.9\linewidth]{DistancePowerPlot.png}}
	\captionof{figure}{The figure shows the laser power $P$ in dependence of the distance $d$ of the cavity mirrors. Within the range of \SI{68}{\cm} to \SI{100}{\cm} no signal was observed and the values were therefor noted as zero.}
	\label{fig:LaserpowerDistance}
\end{minipage}
\vspace{0.5cm}

\fig{LaserpowerDistance} shows the stability conditions mentioned in \eq{StabilityConditions} very good. The laser power suddenly drops at about $L=\SI{60}{\cm}$ resonator length and is zero at \SI{67}{\cm}. After roughly \SI{30}{\cm} without any lasing, the laser power increases at \SI{100}{\cm} resonator length and rises back to the operating power.\\
This roughly confirms the theoretical solution for the stability condition. By writing \eq{StabilityConditions} as
\begin{align}
    0\leq\left(1-\frac{L}{R_1}\right)\left(1-\frac{L}{R_2}\right)\leq1
\end{align}
it is visible in which distances $L$ the stability conditions is fulfilled for known osculating circles $R_1$ and $R_2$. In this case $R_1=70$ and $R_2=100$, meaning that both g are positive for $L\leq70$ and both negative for $L\geq100$ resulting in a positive product. Thus, the stability condition is met and a laser beam is achieved. For distances $L$ between 70 and 100 only $g_1$ is negative resulting in a negative product. Hence the stability condition is violated and no laser beam is achieved. This can be confirmed by the results (\fig{LaserpowerDistance}), only the lower limit is less by $\SI{3}{\centi\meter}$. This could be caused by an error in the osculating circle of one mirror. Another error could be an imperfect alignment of the mirrors and the tube containing the He-Ne gas mixture.\\
Important to note here is that the left flank of the plot dips at around \SI{52}{\cm} and increases after that again. The theoretical course on the other hand is mirrored, such that left and right flank are the same. The drop at \SI{52}{\cm} therefor is irregular. This could be the result of different mode of the laser being measured.\\

\subsection{Analyzation of the beam radius with changing cavity mirror distance}
\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{BeamradiusFit.png}}
	\captionof{figure}{The beam radius $W(z)$ in dependence of the distance $z$ of the cavity mirrors is fitted with the help of equation \eqref{eq:BeamRadius}. To achieve a converging fit, the parameter $z_C$ is fixed at its theoretical value - see \eqref{eq:TheoreticalFocalPoint} - $z_C = \SI{27}{cm}$. The $R^2$-Value of this fit is $R^2=\SI{0,74516}{}$.}
	\label{fig:BeamradiusFit}
\end{minipage}
\vspace{0.5cm}

The measurement data for the beam radius has been fitted with equation \eqref{eq:BeamRadius}. $z_C$ has been fixed at the theoretical value of \SI{27}{\cm} due to the following correlation:
\begin{align}
    z_1 - z_C &= \dfrac{-(z_2-z_1)(R_2-(z_2 - z_1))}{R_2-R_1-2(z_2-z_1)}\nonumber\\
    z_C &= \SI{5}{\cm} + \dfrac{-(\SI{115}{\cm}-\SI{5}{\cm})(\SI{100}{\cm}-(\SI{115}{\cm} - \SI{5}{\cm}))}{\SI{100}{\cm}-(-\SI{70}{\cm})-2(\SI{115}{\cm}-\SI{5}{\cm})}\nonumber\\
    z_C &= \SI{27}{\cm}
    \label{eq:TheoreticalFocalPoint}
\end{align}

Furthermore the \textsc{Rayleigh}-Length can be calculated as:
\begin{align}
    z_R^2 &= \dfrac{-(z_2-z_1)(R_2-(z_2 - z_1))(R_2-R_1-(z_2-z_1))}{(R_2-R_1-2(z_2-z_1))^2}\nonumber\\
    z_R &= \sqrt{\dfrac{-(\SI{115}{\cm}-\SI{5}{\cm})(\SI{100}{\cm}-(\SI{115}{\cm} - \SI{5}{\cm}))(\SI{100}{\cm}-(-\SI{70}{\cm})-(\SI{115}{\cm}-\SI{5}{\cm}))}{(\SI{100}{\cm}-(-\SI{70}{\cm})-2(\SI{115}{\cm}-\SI{5}{\cm}))^2}}\nonumber\\
    &= \SI{5,1381}{\cm}
    \label{eq:TheoreticalRayleighLength}
\end{align}

This leaves us with the focal radius
\begin{align}
    W_0 &= \sqrt{\dfrac{\lambda \cdot z_R}{\pi}} = \sqrt{\dfrac{\SI{632,8}{\nm} \cdot \SI{5,1381}{\cm}}{\pi}}\nonumber\\
    &= \SI{0,1017}{\mm}.
    \label{eq:TheoreticalBeamWaste}
\end{align}

Equations \eqref{eq:TheoreticalFocalPoint} through \eqref{eq:TheoreticalBeamWaste} can be found in \bib{FundamentalsOfPhotonics}.

The fit from \fig{BeamradiusFit} results in the following values for $W_0$ and $z_R$, whilst the focal point $z_C$ is fixed to \SI{27}{\cm}:
\begin{align}
    W_0 &= \SI{0,43596}{\mm} \pm \SI{0,05775}{\mm}\nonumber\\
    z_R &= \SI{31,84172}{\cm} \pm \SI{6,09788}{\cm}
    \label{eq:FitValues}
\end{align}
The overall $R^2$-Value of the fit is $R^2 = \SI{0,74516}{}$.\\

In comparison to the fit with only $z_C$ being fixed (see \fig{BeamradiusFit}), this is the curve shape for \textit{all parameters} fixed to their theoretical values:

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{BeamradiusTheoreticalCurveShape.png}}
	\captionof{figure}{The figure shows the beam radius $W(z)$ in dependence of the distance $z$ of the cavity mirrors. The red line corresponds to equation \eqref{eq:BeamRadius}, with the fixed parameters $W_0 = \SI{0,1017}{\mm}, z_C = \SI{27}{\cm}$ and $z_R = \SI{5,1381}{\cm}$.}
	\label{fig:TheoreticalCurveShape}
\end{minipage}
\vspace{0.5cm}

If one compares the theoretical curve shape shown in \fig{TheoreticalCurveShape} to the fitted curve in \fig{BeamradiusFit}, it becomes quite clear, that our measured data is quite far off from the theoretical model. The model only fits to an $R^2$-Value of \SI{0,05263}{} to our measurement data.\\ 
This is why the fit from \fig{BeamradiusFit} is probably more meaningful, allthough the value for $z_C$ was assumed to be fixed at \SI{27}{\cm} - its theoretical value. The fit achieves an $R^2$-Value of \SI{0,74516}{}, which is still not very good, but an order of magnitude better, than the theoretical model.\\
Concerning the fit values \eqref{eq:FitValues}, one can not compare them to the theoretical values shown in \eqref{eq:TheoreticalFocalPoint} through \eqref{eq:TheoreticalBeamWaste}. This is due to the fact, that the theoretical model has such a bad $R^2$-Value compared to the fit - meaning the fit actually applies better to our data than the theoretical model.\\
One way to verify the fit however, is to look at the absolute errors of our fit parameters. Here one can observe, that the fit in \fig{BeamradiusFit} (due to its still very low $R^2$-Value) has high error bars - about \SI{19,1}{\percent} for $z_R$ and \SI{13,2}{\percent} for $W_0$. Its quite clear, that the errors are too high and therefor the measurement has to be claimed as inaccurate.\\
As an outlook to eliminate such errors it might be possible to measure both flanks of the beam (between $M2$ and the cavity \textit{and} between $M1$ and the cavity), such that the fit can be approximated with two beam flanks rather than one. This should give a better $R^2$-Value and rise the accuracy of the data in comparison to the theory.


\subsection{Laser output power in dependence of the discharge current}
\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.9\linewidth]{DischargecurrentPowerPlot.png}}
	\captionof{figure}{The figure shows the laser output power in dependence of the discharge current applied to the active medium}
	\label{fig:CurrentPower}
\end{minipage}

In \fig{CurrentPower} it can be seen, that in the beginning the power of the laser strongly rises with a higher discharge current. At a current of around $\SI{4,5}{\milli\ampere}$ the ascent decreases. This indicates the form of a saturation curve meaning that the laser power will steadily approach a limit. Even with an immense discharge current the power wont go past the limit. Unfortunately the device wont allow a higher charge to prove the saturation, but first signs are visible. Nevertheless by theoretically thinking about the cause of the form of the curve, we can be certain that \fig{CurrentPower} depicts a saturation curve. In the beginning there are only a few electrons because of the low current. Compared to that there are a lot of He and Ne atoms that can be hit. Thus, every electron is able to excite the atoms into higher states. The more electrons are available due to a higher current, the more atoms can be excited, hence the laser power rises. However at some point as the numbers of electrons and atoms get closer it becomes less likely for an electron to hit a non excited atom. In this way the laser power no longer increases as sharply as in the beginning. Now with more electrons the power only gains slowly, because it gets a little bit more likely for electron to hit one of the few remaining non excited atoms if there are more electrons. In the extreme case a limit is reached when every possible atom is excited. So the maximum power of the laser is reached and is limited by the number of He and Ne atoms. This process perfectly describes a saturation curve and is supported by the received data of this experiment.

\subsection{Determination of the finesse of the resonator}

First of all the reflectivity of the mirrors has to be determined. Therefor the input power and transmitted power was measured.\\

\begin{table}[H]
\begin{center}
\begin{tabular}{|c|c|c|}
\hline
Mirror & Transmitted Power / \si{\uW}	&	Input Power / \si{\mW}	\\
\hline
1	&	17,02	&	33,7\\
1	&	16,80	&	32,4\\
1	&	17,60	&	33,6\\
1	&	16,60	&	33,9\\
1	&	17,40	&	34,2\\
2	&	10,02	&	35,7\\
2	&	9,41	&	35,2\\
2	&	9,60	&	37,3\\
2	&	10,53	&	35,3\\
2	&	12,66	&	35,8\\
\hline
\end{tabular}
\end{center}
\caption{Transmitted power as well as the input power of the mirrors $M1$ and $M2$}
\label{tab:Finesse}
\end{table}

With these results the average values can be calculated:\\
\begin{align}
    T_1&=\SI{17,08}{\uW}\nonumber\\
    T_2&=\SI{10,44}{\uW}\nonumber\\
    I&=\SI{34,71}{\mW}
\end{align}

The reflectivity for the mirrors is calculated with \eqref{eq:ReflectivityMirror} giving us $R_1=\SI{0,9995}{}$ and $R_2= \SI{0,9997}{}$, which leads to a reflectivity for the resonator \eqref{eq:ReflectivityResonator} $R=\sqrt{R_1R_2}=\SI{0,9996}{}$. Finally the finesse can be determined with \eqref{eq:Finesse} resulting in ${\cal F}= \SI{7852,41}{}$. This means the resonator has a high spectral resolving power, hence sharp resonances are achieved. This implies the build Laser is well working. Nevertheless way higher finesses up to a few \SI{100000}{} are possible. But for the primitive build used for this experiment the finesse is high enough.



\begin{thebibliography}{9}

    \bibitem{} \label{bib:LaserEnergyLevels}
    \href{https://i.stack.imgur.com/TFujw.png}{https://i.stack.imgur.com/TFujw.png} [20.05.2022]
    
    \bibitem{} \label{bib:GaussianBeam}
    \href{https://www.primes.de/files/userImages/news/Strahlkaustik_01.jpg}{https://www.primes.de/files/userImages/news/Strahlkaustik\_01.jpg} [20.05.2022]
    
    \bibitem{} \label{bib:FundamentalsOfPhotonics}
    B. E. A. Saleh et. al., M. C. Teich. Fundamentals Of Photonics. John Wiley \& Sons, Incorporated, 2013.  [20.05.2022]
    

\end{thebibliography}

%\appendix





\section*{Erklärung der selbstständigen Anfertigung}
Hiermit erklären wir, dass das vorliegende Protokoll selbständig verfasst und keine anderen als die angegebenen Hilfsmittel verwendet wurden. \\
Insbesondere versichern wir, dass alle genutzten Internetseiten kenntlich gemacht wurden und im Verzeichnis der Internetseiten bzw. im Quellenverzeichnis aufgeführt.\\
Sämtliche Schaltskizzen, Abbildungen sowie Oszilloskopaufnahmen sind selbstständig angefertigt, oder der Versuchsanleitung entnommen. 

\end{document}
