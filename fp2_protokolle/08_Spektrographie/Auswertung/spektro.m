%% Workspace aufräumen
clear;
clc;
load('spektro.mat','-mat');

%% Dateneditierung
a = 4.28783e8;
b = 9.84355e13;
c = -824.61535;
x_Channels = linspace(1,8160,8160);
HgSz_y = HgSpaltzu.';
HgSo_y = HgSpaltoffen.';
HgSpektrum = [x_Channels(:),HgSz_y(:),HgSo_y(:)];
Na_y = Na.';
Zn_y = Zn.';
He1_y = He1.';
He2_y = He2.';
HeSo_y = HeSo.';
LED_y = LED.';
MIF1_y = MIF1.';
MIF2_y = MIF2.';
lambda = sqrt((a+sqrt(a.^2+4.*b.*(x_Channels-c)))./(2.*(x_Channels-c)));
NaZnHeSpektra = [x_Channels(:),Na_y(:),Zn_y(:),He1_y(:),He2_y(:),HeSo_y(:)];
MIFSpektra = [x_Channels(:),lambda(:),LED_y(:),MIF1_y(:),MIF2_y(:)];
csvwrite('HgSpektrum.csv', HgSpektrum)
csvwrite('NaZnHeSpektra.csv', NaZnHeSpektra)
csvwrite('MIFSpektra.csv', MIFSpektra)