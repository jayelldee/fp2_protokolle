\documentclass[a4paper,10pt,headsepline=2pt,footinclude=false]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{graphicx}
\graphicspath{{Bilder/}}
\usepackage{xcolor}
\usepackage[left=2cm,right=1.5cm,top=2.5cm,bottom=2cm]{geometry}
\usepackage{scrlayer-scrpage}
\usepackage[version=3]{mhchem}
\usepackage{csquotes}
\usepackage{mdframed}
\usepackage{multirow}
\usepackage{caption}
\usepackage{hyperref}
\usepackage{siunitx}
\sisetup{locale = DE, per-mode = fraction, separate-uncertainty}
\usepackage{ulem}
\usepackage{appendix}
\usepackage{float}
\usepackage{subfig}
\usepackage{helvet}
\usepackage[shortlabels]{enumitem}

\newcommand{\sst}[1]{_{\text{#1}}}
\newcommand{\ra}{$\rightarrow$ }
\newcommand{\gl}[1]{Gl.(\ref{eq:#1})}
\newcommand{\abb}[1]{Abb. \ref{fig:#1}}
\newcommand{\tab}[1]{Tab. \ref{tab:#1}}
\newcommand{\bib}[1]{[\ref{bib:#1}]}
\renewcommand{\familydefault}{\sfdefault}
\newcommand*\myappendixchanges{%
  \setcounter{table}{0}%
  \gdef\thesection{A.\arabic{section}}%
  \gdef\thetable{A.\arabic{table}}}


\lohead{\includegraphics[width=4cm]{logo}}			%Logo in der Kopfzeile
\cohead{Fortgeschrittenenpraktikum 2\\[0.2cm] Alex Kreis \& Jannik Dierks - Gruppe 1}	%Text in der Kopfzeile
\rohead{\thepage}									%Seitenzahl
\cfoot[]{}											%keine Fußzeile
\setkomafont{headsepline}{\color{red!70!black}}		%Farbe der Trennlinie	
\setkomafont{pagehead}{\bfseries}					%Im Kopf Fett geschrieben
\usepackage{colortbl}								%bringt Farbe in die Tabelle
\definecolor{dunkelgrau}{gray}{0.8}					%Farbe definieren
\definecolor{hellgrau}{gray}{0.9}					%Farbe definieren
\definecolor{rot}{rgb}{0.616,0.051,0.082}			%Farben definieren 0-100%

%Links zu Gleitumgebung von figure und table:
%http://tex.lickert.net/tipps/optionh/optionh.html
%https://www.texwelt.de/fragen/3427/wann-sollte-ich-gleitumgebungen-fur-tabellen-und-abbildungen-verwenden

\setlength\parindent{0pt}

%--------------------------------------------------------------------------------
%	DOKUMENT
%--------------------------------------------------------------------------------
\begin{document}

\renewcommand{\figurename}{Abb.}
\counterwithin{figure}{section}
\renewcommand{\tablename}{Tab.}
\counterwithin{table}{section}
\counterwithin{equation}{section}

\begin{center}
	\huge \textbf{Spektrographie}\\\vspace{3pt}
	\Large Erstautor: Jannik Dierks \\
	\Large Alex Kreis\\
	\Large Gruppe 1\\
	\vspace{0.5cm}
	\large Institut für Physik - Universität Rostock - \today\\
	Versuchsbetreuer*in: Herr Ehrhardt
\end{center}

\tableofcontents
\vspace{0.5cm}

\section*{Ziel des Versuchs}
Spektrographie ist eine experimentelle Technik, die aus vielen Laboren heute nicht mehr wegzudenken ist. Neben ihrer großen Bedeutung in der Astrophysik und chemischen Analytik besteht ebenfalls ein breites Anwendungsfeld in Optiklaboren.\\

Sie sollen in diesem Versuch zum Einen die Funktionsweise eines Prismenspektrographen kennenlernen und bei dem Aufbau des Spektrographen experimentelle Kompetenzen erwerben. Zum Anderen ist die Auseinandersetzung mit physikalischen Sachverhalten, die mit einem Spektrographen untersucht werden können, Bestandteil dieses Praktikumversuchs. Sie sollen Kenndaten von Metallinterferenzfiltern bestimmen.
\newpage

	
\section{Aufgabenstellung}
Bestimmen Sie Mittenwellenlänge und Halbwertsbreite der wellenlängenabhängigen Transmission $T(\lambda)$ eines Metallinterferenzfilters mithilfe eines Spektrographen.


\section{Physikalische Grundlagen}
Metallinterferenzfilter oder \enquote{Fabry-Perot-Etalons} sind optische Bauelemente zum Filtern von Licht bestimmter Wellenlängen aus einem Spektrum (analog zu einem Bandpassfilter in der Elektro-Technik). Dabei besteht das Etalon aus einer dielektrischen Schicht, auf deren plan-parallele Seitenflächen eine Schicht Metall aufgedampft wird. Diese Spiegelflächen fungieren als Resonator. Trifft Licht auf den Filter, wird es in den Resonator eingekoppelt und reflektiert mehrfach an den beiden Spiegelflächen - s. \abb{Metallinterferenzfilter}. \\

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.5\linewidth]{Metallinterferenzfilter.jpg}}
	\captionof{figure}{Strahlenverlauf eines einfallenden Lichtstrahls am Metallinterferenzfilter. $\theta$ ist dabei der Winkel zum Lot, das orthogonal auf der planaren Spiegelfläche steht. $n$ und $n_0$ sind die jeweiligen Brechungsindizes der Materialien und $l$ der Abstand der beiden Spiegelflächen (also die Dicke der dielektrischen Schicht). $T_0$ und $T_1$ beschreiben die transmittierten Strahlen. \bib{Metallinterferenzfilter}}
	\label{fig:Metallinterferenzfilter}
\end{minipage}
\vspace{0.5cm}

Da die Spiegelflächen eine Reflektivität kleiner als eins haben, wird jedes Mal bei einer Reflexion innerhalb des Resonators auch etwas Licht transmittiert. Diese transmittierten Lichtstrahlen interferieren hinter dem Metallinterferenzfilter miteinander. Abhängig von der Dicke $l$ der dielektrischen Schicht löschen sich so bestimmte Wellenlängen aus und das Spektrum des einfallenden Lichts wird gefiltert.\\

Die Intensität der Transmission $T$ kann dabei in Abhängigkeit der Wellenlänge $\lambda$ wie folgt dargestellt werden:
\begin{align}
    T(\lambda) = \dfrac{I_t}{I_i} = \dfrac{\left( 1 - \dfrac{A}{1-R} \right)^2}{1 + \dfrac{4R}{(1-R)^2}\cdot \sin^2(\frac{\delta}{2})}
    \label{eq:TransmissionWellenlaenge}
\end{align}
Dabei ist $I_t$ die transmittierte und $I_i$ die einfallende Intesität. $R$ beschreibt die Reflektivität der Spiegelflächen und $A$ für die Absorptionseffekte and diesen. $\delta$ ist in unserem Fall der Gangunterschied zwischen den einzelnen Strahlen nachdem diese das Etalon passiert haben (in \abb{Metallinterferenzfilter} mit $l_0$ gekennzeichnet).\\
Für $\delta$ kann der Zusammenhang
\begin{align}
    \delta = \dfrac{2\pi}{\lambda}nl \cdot \cos{\theta}
    \label{eq:Delta}
\end{align}
mit dem Brechungsindex $n$, der Etalon-Dicke $l$ und dem Einfallswinkel $\theta$ hergestellt werden. $\lambda$ beschreibt wie oben auch die Wellenlänge des einfallenden Lichts.\\

Nimmt man an, dass der Zähler aus \gl{TransmissionWellenlaenge} eine Konstante darstellt und vereinfachend $\dfrac{4R}{(1-R)^2}$ als $F$ dargestellt wird, nimmt \gl{TransmissionWellenlaenge} die Form einer Airy-Funktion an.
\begin{align}
    T = \dfrac{1}{1+F\sin^2(\frac{\delta}{2})}
\end{align}
Betrachtet man diese Funktion genauer, erkennt man, dass sie ein Maximum für \begin{align}
    \lambda = 2nl\cos{\theta} = \dfrac{\delta}{\pi} = \lambda_c
\end{align}
hat. $\lambda_c$ wird in diesem Zusammenhang Mittenfrequenz genannt. Die Mittenfrequenz ist eine wichtige Eigenschaft des Metallinterferenzfilters, um diesen zu charakterisieren. Die Wellenlänge der Mittenfrequenz passiert den Filter mit der höchsten Intensität.\\

Eine weitere wichtige Eigenschaft des Fabry-Perot-Etalons ist seine Halbwertsbreite $\gamma$. Ähnlich wie die Finesse ${\cal F}$ eines Resonators kann die Halbwertsbreite eine Aussage über die Auflösung bzw. die Güte des Metallinterferenzfilters treffen. Sie ist definiert über 
\begin{align}
    \gamma = \dfrac{2\lambda_c}{\pi}\cdot \dfrac{1}{\sqrt{\dfrac{4R}{(1-R)^2}}} = \dfrac{2\lambda_c}{\pi}\cdot \dfrac{1}{\sqrt{F}}
\end{align}

\section{Experimenteller Aufbau}
\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{Spektrograph.png}}
	\captionof{figure}{Schematischer Versuchsaufbau des Prismenspektrographen. $LQ$ beschreibt dabei die jeweilige Lichtquelle. $L_0$ ist die Kollimations-Linse, um das stark divergente Licht der einzelnen Lichtquellen zu kollimieren. $S_1$ und $S_2$ stehen für Spiegel, die das kollimierte Licht in den Spektrographen-Aufbau lenken.$L_1: f = \SI{50}{\mm} , L_2: f = \SI{75}{\mm}, L_3: f = \SI{300}{\mm}$ sind Linsen, die für den Fokus des Lichtstrahls in den Einzelspalt $Sp$ bzw. ($L_3$) für die Fokussierung der Spektrallinien auf die CCD-Zeile zuständig sind. $B_1$ und $A$ sind Blenden, die die jeweilige Strahlbreite begrenzen. $Sp$ beschreibt einen verstellbaren Einzelspalt, der die Maximale Intensität des Lichts begrenzt, das durch den Spektrographen geschickt wird und $P$ das Prisma, das für die spektrale Aufspaltung des Lichts sorgt. Am Ende des Aufbaus ist eine CCD-Zeile eingebaut, die die Intensität des Lichtes räumlich (1D) getrennt in $x$-Richtung auflöst.}
	\label{fig:AufbauSpektro}
\end{minipage}
\vspace{0.5cm}

Bei einem Spektrographen wird die Dispersionseigenschaft eines Prismas ausgenutzt um Licht verschiedener Wellenlängen voneinander zu trennen. Die Dispersion im Prisma sorgt für verschiedene Weglängen des Lichts, abhängig von der Wellenlänge. So treffen die Strahlenbündel verschiedener Wellenlängen bereits räumlich versetzt auf die Fokuslinse $L_3$ (s. \abb{AufbauSpektro} und \abb{Linse3}), die den Effekt in ihrem Brennpunkt verstärkt.

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.5\linewidth]{Linse_3.png}}
	\captionof{figure}{Laterale Trennung des Lichts für verschiedene Wellenlängen durch die Kombination eines dispersiven Elementes $D$ und einer fokussierenden Linse $L_3$. Das dispersive Element ist in unserem Fall ein Prisma (s. \abb{AufbauSpektro}).}
	\label{fig:Linse3}
\end{minipage}
\vspace{0.5cm}

Die oben genannte laterale Aufspaltung der einzelnen Wellenlängen wird über den Zusammenhang
\begin{align}
    \Delta x = f_3 \cdot \dfrac{d\theta}{d\lambda} \cdot \Delta\lambda
    \label{eq:LatAufspaltung}
\end{align}
beschrieben. $f_3$ ist die Brennweite der Linse $L_3$ und $\dfrac{d\theta}{d\lambda}$ die Abhängigkeit des Austrittswinkels aus dem dispersiven Element von der Wellenlänge.\\
Es gilt 
\begin{align}
    \dfrac{d\theta}{d\lambda} = \dfrac{d\theta}{dn}\dfrac{dn}{d\lambda}.
    \label{eq:Dispersionsrelation}
\end{align}
Mithilfe der Cauchy-Gleichung \bib{Cauchy} wird die Abhängigkeit des Brechungsindex von der Wellenlänge angenähert durch
\begin{align}
    n(\lambda) = \sum_{i=0}^\infty \dfrac{B_i}{\lambda^{2i}}.
    \label{eq:CauchyGleichung}
\end{align}
\gl{LatAufspaltung} kann nun mithilfe von \gl{Dispersionsrelation} und \gl{CauchyGleichung} als
\begin{align}
    dx = f_3 \dfrac{d\theta}{dn}\left(-\dfrac{2B_1}{\lambda^3}-\dfrac{4B_2}{\lambda^5}\right)d\lambda
    \label{eq:Differentialform}
\end{align}
geschireben werden. Dabei wurden die Differenzen $\Delta x$ und $\Delta \lambda$ durch infinitesimale Größen ersetzt und der Ausdruck als Differenzialform umgeschrieben. Integrieren und Umformen liefert das nötige Fit-Modell zur Kalibrierung des Spektrographen
\begin{align}
    \lambda = \sqrt{\dfrac{a+\sqrt{a^2+4b(x-c)}}{2(x-c)}}
    \label{eq:FitModell}
\end{align}
mit 
\begin{align}
    a = f_3\dfrac{d\theta}{dn}B_1 \hspace{0.5cm} und \hspace{0.5cm} b = f_3\dfrac{d\theta}{dn}B_2.
\end{align}
$c$ in \gl{FitModell} kommt durch die Integration von \gl{Differentialform} zu \gl{FitModell} zustande.\\

\subsection*{Materialien}
\begin{minipage}[t]{0.48\linewidth}
	\begin{itemize}[label=-]
    	\item Linsen der Marke ThorLabs (Brennweiten $f_1 = \SI{50}{\mm}, f_2 = \SI{75}{\mm}, f_1 = \SI{300}{\mm}$)
    	\item Justage Laser Thorlabs
    	\item Verschiedene Lichtquellen (Hg-Lampe, Na-Lampe, He-Lampe, Nikkel-Lampe, LED)
    	\item Thorlabs Spiegel
	\end{itemize}
\end{minipage}
\hfill
\begin{minipage}[t]{0.48\linewidth}
	\begin{itemize}[label=-]
    	\item Diverse Blenden
    	\item Verstellbarer Einzelspalt Thorlabs
    	\item Drehbares Prisma
    	\item Strahlblocker 
	\end{itemize}
\end{minipage}

\section{Durchführung}
\begin{enumerate}
    \item Der Spektrograph wird wie in \abb{AufbauSpektro} gezeigt aufgebaut und einjustiert.
    \item Mithilfe der Hg-Lampe wird ein Kalibrierungs-Spektrum für die CCD-Zeile aufgenommen.
    \item Die Hg-Lampe als Lichtquelle wird gegen eine Na-, Ni- und He-Lampe sowie eine LED ausgetauscht, der Strahl neu kollimiert und jeweils ein Spektrum für jede Lampe aufgenommen.
    \item Mit der LED als Lichtquelle werden hinter (im Sinne des optischen Weges) Spiegel $S_2$ nacheinander zwei verschiedene Metallinterferenzfilter eingesetzt und erneut jeweils ein Spektrum für jeden Filter aufgenommen.
\end{enumerate}

\section{Resultate}


\section{Diskussion}





\begin{thebibliography}{9}

    \bibitem{} \label{bib:Metallinterferenzfilter}
    \href{http://what-when-how.com/wp-content/uploads/2011/08/tmp69338.jpg}{http://what-when-how.com/wp-content/uploads/2011/08/tmp69338.jpg}[03.06.2022]
    \bibitem{} \label{bib:Cauchy}
    \href{https://iopscience.iop.org/article/10.1088/0953-8984/13/17/309/pdf}{D Y Smith et al 2001 J. Phys.: Condens. Matter 13 3883}[03.06.2022]

\end{thebibliography}

\section*{Erklärung der selbstständigen Anfertigung}
Hiermit erklären wir, dass das vorliegende Protokoll selbständig verfasst und keine anderen als die angegebenen Hilfsmittel verwendet wurden. \\
Insbesondere versichern wir, dass alle genutzten Internetseiten kenntlich gemacht wurden und im Verzeichnis der Internetseiten bzw. im Quellenverzeichnis aufgeführt.\\
Sämtliche Schaltskizzen, Abbildungen sowie Oszilloskopaufnahmen sind selbstständig angefertigt, oder der Versuchsanleitung entnommen. 

\end{document}
