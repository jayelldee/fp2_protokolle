\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Aufgabe 1}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Aufgabenstellung}{2}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Physikalische Grundlagen}{2}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Experimenteller Aufbau}{2}{subsection.1.3}%
\contentsline {subsection}{\numberline {1.4}Durchführung}{2}{subsection.1.4}%
\contentsline {subsection}{\numberline {1.5}Resultate}{2}{subsection.1.5}%
\contentsline {subsection}{\numberline {1.6}Diskussion}{2}{subsection.1.6}%
\contentsline {section}{\numberline {2}Aufgabe 2}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Aufgabenstellung}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Physikalische Grundlagen}{2}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Experimenteller Aufbau}{2}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}Durchführung}{2}{subsection.2.4}%
\contentsline {subsection}{\numberline {2.5}Resultate}{3}{subsection.2.5}%
\contentsline {subsection}{\numberline {2.6}Diskussion}{3}{subsection.2.6}%
\contentsline {section}{\numberline {3}Aufgabe 3}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Aufgabenstellung}{3}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Physikalische Grundlagen}{3}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Experimenteller Aufbau}{3}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Durchführung}{3}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Resultate}{3}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Diskussion}{3}{subsection.3.6}%
\contentsline {section}{\numberline {4}Aufgabe 4}{3}{section.4}%
\contentsline {subsection}{\numberline {4.1}Aufgabenstellung}{3}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Physikalische Grundlagen}{3}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Experimenteller Aufbau}{3}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Durchführung}{3}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Resultate}{3}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Diskussion}{3}{subsection.4.6}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
