\babel@toc {english}{}
\contentsline {section}{\numberline {1}Tasks}{2}{section.1}%
\contentsline {section}{\numberline {2}Fundamentals}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Schrödinger equation for hydrogen atoms}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Sound waves in a spherical resonator}{2}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Similarities and differences}{3}{subsection.2.3}%
\contentsline {section}{\numberline {3}Experimental Setup}{4}{section.3}%
\contentsline {section}{\numberline {4}Methods}{4}{section.4}%
\contentsline {section}{\numberline {5}Results}{5}{section.5}%
\contentsline {subsection}{\numberline {5.1}The spectrum of a spherical resonator}{5}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Analysing the splitted resonance at \SI {5}{\kHz } in detail}{5}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Creating polar diagrams for the spectrum resonances}{6}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}Progression of the resonator spectrum when inserting spacer rings between the two resonator hemispheres}{8}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}Investigating the $l = 1$ resonance for different spacer thicknesses}{9}{subsection.5.5}%
\contentsline {subsection}{\numberline {5.6}Creating azimuth polar diagrams of the relevant resonances to assign the magnetic quantum number $m$}{10}{subsection.5.6}%
\contentsline {section}{\numberline {6}Discussion}{11}{section.6}%
\contentsline {section}{\numberline {A}Original measurement data}{12}{appendix.A}%
\contentsline {subsection}{\numberline {A.1}Task 1}{12}{subsection.A.1}%
\contentsline {section}{\numberline {B}Task 2}{13}{appendix.B}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
