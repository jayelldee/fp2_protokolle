\documentclass[a4paper,10pt,headsepline=2pt,footinclude=false]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{graphicx}
\graphicspath{{Bilder/}}
\usepackage{xcolor}
\usepackage[left=2cm,right=1.5cm,top=2.5cm,bottom=2cm]{geometry}
\usepackage{scrlayer-scrpage}
\usepackage[version=3]{mhchem}
\usepackage{csquotes}
\usepackage{mdframed}
\usepackage{multirow}
\usepackage{caption}
\usepackage{hyperref}
\usepackage{siunitx}
\sisetup{locale = DE, per-mode = fraction, separate-uncertainty}
\usepackage{ulem}
\usepackage{appendix}
\usepackage{float}
\usepackage{subfig}
\usepackage{enumitem}
\usepackage{helvet}
\newcommand{\sst}[1]{_{\text{#1}}}
\newcommand{\ra}{$\rightarrow$ }
\newcommand{\eq}[1]{Eq.(\ref{eq:#1})}
\newcommand{\fig}[1]{Fig. \ref{fig:#1}}
\newcommand{\tab}[1]{Tab. \ref{tab:#1}}
\renewcommand{\familydefault}{\sfdefault}
\newcommand*\myappendixchanges{%
  \setcounter{table}{0}%
  \gdef\thesection{A.\arabic{section}}%
  \gdef\thetable{A.\arabic{table}}}


\lohead{\includegraphics[width=4cm]{logo}}			%Logo in der Kopfzeile
\cohead{Advanced laboratory course 2\\[0.2cm] Alex Kreis \& Jannik Dierks - Group 1}	%Text in der Kopfzeile
\rohead{\thepage}									%Seitenzahl
\cfoot[]{}											%keine Fußzeile
\setkomafont{headsepline}{\color{red!70!black}}		%Farbe der Trennlinie	
\setkomafont{pagehead}{\bfseries}					%Im Kopf Fett geschrieben
\usepackage{colortbl}								%bringt Farbe in die Tabelle
\definecolor{dunkelgrau}{gray}{0.8}					%Farbe definieren
\definecolor{hellgrau}{gray}{0.9}					%Farbe definieren
\definecolor{rot}{rgb}{0.616,0.051,0.082}			%Farben definieren 0-100%

%Links zu Gleitumgebung von figure und table:
%http://tex.lickert.net/tipps/optionh/optionh.html
%https://www.texwelt.de/fragen/3427/wann-sollte-ich-gleitumgebungen-fur-tabellen-und-abbildungen-verwenden

\setlength\parindent{0pt}

%--------------------------------------------------------------------------------
%	DOKUMENT
%--------------------------------------------------------------------------------
\begin{document}

\renewcommand{\figurename}{Fig.}
\counterwithin{figure}{section}
\renewcommand{\tablename}{Tab.}
\counterwithin{table}{section}
\counterwithin{equation}{section}

\begin{center}
	\huge \textbf{Quantum Analogs - A spherical resonator as a model for a hydrogen atom}\\\vspace{3pt}
	\Large Lead author: Alex Kreis \\
	\large Second author: Jannik Dierks\\
	\Large Group 1\\
	\vspace{0.5cm}
		\large Institute for Physics - University of Rostock - \today\\
	Supervisor: Mr. Trelin
\end{center}

\tableofcontents
\vspace{0.5cm}

\section*{Aim of the course}
With the help of the mathematical analogy between the Schrödinger-Equation and the wave equation for sound waves, the important properties of a quantum mechanical system are analysed. The focus lays on a hydrogen atom, simulated by a spherical resonator.
\newpage
	
\section{Tasks}
\begin{enumerate}
	\item[1)] Determine the frequency spectrum of the spherical resonator for $\alpha=\SI{0}{\degree}$ and $\alpha=\SI{180}{\degree}$. Why are are these spectra different? Analyse the spectrum for $\alpha=\SI{0}{\degree}$ in an area around \SI{5}{\kilo\hertz} more detailed. Why is the resonance splitted?
	\item[2)] Plot the amplitude $A(\theta)$ in relationship to the polar angle $\theta$ in a polar diagram for the first five resonances at $\alpha=\SI{180}{\degree}$. Use these diagrams to assign the quantum number $l$ to the corresponding resonance.
	\item [3)] Insert a spacer (\SI{3}{\mm}, \SI{6}{\mm}, \SI{9}{\mm}) between the resonator halfs and again determine the spectrum. A splitting of the peaks should be visible. How can you explain this? Plot a diagram describing the splitting of the peaks in relationship to the thickness of the spacer for the $l=1$-resonance. Again use the polar diagrams to analyse the amplitude $A(\varphi)$ in relationship to the azimuth angle $\varphi$. Use these results to assign the magnetic quantum number $m$ to the corresponding resonances.
\end{enumerate}


\section{Fundamentals}
\subsection{Schrödinger equation for hydrogen atoms}
It is useful to develop the Schrödinger equation for the hydrogen atom in spherical coordinates since it is surrounded by a radially symmetric potential. Thus 
            \begin{align}
                E\psi=\frac{\hbar^2}{2m}\left[\frac{\partial}{\partial r}\left(r^2\frac{\partial\psi}{\partial r}\right)+\frac{1}{r^2\sin{\theta}}\frac{\partial}{\partial\theta}\left(\sin{\theta}\frac{\partial\psi}{\partial\theta}\right)+\frac{1}{r^2\sin^2{\theta}}\frac{\partial^2\psi}{\partial\varphi^2}\right]-\frac{e^2}{r}\psi
                \label{eq:schröd}
            \end{align}
is the relevant schrödinger equation with the reduced plank constant $\hbar$, the mass $m$ and charge $e$ of an electron and $\psi\equiv\psi(r,\theta,\varphi)$. The wave function can be splitted into two parts, one for the radius $r$ and one for the two angles $\theta$ and $\varphi$:
            \begin{align}
                \psi(r,\theta,\varphi)=\chi_{n'l}(r)Y^m_l(\theta,\varphi)
                \label{eq:sep}
            \end{align}
with the spherical harmonics
            \begin{align}
                Y^m_l(\theta,\varphi)=(-1)^m\sqrt{\frac{(2l+1)}{4\pi}\frac{(l-m)!}{(l+m)!}}P^m_l(\cos{\theta})e^{im\varphi}
                \label{eq:sphar}
            \end{align}
where $P^m_l(\cos{\theta})$ are the ordinary Legendre polynomials. The quantum numbers in \eq{sep} are the radial quantum number $n' (n'\in\mathbb{N})$, the angular momentum quantum number $l (l\in[0,n-1])$ and the magnetic quantum number $m (m\in[-l,l])$. However it is more common to use the main quantum number $n=l+1+n' (n\in\mathbb{N})$ instead of $n'$. After seperating the schrödinger equation \eq{schröd} into two seperate differential equations we get
            \begin{align}
                -\left[\frac{1}{sin\theta}\frac{\partial}{\partial\theta}\left(\sin{\theta}\frac{\partial}{\partial\theta}\right)+\frac{1}{\sin^2{\theta}\frac{\partial^2}{\partial\varphi^2}}\right]Y^m_l(\theta,\varphi)=l(l+1)Y^m_l(\theta,\varphi)
                \label{eq:sepwink}
            \end{align}
and
            \begin{align}
                \left[-\frac{\hbar^2}{2m}\frac{\partial^2}{\partial r^2}(r)-\frac{l(l+1)\hbar^2}{2mr^2}-\frac{e^2}{r}\right]\chi_{n'l}(r)=E\chi_{n'l}(r).
                \label{eq:seprad}
            \end{align}
The possible energy states of a hydrogen atom are given by the eigenvalues of \eq{seprad} and are given by
            \begin{align}
                E_{n'l}=-\left(\frac{e^2}{8\pi\varepsilon_0a_0}\right)\frac{1}{(l+n'+1)^2}=-\left(\frac{e^2}{8\pi\varepsilon_0a_0}\right)\frac{1}{n^2}=E_n
                \label{eq:ener}
            \end{align}
where $a_0$ is the Bohr radius and $\varepsilon_0$ is the vacuum permittivity.

\subsection{Sound waves in a spherical resonator}
The propagation of sound waves in air can be described with the help of the euler equation
            \begin{align}
                \frac{\partial\vec{u}}{\partial t}+\frac{1}{\rho}\nabla p(\vec{r},t)=0
                \label{eq:euler}
            \end{align}
and the continuity equation
            \begin{align}
                \frac{\partial\rho}{\partial t}+\rho\nabla\cdot\vec{u}=0
                \label{eq:conti}
            \end{align}
with $\vec{u}$ as the velocity in air, $p$ as pressure and $\rho$ as volumetric mass density of air. Additionaly the density of air can be connected with the airpressure by the equation:
            \begin{align}
                \frac{\partial p}{\partial\rho}-\frac{1}{\kappa\rho}=0
                \label{eq:connec}
            \end{align}
with $\kappa$ as compressibility. These equations can be combined into a wave equation for airpressure:
            \begin{align}
                \left(\nabla^2-\frac{1}{v^2}\frac{\partial^2}{\partial t^2}\right)p(\vec{r},t)=0
                \label{eq:comb}
            \end{align}
with $v^2=\frac{1}{\kappa\rho}$ as the speed of sound. For this experiment it is interesting to look at the normal modes of a spherical resonator. This means it is possible to convert the timedepending wave equation to the timeindepending Helmholtz equation, by splitting of a time factor $\cos{(\omega t)}$ from $p(\textbf{r},t)$. Thus $p(\vec{r},t)=p(\vec{r})\cos{(\omega t)}$ and 
            \begin{align}
                \left(\nabla^2+\frac{\omega}{v^2}\right)p(\vec{r})=0 .
                \label{eq:helm}
            \end{align}
In spherical coordinates and by seperating the variables $p(\vec{r})=f_l(r)Y^m_l(\theta,\varphi)$, the equations above can be formed into two differential equations:
            \begin{align}
                -\left[\frac{1}{\sin{\theta}}\frac{\partial}{\partial\theta}\left(\sin{\theta}\frac{\partial}{\partial\theta}\right)+\frac{1}{\sin^2{\theta}}\frac{\partial^2}{\partial\varphi^2}\right]S^m_l(\theta,\varphi)=l(l+1)S^m_l(\theta,\varphi)
                \label{eq:diff1}
            \end{align}
            \begin{align}
                -\left[\frac{\partial^2}{\partial r^2}+\frac{2}{r}\frac{\partial}{\partial r}-\frac{l(l+1)}{r^2}\right]f_l(r)=\frac{\omega^2}{v^2}f_l(r)
                \label{eq:diff2}
            \end{align}            
with $S^m_l(\theta\varphi)$ as the real spherical harmonics. The solution of \eq{diff2} is given as the spherical Bessel function $j_l(\frac{\omega r}{v})$.The resonance frequenies of the resonator can be written as
            \begin{align}
                \omega_{nl}=\frac{v}{R}z_{nl} ,
                \label{eq:reso}
            \end{align}
with R as the radius of the resonator and $z_{nl}$ as the nth zero of the function $j_l(x)$.

\subsection{Similarities and differences}
\begin{enumerate}
    \item [(1)]
    The schrödinger equation \eq{sepwink} is equivalent to the helmholtz equation \eq{diff1} in terms of the angular fraction. In contrast the radial fractions are different. The reason for this, is the coulomb potential appearing in the schrödinger equation while the helmholtz equation has no potential. Thus the solutions and their corresponding resonances can not be compared quantitativly. Nevertheless the quantum numbers $n'$, $l$ and $m$ can be assigned to the resonces for both problems.
    \item[(2)]
    The dispersion relations are  different in both equations. For schrödinger:
    \begin{align}
        \omega=\frac{\hbar}{2m}k^2
        \label{eq:disrelschrö}
    \end{align}
    whereas sound waves hold a linear dispersion relation:
    \begin{align}
        \omega=vk
        \label{eq:disrelsound}
    \end{align}
    with k as the wavenumber (or fourier transform of $\nabla^2$) for both cases.
    \item[(3)]
    The wave function is a complex funktion ($\psi(\vec{r},t)\in\mathbb{C}$), whilst the function for air pressure is a real function ($p(\vec{r},t)\in\mathbb{R}$). Thus the time fraction for $\psi(\vec{r},t)$ is given as $\exp{(-i\frac{E}{\hbar}t)}$ and for $p(\vec{r},t)$ as $\cos{(\omega t)}$. Furthermore the spherical harmonics are real in the case of the spherical resonator:
    \begin{align}
        S^m_l(\theta,\varphi)=
        \begin{cases}
        \frac{1}{\sqrt{2}}\left[Y^{-m}_l(\theta,\varphi)+Y^{m}_l(\theta,\varphi)\right] & , m > 0 ,\\
        \frac{i}{\sqrt{2}}\left[Y^{-m}_l(\theta,\varphi)-Y^{m}_l(\theta,\varphi)\right] & , m < 0 .\\
        \end{cases}
        \label{eq:realharm}
    \end{align}
\end{enumerate}

\section{Experimental Setup}
\subsubsection*{Setup}
\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.9\linewidth]{QuA_setup.png}}
	\captionof{figure}{Schematic of the experimental setup}
	\label{fig:setup}
\end{minipage}

\subsubsection*{Materials}
\begin{minipage}[t]{0.48\linewidth}
	\begin{itemize}[label=-]
    	\item Spherical resonator with a build-in speaker and a microphone  
    	\item Analogue oszilloscope
    	\item Signal amplifier
	\end{itemize}
\end{minipage}
\hfill
\begin{minipage}[t]{0.48\linewidth}
	\begin{itemize}[label=-]
    	\item Spacer rings (thicknesses \SI{3}{\mm} and \SI{6}{\mm})
    	\item PC with data-aquisition software
	\end{itemize}
\end{minipage}

\section{Methods}
\label{sec:methods}
for 1)
    \begin{itemize}[label=-]
    	\item The spherical resonator is set up following \fig{setup} with the microphone on the toop hemisphere and the speaker on the bottom hemisphere.
    	\item To set $\alpha=\SI{180}{\degree}$ the microphone should be opposite to the speaker.
    	\item It is necessary to be aware of the relationship between $\alpha$ and the polar angle $\theta$ following 
    	    \begin{align}
                \theta=\arccos{\left[\frac{1}{2}(\cos{\alpha}-1)\right]}
                \label{eq:theta}
            \end{align}
        \item With the help of the programm \enquote{SpectrumSLC} the frequency spectrum between $\SI{100}{\hertz}$ and $\SI{10}{\kilo\hertz}$ can be determined (resolution of $\SI{10}{\hertz}$ and integration time of $\SI{50}{\milli\second}$).
        \item The same is repeated for $\alpha=\SI{0}{\degree}$ (microphone and speaker on the same side).
        \item For $\alpha=\SI{0}{\degree}$ the resonance at $\SI{5}{\kilo\hertz}$ is analised with a higher resolution ($\SI{1}{\hertz}$) and integration time (\SI{100}{\milli\second}).
        \item This is repeated for $\alpha=\SI{20}{\degree}$ and $\alpha=\SI{40}{\degree}$.
	\end{itemize}
for 2)
        \begin{itemize}[label=-]
    	\item Similar to 1), the resonance spectrum is determined for $\alpha=\SI{180}{\degree}$ between $\SI{2}{\kilo\hertz}$ and $\SI{7}{\kilo\hertz}$ with a resolution of $\SI{10}{\hertz}$. 
    	\item In the program \enquote{SpectrumSLC} the resonance frequency is selected by clicking on the peak.
    	\item With the help of the function \enquote{Measure Wave Function} a polar diagram showing the amplitude in relationship to $\theta$ can be plotted.
	    \item For that $\alpha$ is set to $\SI{0}{\degree}$ and the amplitude can be measured in $\SI{10}{\degree}$ steps. The angle has to be changed manually and the measurement of the amplitude has to be starded manually as well.
        \item To receive a complete diagram the option \enquote{Complete by Symmetry} is activated.
        \item This is repeated for every peak.
	\end{itemize}
for 3)
        \begin{itemize}[label=-]
    	\item The first three resonances for $\alpha=\SI{180}{\degree}$ are determined with the same method described in 2). 
    	\item This is repeated for different inserted spacers $\SI{3}{\milli\meter}$, $\SI{6}{\milli\meter}$ and $\SI{9}{\milli\meter}$. For each spacer the $l=1$ resonances is analised more detailed.
    	\item With the help of the function \enquote{Measure Wave Function} a polar diagram is plotted for the splitted peaks. In this case is $\alpha=\theta$, because of the different symmetry caused by the spacer (vertical axis is longer than horizontel axis). 
    	\item The quantum number $m$ is now assigned to the corresponding peak.
    	\item This is repeated for $l=2$.
	\end{itemize}


\section{Results}
\subsection{The spectrum of a spherical resonator}
Following the instructions described in the section \enquote{Methods} (p. \pageref{sec:methods}) one can acquire the following spectra for the spherical resonator:\\

\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A1/A1_0_sweep.png}}
	\captionof{figure}{Frequency spectrum for $\alpha=\SI{0}{\degree}$}
	\label{fig:spectrum_0deg}
\end{minipage}
\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A1/A1_180_sweep.png}}
	\captionof{figure}{Frequency spectrum for $\alpha=\SI{180}{\degree}$}
	\label{fig:spectrum_180deg}
\end{minipage}

\subsection{Analysing the splitted resonance at \SI{5}{\kHz} in detail} 
The splitted peak around \SI{5}{\kHz} is of high interest in order to demonstrate the dependency of the measured amplitude with respect to the angle $\alpha$. Therefor it is contemplated in more detail in the following figure:\\

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.7\linewidth]{A1/peaks_at_5kHz_crosses.png}}
	\captionof{figure}{Detailed sweep of the resonant peak at \SI{5}{\kHz} for different rotation angles $\alpha$ of the spherical resonator}
	\label{fig:5k_sweep}
\end{minipage}

\vspace{0.2cm}

The original data for the peak around \SI{5}{\kHz} can be found in the appendix (\fig{appendix_5k_0deg}).

\subsection{Creating polar diagrams for the spectrum resonances}
To match the quantum number $l$ to the corresponding resonances from the spectrum (\fig{spectrum_180deg}) it is crucial to map the resonance in respect to the location of the microphone. This way it is easier to get a spatial image of said resonance. In the following this is achieved by plotting the measured amplitude of the microphone in respect to the tilt between nothern and southern hemisphere of the resonator. The results are displayed in polar diagrams which are shown below:

\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A2/A2_wave_function_2294hz_2.png}}
	\captionof{figure}{Amplitude of the resonance at \SI{2294}{\Hz} with respect to the angle $\alpha$}
	\label{fig:polar_2294}
\end{minipage}
\hspace{0.2cm}
\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A2/A2_wave_function_3675hz.png}}
	\captionof{figure}{Amplitude of the resonance at \SI{3675}{\Hz} with respect to the angle $\alpha$}
	\label{fig:polar_3675}
\end{minipage}

\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A2/A2_wave_function_4963hz.png}}
	\captionof{figure}{Amplitude of the resonance at \SI{4963}{\Hz} with respect to the angle $\alpha$}
	\label{fig:polar_4963}
\end{minipage}
\hspace{0.2cm}
\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A2/A2_wave_function_6205hz.png}}
	\captionof{figure}{Amplitude of the resonance at \SI{6205}{\Hz} with respect to the angle $\alpha$}
	\label{fig:polar_6205}
\end{minipage}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.5\linewidth]{A2/A2_wave_function_6550hz.png}}
	\captionof{figure}{Amplitude of the resonance at \SI{6550}{\Hz} with respect to the angle $\alpha$}
	\label{fig:polar_6550}
\end{minipage}

\subsection{Progression of the resonator spectrum when inserting spacer rings between the two resonator hemispheres}
\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A3/A3_180°_2_bis_7kHz_0mm.png}}
	\captionof{figure}{Spectrum of the resonator at $\alpha = \SI{180}{\degree}$ and \SI{0}{\mm} spacing} 
	\label{fig:spectrum_0mm}
\end{minipage}
\hspace{0.2cm}
\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A3/A3_180°_2_bis_7kHz_3mm.png}}
	\captionof{figure}{Spectrum of the resonator at $\alpha = \SI{180}{\degree}$ and \SI{3}{\mm} spacing} 
	\label{fig:spectrum_3mm}
\end{minipage}

\vspace{0.5cm}

\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A3/A3_180°_2_bis_7kHz_6mm.png}}
	\captionof{figure}{Spectrum of the resonator at $\alpha = \SI{180}{\degree}$ and \SI{6}{\mm} spacing} 
	\label{fig:spectrum_6mm}
\end{minipage}
\hspace{0.2cm}
\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A3/A3_180°_2_bis_7kHz_9mm.png}}
	\captionof{figure}{Spectrum of the resonator at $\alpha = \SI{180}{\degree}$ and \SI{9}{\mm} spacing} 
	\label{fig:spectrum_9mm}
\end{minipage}

 
\subsection{Investigating the $l = 1$ resonance for different spacer thicknesses}
\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{A3/magnetic_shift_peak.png}}
	\captionof{figure}{Detailed progression of the resonance at $l=1$ for different spacer thicknesses}
	\label{fig:l1_resonance_peak}
\end{minipage}
\vspace{0.2cm}

To analyse the splitting of the peaks properly, the peak splitting (in \si{\Hz}) is displayed in the next plot in relation to the spacer thickness:

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{A3/splitting_linear_fit.png}}
	\captionof{figure}{Linear fit for the peak splitting of the $l=1$ resonance in realtion to the spacer thickness}
	\label{fig:linfit_splitting}
\end{minipage}

This fit was created by using the standar linear fit model with 
\begin{align}
    f(x) = mx+n
\end{align}
where $f(x)$ is the frequency shift and $x$ the spacer thickness. This fit produces the following values for $m$ and $n$:
\begin{align}
    n = \SI{2,2}{\hertz} \hspace{0.5cm} \text{and} \hspace{0.5cm} m = \SI{18,73}{\Hz\per\mm}
\end{align}

\subsection{Creating azimuth polar diagrams of the relevant resonances to assign the magnetic quantum number $m$}

\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[scale=0.32]{A3/A3_azimut_cut_l1_2090Hz.png}}
	\captionof{figure}{Amplitude $A$ in relation to the azimuth angle $\varphi$ at the resonance of \SI{2090}{\Hz} ($l=1$ resonance)} 
	\label{fig:azimuth_2090}
\end{minipage}
\hspace{0.2cm}
\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A3/A3_azimut_cut_l1_2258Hz.png}}
	\captionof{figure}{Amplitude $A$ in relation to the azimuth angle $\varphi$ at the resonance of \SI{2258}{\Hz} ($l=1$ resonance)} 
	\label{fig:azimuth_2258}
\end{minipage}

\vspace{0.5cm}

\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A3/A3_azimut_cut_l2_3461Hz.png}}
	\captionof{figure}{Amplitude $A$ in relation to the azimuth angle $\varphi$ at the resonance of \SI{3461}{\Hz} ($l=2$ resonance)} 
	\label{fig:azimuth_3461}
\end{minipage}
\hspace{0.2cm}
\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{A3/A3_azimut_cut_l2_3481Hz.png}}
	\captionof{figure}{Amplitude $A$ in relation to the azimuth angle $\varphi$ at the resonance of \SI{3481}{\Hz} ($l=2$ resonance)} 
	\label{fig:azimuth_3481}
\end{minipage}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.5\linewidth]{A3/A3_azimut_cut_l2_3628Hz.png}}
	\captionof{figure}{Amplitude $A$ in relation to the azimuth angle $\varphi$ at the resonance of \SI{3628}{\Hz} ($l=2$ resonance)} 
	\label{fig:azimuth_3628}
\end{minipage}

\section{Discussion}
The spectra of the spherical resonator (\fig{spectrum_0deg} and \fig{spectrum_180deg}) show a typical behaviour of a resonance spectrum. Whilst the amplitude off-resonance is nearly zero, once hitting a resonance it quickly rises creating a sharp peak at the resonator-specific resonances.\\
It is interesting to observe, that the spectrum for a rotation of $\alpha = \SI{0}{\degree}$ is different from the spectrum of $\alpha = \SI{180}{\degree}$. If you think about it geometrically since the resonator is a sphere, the resonances that form inside it should be symmetrical as well, leaving no other option than to expect, that the amplitudes are the same at $\alpha = \SI{0}{\degree}$ and $\alpha = \SI{180}{\degree}$. \\
However one has to take into account, that the microphone is mounted off-axis of the resonance building up in the resonator. This way the microphone does not move simply around the axis of the resonance but instead takes a specific trajectory described by the relation between $\theta$ and $\alpha$. Therefor the spectra at \SI{0}{\degree} and \SI{180}{\degree} differ in the heights of the amplitudes (amplitude is angle-dependent) due to the microphone not being mounted on axis with the resonances. At \SI{180}{\degree} however, the amplitudes in general are a lot higher, because at that specific position the microphone is placed directly opposite of the speaker. Note, that the resonances only differ in their amplitude not thier frequency (the frequencies are not angle-dependent). This supports the statement above.\\

In \fig{5k_sweep} the splitting of a resonance peak can be observed for different rotations of the resonator halfs. One can see, that the frequencies of the resonant peaks stay the same, the amplitudes however slightly shift. The peak with lower frequency has its maximum amplitude at \SI{40}{\degree} rotation, whilst the peak at the higher frequency stays roughly constant.\\
It can be assumed, that this reasonance is degenerated. Therefor the splitting is induced due to asymmetries in the structure of the spherical resonator. This leads to the corresponding effect of a magnetic shift in the model of a hydrogen atom.\\
In this regard, the peak that changes its amplitude more (peak with the lower frequency) is the one, that is associated with $l=0$ since the amplitude can only change for the degenerated peak. Therefor the peak with the higher frequency has a quantum number $l \neq 0$.\\

The quantum number $l$ for the different resonances (see \fig{polar_2294} to \fig{polar_6550}) can be assigned as follows:

\begin{itemize}
	\centering
	\item[] \fig{polar_2294} \ra $l=1$
	\item[] \fig{polar_3675} \ra $l=2$
	\item[] \fig{polar_4963} \ra $l=3$
	\item[] \fig{polar_6205} \ra $l=5$
	\item[] \fig{polar_6550} \ra $l=6$
\end{itemize}

See \fig{quantum_numbers} for comparison.\\
Especially in the last case one can see, that resonances in a spherical resonator can overlap and hence produce polar diagrams, which might look like \fig{polar_6550}. The main amplitude is definitely produced by the resonance of $l=6$. However there are fractions of the $l=1$ typical resonance-shapes observable, too. This causes a superposition diagram as shown in \fig{polar_6550}.\\
In general one can assume, that since the resonator is not perfectly spherical and there is the possibility of superposition between the resonances, the images \fig{polar_2294}, \fig{polar_3675} and especially \fig{polar_6550} are not as clear (in terms of the shape) as the others.

The resonance splitting in \fig{spectrum_0mm} to \fig{spectrum_9mm} can be explained with the inserting of spacer rings. By breaking the spherical symmetry of the resonator, the degeneration of the resonances is abolished. Hence the propagation of the sound waves is now determined by the vertical axis of symmetry. The higher the spacing, the stronger this effect can be observed (compare \fig{spectrum_3mm} to \fig{spectrum_9mm}). \fig{l1_resonance_peak} shows the splitting of only the $l=1$-resonance in more detail.\\
To characterize this splitting, we investigated the $l=1$-resonance further, discovering that the frequency splitting of the resonance scales linearly with the thickness of the spacer rings inserted (see \fig{linfit_splitting}).\\

With this new splitting comes a new qunatum number that can be assigned to the different resonances - the magnetic quantum number $m$. It is important to note, that (unlike the hydrogen atom model) the splitting only is observable for positive $m$. Resonances for negative $m$ are only phase shifted and therefor can not be observed, because the frequencies of the resonance do not split. They stay degenerated.\\
With keeping this in mind, the following magnetic quantum numbers $m$ can be assinged to the azimuth polar diagrams (\fig{azimuth_2090} to \fig{azimuth_3628}):

\begin{itemize}
	\centering
	\item[] \fig{azimuth_2090} \ra $l=1$ and $m=0$
	\item[] \fig{azimuth_2258} \ra $l=1$ and $m=1$
	\item[] \fig{azimuth_3461} \ra $l=2$ and $m=1$
	\item[] \fig{azimuth_3481} \ra $l=2$ and $m=0$
	\item[] \fig{azimuth_3628} \ra $l=2$ and $m=2$
\end{itemize}

In order to assume $m=0$ for \fig{azimuth_3481} it is assumed, that the wavefunction propagates under an angle of \SI{45}{\degree} with respect to the azimuth plane. This is why the amplitudes look shifted to the \enquote{bottom half} of the azimuth plane.

%\begin{thebibliography}{9}

%    \bibitem{} %\label{bib:}
%    \href{}{ [Datum]}

%\end{thebibliography}

\appendix
\section{Original measurement data}
\subsection{Task 1}
    \begin{minipage}{0.48\linewidth}
    	\makebox[\linewidth]{\includegraphics[width=\linewidth]{Appendix/A1_0_5kHz_sweep.png}}
    	\captionof{figure}{Frequency spectrum with high resolution around \SI{5}{\kHz} for $\alpha=\SI{0}{\degree}$}
    	\label{fig:appendix_5k_0deg}
    \end{minipage}
    \begin{minipage}{0.48\linewidth}
    	\makebox[\linewidth]{\includegraphics[width=\linewidth]{Appendix/A1_20_5kHz_sweep.png}}
        \captionof{figure}{Frequency spectrum with high resolution around \SI{5}{\kHz} for $\alpha=\SI{20}{\degree}$}
    	\label{fig:appendix_5k_20deg}
    \end{minipage}
    \vspace{0.5cm}
    
    \begin{minipage}{\linewidth}
    	\makebox[\linewidth]{\includegraphics[width=0.5\linewidth]{Appendix/A1_40_5kHz_sweep.png}}
        \captionof{figure}{Frequency spectrum with high resolution around \SI{5}{\kHz} for $\alpha=\SI{40}{\degree}$}
    	\label{fig:appendix_5k_40deg}
    \end{minipage}

\section{Task 2}
\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{Appendix/A3_180°_l1_0mm.png}}
	\captionof{figure}{$l=1$ resonance with a spacing of \SI{0}{\mm}}
	\label{fig:appendix_l1_0mm}
\end{minipage}
\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{Appendix/A3_180°_l1_3mm.png}}
	\captionof{figure}{$l=1$ resonance with a spacing of \SI{3}{\mm}}
	\label{fig:appendix_l1_3mm}
\end{minipage}

\vspace{0.5cm}

\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{Appendix/A3_180°_l1_6mm.png}}
	\captionof{figure}{$l=1$ resonance with a spacing of \SI{6}{\mm}}
	\label{fig:appendix_l1_6mm}
\end{minipage}
\begin{minipage}{0.48\linewidth}
	\makebox[\linewidth]{\includegraphics[width=\linewidth]{Appendix/A3_180°_l1_9mm.png}}
	\captionof{figure}{$l=1$ resonance with a spacing of \SI{9}{\mm}}
	\label{fig:appendix_l1_9mm}
\end{minipage}

\vspace{0.5cm}

\begin{minipage}{\linewidth}
	\makebox[\linewidth]{\includegraphics[width=0.8\linewidth]{Appendix/quantum_number_and_orbitals.jpg}}
	\captionof{figure}{Hydrogen orbital shapes organized by their quantum numbers $l$ and $m$}
	\label{fig:quantum_numbers}
\end{minipage}



\section*{Erklärung der selbstständigen Anfertigung}
Hiermit erklären wir, dass das vorliegende Protokoll selbständig verfasst und keine anderen als die angegebenen Hilfsmittel verwendet wurden. \\
Insbesondere versichern wir, dass alle genutzten Internetseiten kenntlich gemacht wurden und im Verzeichnis der Internetseiten bzw. im Quellenverzeichnis aufgeführt.\\
Sämtliche Schaltskizzen, Abbildungen sowie Oszilloskopaufnahmen sind selbstständig angefertigt, oder der Versuchsanleitung entnommen. 

\end{document}
