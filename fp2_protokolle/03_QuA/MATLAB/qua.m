%% Workspace aufräumen
clear;
clc;
load('qua.mat','-mat');

%% Konstantendefinition und Normierung bzw. Kalibrierung
f1 = figure('Name','Double Peak for different Rotations');
f2 = figure('Name','Magnetic Shift (Sweep)');
f3 = figure('Name','Magnetic Shift (Peak)');

%% 1.1 Plotten des Doppelpeaks für 0deg bis 40deg
figure(f1);
plot(f_0, A_0, '+', 'Color', [1 0.5 0])
hold on
plot(f_20, A_20, '+', 'Color', [0 0.5 1])
plot(f_40, A_40, 'k+')
hold off
legend('0° rotation', '20° rotation', '40° rotation')
ylabel ('Amplitude (a.u.)')
ylim([0 16])
xlabel ('Frequency / Hz')
xlim([4898 5002]);
grid on

% f = gcf;
% exportgraphics(f,'peaks_at_5kHz.png','Resolution',600)

%% 1.2 Plotten der Verschiebung durch die Spacer-Ringe (Sweep)
figure(f2);
plot(f0mm, A0mm, '-')
hold on
plot(f3mm, A3mm, '-')
plot(f6mm, A6mm, '-')
plot(f9mm, A9mm, '-')
hold off
legend('0mm spacing', '3mm spacing', '6mm spacing', '9mm spacing','Location','northwest')
ylabel ('Amplitude (a.u.)')
ylim([0 50])
xlabel ('Frequency / Hz')
xlim([1998 5502]);
grid on

exportgraphics(f2,'magnetic_shift_sweep.png','Resolution',600)

%% 1.2 Plotten der Verschiebung durch die Spacer-Ringe (Sweep)
figure(f3);
plot(f0mm_P, A0mm_P, '-')
hold on
plot(f3mm_P, A3mm_P, '-')
plot(f6mm_P, A6mm_P, '-')
plot(f9mm_P, A9mm_P, '-')
hold off
legend('0mm spacing', '3mm spacing', '6mm spacing', '9mm spacing','Location','northwest')
ylabel ('Amplitude (a.u.)')
ylim([0 18])
xlabel ('Frequency / Hz')
xlim([1998 2402]);
grid on

exportgraphics(f3,'magnetic_shift_peak.png','Resolution',600)

%% 2 Fitting
% %Linear-Fit
% f_lin = fit(x_beta,Z_beta,'poly1')
% 
% %Plotting the linear fit
% plot(f_lin,x_beta, Z_beta, 'k+')
% legend('Datenpunkte','Linearer Fit')
% ylabel ('Zählrate Z(x) / 1')
% ylim([9000, 14000])
% xlabel ('Stellschraubenposition x / \mum')
% xlim([-5 505]);
% grid on


% %Fit-Formel aus Versuchsanleitung
%lambda_approx_va = ['(1/4).*(sin(a.*sin(((x-248).*10.^(-6))./0.489))./(a.*sin(((x-248).*10.^(-6))./0.489))).^2'...
%'*(sin(2.*b.*sin(((x-248).*10.^(-6))./0.489))./sin(b.*sin(((x-248).*10.^(-6))./0.489))).^2+c'];

% %Fitoptionen
% fo = fitoptions('Method','NonlinearLeastSquare',...
%     'Lower',[0,0,0],'Upper',[Inf,Inf,1],...
%     'DiffMinChange',1.0e-8,...
%     'DiffMaxChange',0.1,...
%     'MaxFunEvals',600,...
%     'MaxIter',400,...
%     'TolFun',1.0e-6,...
%     'TolX',1.0e-6,...
%     'StartPoint',[0.1473,0.6013,0.5325]);
% ft1 = fittype(lambda_approx_va,'options',fo);

% %Fit
% lambdafit_va = fit(x,Z_kor,ft1);
% 
% 
% %Fit-Plot
% plot(lambdafit_va,x, Z_kor, 'k+')
% legend('Datenpunkte', 'Lambda-Fit')
% ylabel ('Zählrate Z(x) / 1')
% ylim([0, 1.05])
% xlabel ('Stellschraubenposition x / \mum')
% xlim([-20 520]);
% grid on